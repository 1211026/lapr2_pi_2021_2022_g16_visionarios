# Use Case Diagram (UCD)

**In the scope of this project, there is a direct relationship of _1 to 1_ between Use Cases (UC) and User Stories (US).**

However, be aware, this is a pedagogical simplification. On further projects and curricular units might also exist _1 to N **and/or** N to 1 relationships between US and UC.

**Insert below the Use Case Diagram in a SVG format**

![UseCaseDiagram.svg](UseCaseDiagram.svg)


**For each UC/US, it must be provided evidences of applying main activities of the software development process (requirements, analysis, design, tests and code). Gather those evidences on a separate file for each UC/US and set up a link as suggested below.**

# Use Cases / User Stories
| UC/US  | Description                                                               |                   
|:----|:------------------------------------------------------------------------|
| US 003 | [ShortNameOfUS001](US003.md)   |
| US 009 | [ShortNameOfUS002](US009.md)  |
| US 010 | [ShortNameOfUS003](US010.md)|
| US 011 | [ShortNameOfUS004](US011.md)|
| US 012 | [ShortNameOfUS004](US012.md)|
| US 013 | [ShortNameOfUS004](US013.md)|
| ... | ...|
| US 326 | [ShortNameOfUS326](US326.md)|
