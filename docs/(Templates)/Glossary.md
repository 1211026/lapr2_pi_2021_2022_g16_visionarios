# Glossary

**Terms, Expressions and Acronyms (TEA) must be organized alphabetically.**

(To complete according the provided example)

| **_TEA_** (EN)  | **_TEA_** (PT) | **_Description_** (EN)                                           |                                       
|:------------------------|:-----------------|:--------------------------------------------|
| **AGES** |**Agrupamentos de Centros de Saúde** | Groupings of Healthcare Centers | 
| **ARS** |**ARS** | Portuguese Regional health administration |
| **Clerk** | **Administrativo** | Person responsible for carrying out various business supporting activities on the system. |
| **CLK** | **ADM** | Acronym for _Clerk_.|
| **Contiguous Subsequence** |**Subesequências contiguas** | It is a certain interval of a sequence where it is increasing or decreasing| 
| **Center Coordinator** |**Coordenador de Centro** | Has the responsibility to manage the Covid19 vaccination process |
| **Covid-19** | **Covid-19** | Ultra-transmissible virus that attacks the respiratory tract.|
| **DGS** | **DGS** | State-Funded Portuguese healthcare system.|
| **FIFO Queue** |**Fila FIFO** | The main tipe of queues( First come, First Served) | 
| **Healthcare centre** |**Centro de Saúde** | Similar to the Vaccination centre, but can give all of the Vaccines. Is linked to ARS. | 
| **Nurse** | **Enfermeira** | Person in charge of giving vaccines|
| **Pfizer vaccine** |**Vacina Pfizes** | Type of Vaccine | 
| **Recepcionits** |**Rececionistas** | The people that atend you when you arrive and aknowledges if the user is ready to take the vaccine | 
| **Recovery Period** |**Periodo de Recuperação** | Time to recover after the Vaccine (30 mins)  | 
| **Recovery Room** |**Sala de recuperação** | The room where you can rest after the Vaccine | 
| **Vaccine** | **Vacina** |A substance used to stimulate the production of antibodies and provide immunity against one or several diseases|
| **Vaccination Center** | **Centro de Vacinação** | A place to get the vaccine|
| **SNS** |**SNS** | Portuguese National Health Service| 
| **SNS user** |**Utilizador do SNS** | A person that uses de Portuguese National Health Service |
| **The Astra Zeneca vaccine** |**A vacina Astra Zeneca** | Type of vaccine |
| **The Moderna vaccine** |**A vacina morderna** | Type of Vaccine| 
| **Vaccine dosage** |**Dose da vacina** | The amount of vaccine one has to take (e.g.: 30 ml) |








