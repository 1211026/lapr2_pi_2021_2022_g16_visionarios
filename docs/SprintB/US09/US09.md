# US 09 - As an administrator, I want to register a vaccination center to respond to a certain pandemic

## 1. Requirements Engineering

### 1.1. User Story Description

*As an administrator, I want to register a vaccination center to respond to a certain pandemic.*

### 1.2. Customer Specifications and Clarifications 

*[...]vaccination process is mainly carried out through community mass vaccination centers [...] distributed across the country."

"Both kinds of vaccination centers are characterized by a name, an address, a phone number, an e-mail address, a fax number, a website address, opening and closing hours, slot duration (e.g.: 5 minutes) and the maximum number of vaccines that can be given per slot (e.g.: 10 vaccines per slot)."

"In addition, each vaccination center has one coordinator".*

### 1.3. Acceptance Criteria

*Insert here the client acceptance criteria.*

### 1.4. Found out Dependencies

*No dependencies were found.

### 1.5 Input and Output Data

#### Input data
Name, address, phone number, e-mail address, fax number, website address, opening and closing hours, slot duration maximum number of vaccines that can be given 
per slot.
#### Output data
(In)Success of the operation(Creation of the vaccination center).


### 1.6. System Sequence Diagram (SSD)


![USXXX-SSD](US09-SSD.svg)


### 1.7 Other Relevant Remarks




## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt 
 

![USXXX-MD](US09-DMExcerpt.svg)

### 2.2. Other Remarks

*Use this section to capture some aditional notes/remarks that must be taken into consideration into the design activity. In some case, it might be usefull to add other analysis artifacts (e.g. activity or state diagrams).* 



## 3. Design - User Story Realization 

### 3.1. Rationale

**The rationale grounds on the SSD interactions and the identified input/output data.**

| Interaction ID | Question: Which class is responsible for... | Answer  | Justification (with patterns)  |
|:-------------  |:--------------------- |:------------|:---------------------------- |
| Step 1: Starts creating a vaccination center 		 |	instantiating a new vaccination center?						 |        Company     |   Creator                           |
| Step 2 : request data  		 |							 |             |                              |
| Step 3:types requested data  		 |	saving the input data?						 |  VaccinationCenter           |                  The created object has its own data            |
| Step 4 : shows the data and request confirmation |	validating data locally?						 |    Vaccination Center         |  knows its own data                            |
          |                          |   validating data globally?           |   Company                          |knows all the VaccinationCenters
| Step 5:confirms the data  		 |	saving the created data?						 |  Company           |   records all the VaccinationCenters                           |
| Step 6:informs the success of the operation  		 |	informing the operating success?						 |      UI       |  responsible for interacting with the user                            |              
 


### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are: 

 * Company
 * VaccinationCenter

Other software classes (i.e. Pure Fabrication) identified: 
 * RegisterVaccinationCenterUI  
 * RegisterVaccinationCenterController

## 3.2. Sequence Diagram (SD)



![USXXX-SD](US09-SD.svg)

## 3.3. Class Diagram (CD)



![USXXX-CD](US09-CD.svg)

# 4. Tests 
###Tests for Company class
**Test 1:** Test for the validateVaccinationCenter method with a null instance.

	public void testValidateVaccinationCenterNull(){
        VaccinationCenter vc1=null;
        boolean expected=false;
        boolean result= c1.validateVaccinationCenter(vc1);
        Assertions.assertEquals(expected,result);
    }
**Test 2:** Test for saveVaccinationCenter method with a valid center. 

	public void testSaveVaccinationCenter(){
        ArrayList<VaccinationCenter> vclist= new ArrayList<>();
        vclist.add(vc1);
        boolean expected=true;
        boolean result= c1.saveVaccinationCenter(vc1);
        Assertions.assertEquals(expected,result);
    }
###Tests for RegisterVaccinationCenter class

**Test 1:** Test for the saveVaccinationCenter method with a valid center.

        public void testSaveVaccinationCenterValid(){
        cr1.createVaccinationCenter("Centro1","Rua1",1111,"email",11,"websiteadress",8,17,5,10);
        boolean expected=true;
        boolean result= cr1.saveVaccinationCenter();
        Assertions.assertEquals(expected,result);
    }
# 5. Construction (Implementation)

###VaccinationCenter

    public VaccinationCenter(String name, String adress, int phoneNumber, String email, int faxNumber, String websiteAdress, int openingHours, int closingHours, int slotDuration, int maxSlotVaccines) {
        this.name = name;
        this.adress = adress;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.faxNumber = faxNumber;
        this.websiteAdress = websiteAdress;
        this.openingHours = openingHours;
        this.closingHours = closingHours;
        this.slotDuration = slotDuration;
        this.maxSlotVaccines = maxSlotVaccines;
    }

###Company

    public VaccinationCenter createVaccinationCenter(String name, String adress, int phoneNumber, String email, int faxNumber, String websiteAdress, int openingHours, int closingHours, int slotDuration, int maxSlotVaccines){
        return new VaccinationCenter(name, adress, phoneNumber, email, faxNumber, websiteAdress, openingHours, closingHours, slotDuration, maxSlotVaccines);
    }
    public boolean validateVaccinationCenter (VaccinationCenter vc) {
        if (vc == null)
            return false;
        return ! this.vaccinationCenterList.contains(vc);
    }
    public boolean saveVaccinationCenter (VaccinationCenter vc) {
        if (!validateVaccinationCenter(vc))
            return false;
        return this.vaccinationCenterList.add(vc);
    }

###RegisterVaccinationCenterController

    public class RegisterVaccinationCenterController {
    private Company company;
    private VaccinationCenter vc;
    public RegisterVaccinationCenterController () {
        this(App.getInstance().getCompany());
    }
    public RegisterVaccinationCenterController(Company company) {
        this.company = company;
        this.vc = null;
    }
    public boolean createVaccinationCenter(String name, String adress, int phoneNumber, String email, int faxNumber, String websiteAdress, int openingHours, int closingHours, int slotDuration, int maxSlotVaccines) {
        this.vc = this.company.createVaccinationCenter(name, adress, phoneNumber, email, faxNumber, websiteAdress, openingHours, closingHours, slotDuration, maxSlotVaccines);
        return this.company.validateVaccinationCenter(vc);
    }
    public boolean saveVaccinationCenter() {
        return this.company.saveVaccinationCenter(vc);
    }
 


# 6. Integration and Demo 

*In this section, it is suggested to describe the efforts made to integrate this functionality with the other features of the system.*


# 7. Observations

*In this section, it is suggested to present a critical perspective on the developed work, pointing, for example, to other alternatives and or future related work.*





