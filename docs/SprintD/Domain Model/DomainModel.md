# OO Analysis #

The construction process of the domain model is based on the client specifications, especially the nouns (for _concepts_) and verbs (for _relations_) used. 

## Rationale to identify domain conceptual classes ##
To identify domain conceptual classes, start by making a list of candidate conceptual classes inspired by the list of categories suggested in the book "Applying UML and Patterns: An Introduction to Object-Oriented Analysis and Design and Iterative Development". 


### _Conceptual Class Category List_ ###

**Business Transactions**

* Vaccination Program
* Vaccination Process

---

**Transaction Line Items**

* Vaccine

---

**Product/Service related to a Transaction or Transaction Line Item**

*  Software Application

---


**Transaction Records**

* Vaccine

---  


**Roles of People or Organizations**

* SNS users
* Employees
* Nurses
* Coordinator
* Receptionists
* Administrator
* Center Coordinator


---


**Places**

* Community Mass Vaccination Centers
* Health Care Centers

---

**Noteworthy Events**

* Schedule vaccination
* Arrival of the user to the vaccination center

---


**Physical Objects**

* Vaccine
---


**Descriptions of Things**

*  


---


**Catalogs**

*  

---


**Containers**

* Vaccination Centers

---


**Elements of Containers**

* Vaccines

---


**Organizations**

* DGS
* Companies
* ARS
* AGES

---

**Other External/Collaborating Systems**

* SNS
* Pfizer
* Moderna
* Astra Zeneca
* Email
* SMS Message


---


**Records of finance, work, contracts, legal matters**

*
---


**Financial Instruments**

*  

---


**Documents mentioned/used to perform some work**

* Vaccination Certificates
* EU Covid Digital Certificate
---



###**Rationale to identify associations between conceptual classes**###

An association is a relationship between instances of objects that indicates a relevant connection and that is worth of remembering, or it is derivable from the List of Common Associations: 

+ **_A_** is physically or logically part of **_B_**
+ **_A_** is physically or logically contained in/on **_B_**
+ **_A_** is a description for **_B_**
+ **_A_** known/logged/recorded/reported/captured in **_B_**
+ **_A_** uses or manages or owns **_B_**
+ **_A_** is related with a transaction (item) of **_B_**
+ etc.



| Concept (A) 		|  Association   	|  Concept (B) |
|----------	   		|:-------------:		|------:       |
| Company  	|                manages    		|VaccinationCenter|
| Company  	|                  has    		 	|Employee|
|Company| 					applies				|Vaccine|
|Company|					owns				|SNS System|
|SNS System|				has 				|Employee|
|Employee| 					is part of 			|VaccinationCenter|
|Employee|					has					|CenterCoordinator|
|Employee| 					has 				|Administrator|
|Employee|					has 				|Nurse|
|Employee| 					has					|Receptionist|
|CenterCoordinator|			manages				|VaccinationProcess|
|Nurse|						registers 			|AdverseReactions|
|Nurse|						delivers			|VaccineCertification|
|Nurse|						manages				|VaccineAdministration|
|Nurse|						is responsible for	|RecoveryRoom|
|VaccineType|				is a description of	|Vaccine|
|Dosage|					is part of			|VaccineType|
|GroupAge|					is part of			|VaccineType|
|HealthCareCenter| 		 	is a type of		|VaccinationCenter|
|CommunityMassVaccinationCenter| is a type of 	|VaccinationCenter|
|Email|						is part of	 		|Message|
|SMS|						is part of			|Message|
|SMS|						is part of			|VaccineSchedule|
|Message|					is part of 			|RecoveryRoom|
|Administrator|				manages				|VaccinationCenter|
|Administrator|				manages				|Employee|
|Administrator|				registers 			|SNSUser|
|SNSUser| 					uses				|SNSSystem|
|SNSUser| 					requests			|VaccineSchedule|
|SNSUser|					waits on			|RecoveryRoom|
|SNSUser| 					requests			|VaccineCertification|
|SNSUser| 					does				|UserCheckIn|
|VaccineAdministration|     is administered on	|SNSUser|
|Receptionist| 				is part of			|Employee|
|Receptionist|				registers			|UserCheckIn|
|Receptionist| 				registers			|VaccineSchedule|
|VaccineSchedule|			is recorded on		|SNSSystem|
|VaccineSchedule|			for taking			|VaccineType|
|VaccinationCenter|			is part of			|VaccineSchedule|
|AdverseReactions|			is recorded on  	|SNSSystem|



## Domain Model

**Do NOT forget to identify concepts attributes too.**

**Insert below the Domain Model Diagram in a SVG format**

![Domain_Model.svg](Domain_Model.svg)




