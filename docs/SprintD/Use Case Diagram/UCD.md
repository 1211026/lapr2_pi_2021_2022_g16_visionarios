# Use Case Diagram (UCD)

**In the scope of this project, there is a direct relationship of _1 to 1_ between Use Cases (UC) and User Stories (US).**

However, be aware, this is a pedagogical simplification. On further projects and curricular units might also exist _1 to N **and/or** N to 1 relationships between US and UC.

**Insert below the Use Case Diagram in a SVG format**

![UseCaseDiagram.svg](UseCaseDiagram.svg)


**For each UC/US, it must be provided evidences of applying main activities of the software development process (requirements, analysis, design, tests and code). Gather those evidences on a separate file for each UC/US and set up a link as suggested below.**

# Use Cases / User Stories
| UC/US  | Description                                                               |                   
|:----|:------------------------------------------------------------------------|
| US 001 | [ScheduleVaccine(SNSUSer)](../../SprintC/US01/US01.md)   |
| US 002 | [ScheduleVaccine(Receptionist)](../../SprintC/US02/US002.md)  |
| US 003 | [RegisterUSer](../../SprintB/US03/US003.md)|
| US 004 | [RegisterArrivalSNSUser](../../SprintC/US04/US04.md)|
| US 005 | [RegistersVaccineType](../../SprintC/US05/US05.md)   |
| US 006 | [GenerateDailyReport](../../SprintD/US06/US06.md)   |
| US 007 | [RecordAdverseReactions](OptionalUS)   |
| US 008 | [AdministrateVaccine](../../SprintD/US08/US08.md)   |
| US 009 | [RegisterVaccinationCenter](../../SprintB/US09/US09.md)   |
| US 010 | [RegisterEmployee](../../SprintB/US10/US10.md)   |
| US 011 | [GetListEmployeesByRole](../../SprintB/US11/US011.md)   |
| US 012 | [SpecifyNewVaccineType](../../SprintB/US12/US12.md)   |
| US 013 | [SpecifyNewVaccine](../../SprintB/US13/US13.md)   |
| US 014 | [LoadUsersFromCSVFile](../../SprintC/US14/US14.md)   |
| US 015 | [ExportVaccinationStatistics](../../SprintD/US15/US15.md)   |
| US 016 | [AnalyzePerformanceCenter](../../SprintD/US16/US16.md)   |
| US 017 | [ImportDataLegacySystem](../../SprintD/US17/US17.md)   |


