package app.domain.model;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author Diogo Teixeira <1210916@isep.ipp.pt>
 */
public class Employee implements Serializable {
        /**
         * id of the Employee
         */
        private final int id;
        /**
         * name of the Employee
         */
        private String name;
        /**
         *  address of the Employee
         */
        private String address;
        /**
         * roleId of the Employee
         */
         private final String roleId;
         /**
        *  e-mail of the Employee
         */
         private String email;
        /**
         *  phone number of the Employee
         */
         private int phoneNumber;
        /**
        *  citizen card number of the Employee
        */
        private int citizenCardNumber;
        /**
        *  password of the Employee
        */
        private final String password;

        private static List<String> listEmployeeRoles = new ArrayList<>();

        /**
        * list of the Employee roles
        */
        public static List<String> getListEmployeeRoles() {
            listEmployeeRoles.add("Receptionist");
            listEmployeeRoles.add("Nurse");
            listEmployeeRoles.add("Coordinator");
            return listEmployeeRoles;
        }




    public Employee(int id, String name, String address,String email, int phoneNumber,int citizenCardNumber, String roleId) {
        checkRoleIdRules(roleId);
        checkPhoneNumber(phoneNumber);
        this.id = id;
        this.name = name;
        this.address=address;
        this.email=email;
        this.phoneNumber=phoneNumber;
        this.citizenCardNumber=citizenCardNumber;
        this.roleId = roleId;
        this.password= generatePassword();

    }

    private void checkRoleIdRules(String roleId) {
        if (StringUtils.isBlank(roleId))
            throw new IllegalArgumentException("RoleId cannot be empty.");
        if (!roleId.equals("Receptionist"))
                if (!roleId.equals("Coordinator"))
                    if(!roleId.equals("Nurse"))
            throw new IllegalArgumentException("RoleId it's not available.");
    }

    private void checkPhoneNumber(int phoneNumber){
        if(phoneNumber>=999999999 || phoneNumber<100000000){
            throw new IllegalArgumentException("Phone number it's not available.");
        }
    }

    private String generatePassword(){
        StringBuilder password= new StringBuilder();
        Random random= new Random();
        char randomizedNumberOrAlphabetic=(char)(random.nextInt(2) +'0');

        int numCharPassword = 7;
        for (int i = 0; i < numCharPassword; i++) {
            char character = 0;
            if (randomizedNumberOrAlphabetic==48) {
                 character=(char) (random.nextInt(10) + '0');
            }
            if (randomizedNumberOrAlphabetic==49){
                character= (char) (random.nextInt(26) + 'A');
            }
            password.append(character);
        }

        return password.toString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getCitizenCardNumber() {
        return citizenCardNumber;
    }

    public void setCitizenCardNumber(int citizenCardNumber) {
        this.citizenCardNumber = citizenCardNumber;
    }

    public int getId() {
        return id;
    }

    public String getRoleId() {
        return roleId;
    }

    public String getPassword() {
        return password;
    }
}
