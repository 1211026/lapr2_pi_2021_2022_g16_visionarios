package app.domain.model;

import app.domain.model.stores.NumberFullyVaccinatedStore;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;


/**
 *@author Diogo Teixeira <1210916@isep.ipp.pt>
 */
public class ConvertToFile {
    NumberFullyVaccinatedStore numberFullyVaccinatedStore= new NumberFullyVaccinatedStore();
    /**
     * Verify if the interval is saved on the system
     * @param firstDay First day of the interval
     * @param lastDay Last day oh the interval
     * @throws IOException Number of fully vaccinated file
     */
    public void ValidateIntervalOnSystem(String firstDay,String lastDay) throws IOException {
        int start=-1, finish=-1,i=0;
        LocalDate firstDayRes= LocalDate.parse(firstDay, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        LocalDate lastDayRes= LocalDate.parse(lastDay, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        while ( i <numberFullyVaccinatedStore.getNumberFullyVaccinatedList().size()) {
            if (firstDayRes.equals(numberFullyVaccinatedStore.getNumberFullyVaccinatedList().get(i).getDate())){
                start=i;
            }
            if (lastDayRes.equals(numberFullyVaccinatedStore.getNumberFullyVaccinatedList().get(i).getDate())){
               finish=i;
            }
            i++;
        }
        if (finish==-1||start==-1){
            System.out.println("The requested range is not in the system. Please chose a range between ( "+ numberFullyVaccinatedStore.getNumberFullyVaccinatedList().get(0).getDate() + " ; " + numberFullyVaccinatedStore.getNumberFullyVaccinatedList().get(i).getDate() + " )");
        }
        else {
            BuildFile(firstDayRes,start,finish);
        }


    }

    /**
     * Build the file with the users fully vaccinated
     * @param firstDayRes First day of the interval
     * @param start
     * @param finish
     * @throws IOException Number of fully vaccinated file
     */
    public void BuildFile(LocalDate firstDayRes, int start, int finish) throws IOException{
        File usersFullyVaccinatedFile= new File("Users Fully Vaccinated");
        FileWriter fw= new FileWriter(usersFullyVaccinatedFile);
        while (start<=finish){
            fw.write(firstDayRes + ": " + numberFullyVaccinatedStore.getNumberFullyVaccinatedList().get(start).getNumberFullyVaccinated() + "\n");
            firstDayRes=firstDayRes.plusDays(1);
            start++;
        }
        fw.close();
    }
}
