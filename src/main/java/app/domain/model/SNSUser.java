package app.domain.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Random;

public class SNSUser implements Serializable {

    /**
     * name of the SNSuser
     */

    private String name;

    /**
     * id of the SNSuser
     */

    private String sex;

    /**
     *  Birth date of the SNSuser
     */

    private LocalDate dateBirth;

    /**
     * address of the SNSUser
     */

    private String address;

    /**
     * Phone number of the SNSuser
     */

    private int phoneNumber;

    /**
     *  e-mail of the SNSuser
     */

    private String email;

    /**
     * SNSUser id
     */

    private int id;

    /**
     * SNSUsers citizen number
     */

    private int CitizenNumber;

    /**
     * password of the SNSuser
     */

    private String password;

    /**
     * Constructor
     * Creates and instance of SNS user receiving all the attributes
     */

    public SNSUser(String name, String sex, LocalDate dateBirth, String address,  int phoneNumber, String email, int id, int CitizenNumber) {

        checkPhoneNumber(phoneNumber);
        this.name = name;
        this.sex = sex;
        this.dateBirth = dateBirth;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.id = id;
        this.CitizenNumber = CitizenNumber;
        this.password=generatePassword();


    }

    /**
     * method to check if a phone number is valid
     * @param phoneNumber The phone number of the SNS User number
     */

    private void checkPhoneNumber(int phoneNumber){
        if(phoneNumber>=999999999 || phoneNumber<100000000){
            throw new IllegalArgumentException("Phone number it's not available.");
        }
    }

    /**
     * Method to generate a random password
     * @return
     */

    private String generatePassword(){
        StringBuilder password= new StringBuilder();
        Random random= new Random();
        char randomizedNumberOrAlphabetic=(char)(random.nextInt(2) +'0');

        int numCharPassword = 7;
        for (int i = 0; i < numCharPassword; i++) {
            char character = 0;
            if (randomizedNumberOrAlphabetic==48) {
                character=(char) (random.nextInt(10) + '0');
            }
            if (randomizedNumberOrAlphabetic==49){
                character= (char) (random.nextInt(26) + 'A');
            }
            password.append(character);
        }

        return password.toString();
    }


    public int getId() {
        return id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getCitizenNumber() {
        return CitizenNumber;
    }

    public void setCitizenNumber(int citizenNumber) {
        CitizenNumber = citizenNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public LocalDate getDateBirth() {
        return dateBirth;
    }

}
