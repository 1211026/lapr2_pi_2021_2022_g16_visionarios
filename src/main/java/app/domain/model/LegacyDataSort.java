package app.domain.model;

import app.domain.model.stores.LegacyDataStore;
import app.ui.console.utils.Utils;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

/**
 * @author Rui Milhazes <1211031@isep.ipp.pt>
 * C:\Users\ruimm\OneDrive\Documentos\performanceDataFromGaoFuNationalCenterDoPortoVaccinationCenter.csv
 * C:\Users\ruimm\OneDrive\Documentos\performanceDataFromGaoFuNationalCenterDoPortoVaccinationCenter1.csv
 * C:\Users\ruimm\OneDrive\Documentos\performanceDataFromGaoFuNationalCenterDoPortoVaccinationCenter2.csv
 */

public class LegacyDataSort {

    /**
     * Method to sort the Legacy File by bubble sort
     * @param legacyFile is the file loaded by the user
     */

    public static void bubbleSort(File legacyFile) throws IOException {
        String fileInString = LegacyData.fileReader((legacyFile));
        List<LegacyData> ar = LegacyData.readLine(fileInString);
        System.out.println("Option A - Arrival Time.");
        System.out.println("Option B - Center Leaving Time.");
        String sort = Utils.readLineFromConsole("What sorting method do you prefer?");
        System.out.println("");
        if (Objects.equals(sort, "A") || Objects.equals(sort, "Arrival Time")) {
            System.out.println("You have chosen Arrival Time:");
            System.out.println(" ");
        } else {
            if (Objects.equals(sort, "B") || Objects.equals(sort, "Center Leaving Time")) {
                System.out.println("You have chosen Center Leaving Time:");
                System.out.println(" ");
            }
        }
        int column = 0;
        if (Objects.equals(sort, "A")) {
            column = 5;
        } else {
            if (Objects.equals(sort, "B"))
                column = 7;
        }
        long startTime = System.nanoTime();
        if (column == 5) {
            LegacyData temp;
            boolean sorted = false;

            while (!sorted) {
                sorted = true;
                for (int i = 0; i < ar.size() - 1; i++) {
                    if (ar.get(i).getArrivalDateTime().compareTo((ar.get(i + 1)).getArrivalDateTime()) > 0) {
                        temp = ar.get(i);
                        ar.set(i, ar.get(i + 1));
                        ar.set(i + 1, temp);
                        sorted = false;
                    }
                }
            }
        }else{
            if (column==7){
                LegacyData temp;
                boolean sorted = false;

                while (!sorted) {
                    sorted = true;
                    for (int i = 0; i < ar.size() - 1; i++) {
                        if (ar.get(i).getLeavingDateTime().compareTo((ar.get(i + 1)).getLeavingDateTime()) > 0)  {
                            temp = ar.get(i);
                            ar.set(i, ar.get(i + 1));
                            ar.set(i + 1, temp);
                            sorted = false;
                        }
                    }
                }
            }
        }
        long endTime = System.nanoTime();
        System.out.println("Array sorted:");
        System.out.println("|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------...|");
        System.out.println("");
        for(int i = 0; i < ar.size(); i++) {
            System.out.print(ar.get(i)+ " | ");
        }
        System.out.println("");
        System.out.println("|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------...|");
        System.out.println("Bubble Sort runtime: " + (endTime - startTime));
    }

    /**
     * Method to sort the Legacy File by quick sort
     * @param legacyFile is the file loaded by the user
     */

    public static void quickSort(File legacyFile) throws IOException {
        String fileInString = LegacyData.fileReader((legacyFile));
        List<LegacyData> ar = LegacyData.readLine(fileInString);
        System.out.println("Option A - Arrival Time.");
        System.out.println("Option B - Center Leaving Time.");
        String sort = Utils.readLineFromConsole("What sorting method do you prefer?");
        System.out.println("");
        if (Objects.equals(sort, "A")) {
            System.out.println("You have chosen Arrival Time:");
            System.out.println(" ");
        } else {
            if (Objects.equals(sort, "B")) {
                System.out.println("You have chosen Center Leaving Time:");
                System.out.println(" ");
            }

        }
        int column = 0;
        if (Objects.equals(sort, "A")) {
            column = 5;
        } else {
            if (Objects.equals(sort, "B"))
                column = 7;
        }

        //    {
       //    ArrayList<LegacyDataStore> arr;  // this shall be the sorted list to return, no need to initialise
       //    ArrayList<LocalDate> smaller = new ArrayList<LocalDate>(); // datas smaller than pivot
       //    ArrayList<LocalDate> greater = new ArrayList<LocalDate>(); // datas greater than pivot
       //    LocalDate pivot = LocalDate.from(ar.get(0).getArrivalDateTime());  // first data in list, used as pivot
       //    int i;
       //    LegacyDataStore j;     // Variable used for datas in the loop
       //    for (i = 1 ; i < ar.size() ; i++)
       //    {
       //        j=ar.get(i).getArrivalDateTime();
       //        if (j.compareTo(pivot)<0)   // make sure data has proper compareTo method
       //            smaller.add(j);
       //        else
       //            greater.add(j);
       //    }
       //    smaller=quickSort(smaller);  // capitalise 's'
       //    greater=quickSort(greater);  // sort both halfs recursively
       //    smaller.add(pivot);          // add initial pivot to the end of the (now sorted) smaller Vehicles
       //    smaller.addAll(greater);     // add the (now sorted) greater datas to the smaller ones (now smaller is essentially your sorted list)
       //    arr = smaller;            // assign it to sorted; one could just as well do: return smaller
       //
       //    return arr;
       //}

        //int partition(int start,int end){
        //    System.out.println("\n---------Iteration Starts----------");
        //    System.out.println("\nSorting Window from index number:"+start+" to "+end);
        //
        //    LocalDateTime init = start;
        //    LocalDateTime length = end;
        //
        //    Random r = new Random();
        //    LocalDateTime pivotIndex = nextInRange(start,end,r);
        //    LocalDateTime pivot = ar.get(pivotIndex);
        //
        //    System.out.println("Pivot Element "+pivot+" at index:"+pivotIndex);
        //
        //    while(true){
        //        while(ar.get(length).pivot && length>start){
        //            length--;
        //        }
        //
        //        while(ar.get(init)<pivot && init<end){
        //            init++;
        //        }
        //
        //        if(init<length){
        //            int temp;
        //            temp = inputArray.get(init);
        //            inputArray.set(init,inputArray.get(length));
        //            inputArray.set(length,temp);
        //            length--;
        //            init++;
        //
        //            System.out.println("\nAfter Swapping");
        //            for(int i=start;i<=end;i++){
        //                System.out.print(inputArray.get(i)+" ");
        //            }
        //        }else{
        //            System.out.println("\n---------Iteration Ends---------");
        //            return length;
        //        }
        //    }
        //
        //}



        //LocalDateTime smallIndex;
        //
        //for(int i=1;i<ar.size();i++){
        //
        //    LocalDateTime small = ar.get(i-1);
        //    smallIndex = ;
        //
        //    for(column=i;column<ar.size();column++){
        //        if(ar.getL.isafter(small)){
        //            smallIndex = ar.get(column);
        //            small = ;
        //        }
        //    }
        //
        //    //Swap the smallest element with the first element of unsorted subarray
        //    swap(i-1, small);
        //}


        System.out.println("Array sorted:");
        System.out.println("|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------...|");
        System.out.println("");
        for(int i = 0; i < ar.size(); i++) {
            System.out.print(ar.get(i)+ " | ");
        }
        System.out.println("");
        System.out.println("|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------...|");
        //System.out.println("Quick Sort runtime: " + (endTime - startTime));
    }

    //private static void swap(LocalDateTime sourceIndex,LocalDateTime destIndex, File legacyFile) throws IOException {
    //        String fileInString = LegacyDataStore.fileReader((legacyFile));
    //        List<LegacyDataStore> ar = LegacyDataStore.readLine(fileInString);
    //        LegacyDataStore temp = ar.get(destIndex);
    //        ar.set(destIndex, ar.get(sourceIndex));
    //        ar.set(sourceIndex, temp);
    //
    //}
}