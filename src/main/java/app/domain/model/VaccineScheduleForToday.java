package app.domain.model;

import app.domain.model.stores.SNSUserStore;
import app.domain.model.stores.VaccinationScheduleStore;
import app.domain.model.stores.WaitingRoomStore;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class VaccineScheduleForToday extends VaccinationScheduleStore {
    private final List<VaccineSchedule> vsForTodayList= new ArrayList<>();
    private WaitingRoomStore waitingRoomStore;
    private Company company;
    private SNSUserStore snsUserStore;
    private static String name;
    private static int SNSUserNumber;
    public void createVaccinationScheduledForToday() {
        if(verifyVaccinationScheduledForTodayIsEmpty()){
            validateSNSUserScheduleForToday();
        }
    }

    public int validateSNSUserNumber(int SNSUserNumber) {
        int res=2;
        for (int i = 0; i < vsForTodayList.size(); i++) {
            if (vsForTodayList.get(i).getSnsUserNumber() == SNSUserNumber) {
                res = 0;
            }
        }
        return res;
    }

    public int validateSNSUser(String name, int SNSUserNumber) {
        int res=1;
        String vName="";
        for (int i = 0; i < snsUserStore.getSNSUserList().size(); i++) {
            if (snsUserStore.getSNSUserList().get(i).getId()==SNSUserNumber){
                vName=snsUserStore.getSNSUserList().get(i).getName();
            }
        }
        if (Objects.equals(vName, name)) {
            this.name=name;
            this.SNSUserNumber=SNSUserNumber;
            res = 0;
        }
        return res;
    }

    public void saveArrivalOfSNSUser() {
        waitingRoomStore.addSNSUserName(name);
        waitingRoomStore.addSNSUserNumber(SNSUserNumber);
    }
    public boolean verifyVaccinationScheduledForTodayIsEmpty(){
        if (vsForTodayList.size()!=0)
            return true;
        else
            return false;
    }
    public void validateSNSUserScheduleForToday(){
        for (int i = 0; i <getVslist().size() ; i++) {
            VaccineSchedule vaccineScheduleTest;
            vaccineScheduleTest= getVslist().get(i);
            String data=vaccineScheduleTest.getData();
            if (data.equals(LocalDate.now().getDayOfYear())){
                vsForTodayList.add(vaccineScheduleTest);
            }
        }
    }
}
