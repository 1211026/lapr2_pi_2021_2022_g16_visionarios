package app.domain.model;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class ValidateDates {
    /**
     * @author Diogo Teixeira<1210916@isep.ipp.pt>
     */

    /**
     * Validate if the range of days is possible and if the values entered are valid
     * @param firstDay
     * @param lastDay
     * @return
     */
    public int ValidateInterval(String firstDay, String lastDay){
        int res;
        if (firstDay==null || lastDay==null){
            res=4;
            return res;
        }
        try {
            LocalDate firstDayRes=LocalDate.parse(firstDay, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
            try {
                LocalDate lastDayRes=LocalDate.parse(lastDay, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
                if(lastDayRes.isBefore(firstDayRes)){
                    res=1;
                }else
                    res=0;
            }catch (DateTimeException dateTimeException){
                res=3;
            }
        }catch (DateTimeException dateTimeException){
            res=2;
        }
        return res;
    }
}
