package app.domain.model;

import java.io.Serializable;

/**
 * @author Rodrigo Ferreira <1211030@isep.ipp.pt>
 */
public class VaccineSchedule implements Serializable {
    /**
     * Sns user number
     */
    private int snsUserNumber;
    /**
     * Vaccination Center
     */
    private VaccinationCenter vc;
    /**
     * Data of the schedule
     */
    private String data;
    /**
     * Time of the schedule
     */
    private String time;
    /**
     * Vaccine type chosen
     */
    private VaccineType vaccineType;
    /**
     * Vaccine type by omission
     */
    private static final String VACINNETYPE_OMISSION="a";
    /**
     * Constructor
     * Creates and instance of VaccineSchedule receiving all the attributes.
     */
    public VaccineSchedule(int snsUserNumber, VaccinationCenter vc, String data, String time, VaccineType vaccineType) {
        this.snsUserNumber=snsUserNumber;
        this.vc = vc;
        this.data = data;
        this.time = time;
        this.vaccineType = vaccineType;
    }
    /**
     * Constructor
     * Creates and instance of VaccinationCenter receiving all the atributes without the VaccineType.
     */
    public VaccineSchedule(int snsUserNumber, VaccinationCenter vc, String data, String time) {
        this.snsUserNumber = snsUserNumber;
        this.vc = vc;
        this.data = data;
        this.time = time;
    }
    /**
     * Return the sns user number.
     *
     * @return sns user number
     */
    public int getSnsUserNumber() {
        return snsUserNumber;
    }
    /**
     * Return the vaccine type.
     *
     * @return vaccine type.
     */
    public VaccineType getVaccineType() {
        return vaccineType;
    }
    /**
     * Return the Vaccination Center.
     *
     * @return Vaccination Center.
     */
    public VaccinationCenter getVc() {
        return vc;
    }
    /**
     * Return the data.
     *
     * @return data.
     */
    public String getData() {
        return data;
    }
    /**
     * Return the time.
     *
     * @return time
     */
    public String getTime() {
        return time;
    }
}

