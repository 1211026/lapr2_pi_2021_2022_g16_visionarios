package app.domain.model;

import java.io.Serializable;

public class DosageNumberDosageGroupAge extends Vaccine implements Serializable {
    /**
     * Dosage of the vaccine in mL.
     */
    private int dosageNumber;
    /**
     * Group age for the specific vaccine.
     */
    private String groupAge;
    /**
     * Number of doses of each group age
     */
    private int dosagePerGroupAge;


    /**
     * Constructor
     * Creates and instance of Vaccine receiving all the atributes.
     *
     * @param name
     * @param brand
     * @param code
     */
    public DosageNumberDosageGroupAge(String name, String brand, String code,int dosageNumber,String groupAge,int dosagePerGroupAge) {
        super(name, brand, code);
        this.dosageNumber=dosageNumber;
        this.groupAge=groupAge;
        this.dosagePerGroupAge=dosagePerGroupAge;
    }
    /**
     * Get method for the dosage number.
     * @return dosageNumber
     */
    public int getDosageNumber() {
        return dosageNumber;
    }
    /**
     * Get method for the group age.
     * @return groupAge.
     */
    public String getGroupAge() {
        return groupAge;
    }
    /**
     * Get method for the dosage per group age.
     * @return dosagePerGroupAge.
     */
    public int getDosagePerGroupAge() {
        return dosagePerGroupAge;
    }

    /**
     * Method toString to format an String.
     * @return formated String
     */
    public String toString(){
        return String.format("Name:%s Brand:%s Code:%sDosage:%d mL Group Age:%s Dosage per group age:%d",getName(),getBrand(),getCode(),dosageNumber,groupAge,dosagePerGroupAge);
    }

}
