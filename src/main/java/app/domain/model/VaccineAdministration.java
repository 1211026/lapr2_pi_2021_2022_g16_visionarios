package app.domain.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class VaccineAdministration implements Serializable {

    /**
     * SnsNumber of the person to be vaccinated.
     */
    private int snsUserNumber;
    /**
     * Vaccine type chosen.
     */
    private String vaccineType;
    /**
     * Vaccine name chosen.
     */
    private String vaccineName;
    /**
     * Dose administer.
     */
    private int dose;
    /**
     * Lote Number of the vaccine.
     */
    private String loteNumber;
    /**
     * Date of the administration
     */
    private LocalDateTime dateOfAdministration;
    /**
     * Vaccination Center where the vaccination occurs.
     */
    private VaccinationCenter vaccinationCenter;

    /**
     * Constructor
     * Creates and instance of VaccineAdministration receiving all the atributes.
     */

    public VaccineAdministration(int snsUserNumber, String vaccineType, String vaccineName, int dose, String loteNumber, LocalDateTime dateOfAdministration, VaccinationCenter vaccinationCenter) {
        this.snsUserNumber = snsUserNumber;
        this.vaccineType = vaccineType;
        this.vaccineName = vaccineName;
        this.dose = dose;
        this.loteNumber = loteNumber;
        this.dateOfAdministration = dateOfAdministration;
        this.vaccinationCenter = vaccinationCenter;
    }

    /**
     * Return the Vaccine Type.
     *
     * @return vaccineType.
     */
    public String getVaccineType() {
        return vaccineType;
    }
    /**
     * Return the Vaccine Name.
     *
     * @return vaccineName.
     */
    public String getVaccineName() {
        return vaccineName;
    }
    /**
     * Return the dose.
     *
     * @return dose.
     */
    public int getDose() {
        return dose;
    }
    /**
     * Return the lote number.
     *
     * @return loteNumber.
     */
    public String getLoteNumber() {
        return loteNumber;
    }
    /**
     * Return the sns number.
     *
     * @return snsUserNumber.
     */
    public int getSnsUserNumber() {
        return snsUserNumber;
    }
    /**
     * Return the date.
     *
     * @return dateOfAdministration .
     */
    public LocalDateTime getDateOfAdministration() {
        return dateOfAdministration;
    }
    /**
     * Return the vaccination center.
     *
     * @return vaccinationCenter .
     */
    public VaccinationCenter getVaccinationCenter() {
        return vaccinationCenter;
    }
    public String toString(){
        return String.format("%d %s %s %d %s %s %s" ,snsUserNumber,vaccineType,vaccineName,dose,loteNumber,dateOfAdministration,vaccinationCenter);
    }

}
