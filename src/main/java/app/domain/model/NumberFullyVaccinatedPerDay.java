package app.domain.model;

import java.io.Serializable;
import java.time.LocalDate;

public class NumberFullyVaccinatedPerDay implements Serializable {
    /**
     * Number of people fully vaccinated per day.
     */
    private int NumberFullyVaccinated;

    /**
     * Registration date.
      */
    private LocalDate date;

    /**
     * Constructor
     * Creates and instance of number fully vaccinated per day
     */
    public NumberFullyVaccinatedPerDay(LocalDate date, int numberFullyVaccinated) {
        this.date=date;
        this.NumberFullyVaccinated = numberFullyVaccinated;
    }

    public int getNumberFullyVaccinated() {
        return NumberFullyVaccinated;
    }

    public LocalDate getDate() {
        return date;
    }
}
