package app.domain.model;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class DailyReportTimer extends Thread{
    Timer timer = new Timer();
    String time;
    public DailyReportTimer(String time)
    {
        this.time = time;
    }
    public void run()
    {
        try{
            String[] cast = time.split(":");
            Calendar daily = Calendar.getInstance();
            daily.set(Calendar.HOUR_OF_DAY, Integer.parseInt(cast[0]));
            daily.set(Calendar.MINUTE, Integer.parseInt(cast[1]));
            daily.set(Calendar.SECOND, Integer.parseInt(cast[2]));


            timer.schedule(new DailyReport(), daily.getTime(), TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS));

        }catch(RuntimeException e){
            e.printStackTrace();
            System.out.println("Error executing the daily report");
        }
    }

}
