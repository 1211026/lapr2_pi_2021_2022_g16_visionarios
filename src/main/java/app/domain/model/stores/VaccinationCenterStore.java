package app.domain.model.stores;

import app.domain.model.VaccinationCenter;
import app.domain.model.VaccineType;

import java.io.*;
import java.sql.Time;
import java.util.ArrayList;

public class VaccinationCenterStore {

    private ArrayList<VaccinationCenter> vaccinationCenterList = new ArrayList<>();


    /**
     * @author Rodrigo Ferreira <1211030@isep.ipp.pt>
     * @param name Name of the vaccination center
     * @param address Address of the vaccination center
     * @param phoneNumber Phone Number of the vaccination center
     * @param email Email of the vaccination center
     * @param faxNumber Fax number of the vaccination center
     * @param websiteAddress Website of the vaccination center
     * @param openingHours Opening hours of the vaccination center
     * @param closingHours Closing hours of the vaccination center
     * @param slotDuration Duration of the slots provided to the vaccination center
     * @param maxSlotVaccines Maximum of slots that the vaccination center can have
     * @return a created Vaccination Center
     */
    public VaccinationCenter createVaccinationCenter(String name, String address, int phoneNumber, String email, int faxNumber, String websiteAddress, Time openingHours, Time closingHours, int slotDuration, int maxSlotVaccines){
        return new VaccinationCenter(name, address, phoneNumber, email, faxNumber, websiteAddress, openingHours, closingHours, slotDuration, maxSlotVaccines);
    }

    /**
     *Verifies if the Vaccination Center is valid
     * @param vc Vaccination center object
     * @return false if the Vaccination Center is null
     */

    public boolean validateVaccinationCenter (VaccinationCenter vc) {
        if (vc != null){
            return  !this.vaccinationCenterList.contains(vc);
        }else {
            return false;
        }
    }

    /**
     * Verifies if the Vaccination Center is valid then adds the center to the list
     * @param vc Vaccination Center object
     * @return false if the Vaccination Center is not valid, if the center is valid. is added to the list
     */
    public boolean saveVaccinationCenter (VaccinationCenter vc) {
        if (validateVaccinationCenter(vc)){
            return this.vaccinationCenterList.add(vc)&&recordFile();
        }else{
            return false;
        }
    }
    public ArrayList<VaccinationCenter> getVcList() {
       return vaccinationCenterList;
    }

    /**
     * This method record the vaccination centers on a file.
     * @return true if the center was recorded ,false if the file is not valid.
     */

    public  boolean  recordFile(){
        ArrayList<VaccinationCenter> vclist= new ArrayList<>();
        vclist=readFromFile();
        vaccinationCenterList.addAll(vclist);
        try {
            ObjectOutputStream out=new ObjectOutputStream(new FileOutputStream("VaccinnationCenterList.bin"));
            try {
                out.writeObject(vaccinationCenterList);
                return true;
            }finally {
                out.close();
            }
        }catch (IOException e){
            return false;
        }
    }

    /**
     * This method read data from a file.
     * @return an array of vaccination centers.
     */

    public  ArrayList<VaccinationCenter> readFromFile(){
        ArrayList<VaccinationCenter> vclist= new ArrayList<>();
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream("VaccinnationCenterList.bin"));
            try {
                vclist= (ArrayList<VaccinationCenter>) in.readObject();
            }finally {
                in.close();
            }
        }catch (IOException | ClassNotFoundException e){
            e.printStackTrace();
        }
        return vclist;
    }
}
