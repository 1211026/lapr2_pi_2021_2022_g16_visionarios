package app.domain.model.stores;

import app.domain.model.*;

import java.io.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Formatter;

public class VaccinationScheduleStore {
    private SNSUserStore snsUserStore=new SNSUserStore();
    private ArrayList<VaccineSchedule> vslist= new ArrayList<>();
    /**
     * @author Rodrigo Ferreira <1211030@isep.ipp.pt>
     * @param snsUserNumber Number of the SNS User
     * @param vc Vaccination Center for the vaccination center
     * @param data Date of the vaccine schedule
     * @param time Time of the vaccine schedule
     * @param vaccineType Vaccine of the vaccination schedule
     * @return a created Vaccine Schedule.
     */

    public VaccineSchedule createVaccineSchedule(int snsUserNumber, VaccinationCenter vc, String data, String time, VaccineType vaccineType){
        return new VaccineSchedule(snsUserNumber,vc,data,time,vaccineType);
    }
    /**
     *Verifies if the Vaccine Schedule is valid for US001
     * @param vs Vaccination Schedule Object
     * @return false if the Vaccination Schedule is null, true if is valid.
     */
    public boolean validateVaccineSchedule(VaccineSchedule vs){
        if (vs != null){
            return ! this.vslist.contains(vs);
        }else {
            return false;
        }
    }

    /**
     * Verifies if the Vaccine Schedule is valid for US002
     * @param vs Vaccination Schedule Object
     * @return false if the Vaccination Schedule is null, true if is valid.
     */
    public boolean validateVaccineScheduleReceptionist(VaccineSchedule vs) {

        ArrayList<VaccineSchedule> temp = new ArrayList<>();
        boolean check = false;

        for (SNSUser item : snsUserStore.getSNSUserList()) {
            if (item.getId() == vs.getSnsUserNumber() && ChronoUnit.YEARS.between(item.getDateBirth(), LocalDate.now()) >= 13) {
                check = true;
            }
        }
        if (check) {
            for (VaccineSchedule item : vslist) {
                if (item.getSnsUserNumber() == vs.getSnsUserNumber()) {
                    temp.add(item);
                }
            }
            if (temp.size() != 0) {
                for (VaccineSchedule item : temp) {
                    if (ChronoUnit.DAYS.between(LocalDate.parse(item.getData()), LocalDate.parse(vs.getData())) <= 60) {
                        check = false;

                    }
                }
            }
        }
        return check;
    }
    /**
     * Verifies if the Vaccine Schedule is valid,check the max slot for that time and the slot time then adds the schedule to the list.
     * @param vs Vaccination Schedule Object
     * @return false if the Vaccination Center is not valid or the slot is full, if the center is valid is added to the list.
     */
    public boolean saveVaccineSchedule(VaccineSchedule vs){
        if (validateVaccineSchedule(vs) && checkMaxSlot(vs) && checkSameVaccine(vs) && checkSlotDuration(vs)){
            return this.vslist.add(vs) && recordFile();
        }else{
            return false;
        }
    }

    /**
     * Verifies if the Sns user has more than one equal schedule.
     * @param vs Vaccination Schedule Object
     * @return false if there is more than one equal schedule, true if there is just one schedule.
     */
    public boolean checkSameVaccine(VaccineSchedule vs){
        ArrayList<VaccineSchedule> vslist= new ArrayList<>();
        vslist=readFromFile();
        boolean result = false;
        if (vslist.size()==0){
            result = true;
        }else {
            for (VaccineSchedule vaccineSchedule : vslist) {
                if (vaccineSchedule.getSnsUserNumber() != vs.getSnsUserNumber() || vaccineSchedule.getVaccineType() != vs.getVaccineType()) {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }
    /**
     * Verifies for the time chosen if the slot is full.
     * @param vs Vaccination Schedule Object
     * @return false if the slot is full, true if the slot is not full.
     */
    public boolean checkMaxSlot(VaccineSchedule vs){
        int maxSlot=vs.getVc().getMaxSlotVaccines();
         ArrayList<VaccineSchedule> vslist= new ArrayList<>();
        vslist=readFromFile();
        int cont=0;
        if (vslist.size()==0){
            cont=0;
        }else {
            for (VaccineSchedule vaccineSchedule : vslist) {
                if (vaccineSchedule.getVc() == vs.getVc() && vaccineSchedule.getData().equals(vs.getData()) && vaccineSchedule.getTime().equals(vs.getTime())) {
                    cont++;
                }
            }
        }
        return cont <= maxSlot;

    }
    /**
     * Verifies for the time chosen the slot duration and if the time is available.
     * @param vs Vaccination Schedule Object
     * @return false if the time is not available, true if the time is available.
     */
    public boolean checkSlotDuration(VaccineSchedule vs){
        String opening=vs.getVc().getOpeningHours();
        String closing=vs.getVc().getClosingHours();
        LocalTime openingHourss=LocalTime.parse(opening);
        LocalTime closingHours=LocalTime.parse(closing);
        LocalTime comp=openingHourss;
        LocalTime chosedHours=LocalTime.parse(vs.getTime());
        int slotDuration=vs.getVc().getSlotDuration();
        boolean result = false;
        ArrayList<LocalTime> hoursList= new ArrayList<>();
        do {
            hoursList.add(comp);
            comp=comp.plusMinutes(slotDuration);
        }while (comp.isBefore(closingHours));
        for (LocalTime localTime : hoursList) {
            if (localTime.equals(chosedHours)) {
                result = true;
                break;
            }
        }
        return result;
    }

    public ArrayList<VaccineSchedule> getVslist() {
        return vslist;
    }

    /**
     * Method to send a sms to a SNS user.
     * @param vs Vaccination Schedule Object
     * @throws FileNotFoundException If the file is not found
     */
    public void sendSms(VaccineSchedule vs) throws FileNotFoundException {
        Formatter in = new Formatter("SMS.txt");
        in.format("Vaccine was schedule at %s on %s at the %s",vs.getTime(),vs.getData(),vs.getVc());
        in.close();
    }

    /**
     * This method record the vaccines schedule on a file.
     * @return true if the vaccine was recorded ,false if the file is not valid.
     */

    public  boolean  recordFile(){
        try {
            ObjectOutputStream out=new ObjectOutputStream(new FileOutputStream("VaccineSchedule.bin"));
            try {
                out.writeObject(vslist);
                return true;
            }finally {
                out.close();
            }
        }catch (IOException e){
            return false;
        }
    }

    /**
     * This method read data from a file.
     * @return an array of vaccine schedule.
     */

    public  ArrayList<VaccineSchedule> readFromFile(){
        ArrayList<VaccineSchedule> vslist= new ArrayList<>();
        try {
            ObjectInputStream in=new ObjectInputStream(new FileInputStream("VaccineSchedule.bin"));
            try {
                vslist= (ArrayList<VaccineSchedule>) in.readObject();
            }finally {
                in.close();
            }
        }catch (IOException | ClassNotFoundException e){
            e.printStackTrace();
        }
        return vslist;
    }


}
