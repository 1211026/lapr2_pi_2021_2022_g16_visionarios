package app.domain.model.stores;
import app.domain.model.*;

import java.io.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;
import java.util.Objects;

public class VaccineAdministrationStore {
    ArrayList<VaccineAdministration> vaccineAdministrationsList = new ArrayList<>();
    WaitingRoomStore waitingRoomStore = new WaitingRoomStore();
    SNSUserStore snsUserStore=new SNSUserStore();
    VaccinationScheduleStore vaccinationScheduleStore=new VaccinationScheduleStore();
    DosaNumberDosageGroupAgeStore dosaNumberDosageGroupAgeStore=new DosaNumberDosageGroupAgeStore();
    VaccineStore vaccineStore=new VaccineStore();

    /**
     * @author Rodrigo Ferreira <1211030@isep.ipp.pt>
     * @param vaccineType VAccine type to by administer.
     * @param vaccineName Name of the vaccine.
     * @param dose dose of the vaccine.
     * @param loteNumber lote of the vaccine.
     * @return a created Vaccine Administration.
     */
    public VaccineAdministration createVaccineAdministration(int snsUserNumber, String vaccineType, String vaccineName, int dose, String loteNumber, LocalDateTime dateOfAdministration, VaccinationCenter vaccinationCenter){
        return new VaccineAdministration(snsUserNumber,vaccineType,vaccineName,dose,loteNumber,dateOfAdministration,vaccinationCenter);
    }
    /**
     *Verifies if the Vaccine Administration is valid.
     * @param va Vaccine Administration Object
     * @return false if the Vaccine Administration is null, true if is valid.
     */
    public boolean validateVaccineAdministration(VaccineAdministration va){
        if (va != null){
            return ! this.vaccineAdministrationsList.contains(va);
        }else {
            return false;
        }
    }

    /**
     * Verifies if the Vaccine Schedule is valid.
     * @param va Vaccine Administration Object
     * @return false if the Vaccine Adminstration is not valid, if the vaccine is valid is added to the list.
     */
    public boolean saveVaccineAdminstration(VaccineAdministration va){
        if (validateVaccineAdministration(va)){
            return this.vaccineAdministrationsList.add(va) && recordFile();
        }else{
            return false;
        }
    }
    /**
     * Method to send a sms to a SNS user.
     * @throws FileNotFoundException If the file is not found.
     */
    public void sendSMS() throws FileNotFoundException {
        Formatter in = new Formatter("SMSLeaveCenter.txt");
        in.format("The user can leave the center");
        in.close();
    }

    /**
     * Method to help the nurse check the user´s name, age, adverse reactions and the vaccine schedule.
     * @param snsUserNumber
     */
    public String checkUserInfo(int snsUserNumber){
        List<SNSUser> snsUsersList=new ArrayList<>();
        snsUsersList=snsUserStore.readFromFile();
        ArrayList<VaccineSchedule> vaccineSchedulesList=new ArrayList<>();
        vaccineSchedulesList=vaccinationScheduleStore.readFromFile();
        int i,j;
        for ( i = 0; i <snsUsersList.size() ; i++) {
            if (snsUsersList.get(i).getId()==snsUserNumber){
                break;
            }
        }
        for ( j = 0; j < vaccineSchedulesList.size(); j++) {
            if (vaccineSchedulesList.get(j).getSnsUserNumber()==snsUserNumber){
                break;
            }
        }
        String vaccineType=vaccineSchedulesList.get(j).getVaccineType().getDesignation();
        String name=snsUsersList.get(i).getName();
        return String.format("The user %s ,have %f ,have no adverse reactions and schedule the %s vaccine",name,ageOfPerson(snsUserNumber),vaccineType);
    }

    /**
     * This method helps the nurse choose the best vaccine to administer according to group age and vaccine schedule.
     * @param vaccineType schedule
     * @param age of the user
     */

    public ArrayList<DosageNumberDosageGroupAge> checkVaccineToGive(String vaccineType,int age){
        ArrayList<DosageNumberDosageGroupAge> vaccineList=new ArrayList<>();
        vaccineList=dosaNumberDosageGroupAgeStore.readFromFile();
        ArrayList<DosageNumberDosageGroupAge> bestVaccineList=new ArrayList<>();
        for (int i = 0; i <vaccineList.size() ; i++) {
            String groupAge=vaccineList.get(i).getGroupAge();
            String[] parts = groupAge.split("-");
            int lowestAge = Integer.parseInt(parts[0]);
           int highestAge = Integer.parseInt(parts[1]);
            if (Objects.equals(vaccineType, vaccineList.get(i).getName()) &&(age>=lowestAge && age<=highestAge) ){
                bestVaccineList.add(vaccineList.get(i));
            }
        }
        return bestVaccineList;
     }

    /**
     * This method calculates the SNS user age
     * @param snsUserNumber
     * @return the age of the SNS user.
     */

    public double ageOfPerson(int snsUserNumber){
        List<SNSUser> snsUsersList=new ArrayList<>();
        snsUsersList=snsUserStore.readFromFile();
        double years;
        LocalDate datenow=LocalDate.now(),dateOfBirth ;
        int j;
        for ( j =0; j <snsUsersList.size() ; j++) {
            if (snsUsersList.get(j).getId()==snsUserNumber){
                break;
            }
        }
        dateOfBirth=snsUsersList.get(j).getDateBirth();
        years= ChronoUnit.YEARS.between(dateOfBirth,datenow);
        return years;
    }

    /**
     * This method helps the nurse to choose the dose of vaccine.
     * @param snsUserNUmber
     * @return the dose to be administered.
     */
    public int checkDose(int snsUserNUmber ){
        int dose=0;
        ArrayList<VaccineAdministration> vaccineAdministrationsList=new ArrayList<>();
        for (int i = 0; i < vaccineAdministrationsList.size() ; i++) {
            if (snsUserNUmber==vaccineAdministrationsList.get(i).getSnsUserNumber()){
                dose=vaccineAdministrationsList.get(i).getDose();
            }

        }
       return dose+1;
    }

    /**
     *This method helps chech where the vaccines was taken.
     * @param snsUserNumber
     * @return the vaccination center where the administration occurred.
     */
    public VaccinationCenter checkVaccinationCenter(int snsUserNumber){
        ArrayList<VaccineSchedule> vaccineSchedulesList=new ArrayList<>();
        vaccineSchedulesList=vaccinationScheduleStore.readFromFile();
        int i;
        for ( i = 0; i < vaccineSchedulesList.size(); i++) {
            if (snsUserNumber==vaccineSchedulesList.get(i).getSnsUserNumber()){
                break;
            }
        }
        return vaccineSchedulesList.get(i).getVc();
    }



    public ArrayList<VaccineAdministration> getVaccineAdministrationsListList(){
        return vaccineAdministrationsList;
    }
    /**
     * This method record the vaccines administer on a file.
     * @return true if the vaccine was recorded ,false if the file is not valid.
     */

    public  boolean  recordFile(){
        ArrayList<VaccineAdministration> valist= new ArrayList<>();
        valist=readFromFile();
        vaccineAdministrationsList.addAll(valist);
        try {
            ObjectOutputStream out=new ObjectOutputStream(new FileOutputStream("VaccineAdministrationsList.bin"));
            try {
                out.writeObject(vaccineAdministrationsList);
                return true;
            }finally {
                out.close();
            }
        }catch (IOException e){
            return false;
        }
    }

    /**
     * This method read data from a file.
     * @return an array of vaccineAdministration.
     */

    public  ArrayList<VaccineAdministration> readFromFile(){
        ArrayList<VaccineAdministration> valist= new ArrayList<>();
        try {
            ObjectInputStream in=new ObjectInputStream(new FileInputStream("VaccineAdministrationsList.bin"));
            try {
                valist= (ArrayList<VaccineAdministration>) in.readObject();
            }finally {
                in.close();
            }
        }catch (IOException | ClassNotFoundException e){
            e.printStackTrace();
        }
        return valist;
    }


}
