package app.domain.model.stores;

import app.domain.model.DosageNumberDosageGroupAge;


import java.io.*;
import java.util.ArrayList;

public class DosaNumberDosageGroupAgeStore {
    ArrayList<DosageNumberDosageGroupAge> dosageNumberDosageGroupAgesList  = new ArrayList<>();
    /**
     * @author Rodrigo FErreira <1211030@isep.ipp.pt>
     * @param name Name of the vaccine
     * @param brand Brand of the vaccine
     * @param code code of the vaccine
     * @param dosageNumber dosage of the vaccine in mL.
     * @param groupAge group age for the vaccine.
     * @param dosagePerGroupAge dosage for each group.
     * @return a created Vaccine.
     */
    public DosageNumberDosageGroupAge createDosageNumberDosageGroupAge(String name, String brand, String code,int dosageNumber,String groupAge,int dosagePerGroupAge){
        return new DosageNumberDosageGroupAge(name,brand,code,dosageNumber,groupAge,dosagePerGroupAge);
    }
    /**
     * @param dosageNumberDosageGroupAge  object
     * @return false if the Vaccine is null,true if is valid.
     */

    public boolean validateDosageNumberDosageGroupAge (DosageNumberDosageGroupAge dosageNumberDosageGroupAge){
        if (dosageNumberDosageGroupAge == null)
            return false;
        return ! this.dosageNumberDosageGroupAgesList.contains(dosageNumberDosageGroupAge);
    }

    /**
     * Verifies if the Vaccine  is valid and then adds it to the list
     * @param dosageNumberDosageGroupAge Vaccination Type Object
     * @return Returns the addiction of the vaccine object to the list and adds to the file.
     */

    public boolean saveDosageNumberDosageGroupAge (DosageNumberDosageGroupAge dosageNumberDosageGroupAge){
        if (!validateDosageNumberDosageGroupAge(dosageNumberDosageGroupAge))
            return false;
        return this.dosageNumberDosageGroupAgesList.add(dosageNumberDosageGroupAge)&& recordFile();
    }

    public ArrayList<DosageNumberDosageGroupAge> getDosageNumberDosageGroupAge() {
        return dosageNumberDosageGroupAgesList;
    }

    /**
     * This method record the vaccine on a file.
     * @return true if the vaccine was recorded ,false if the file is not valid.
     */

    public  boolean  recordFile(){
        ArrayList<DosageNumberDosageGroupAge> list= new ArrayList<>();
        list=readFromFile();
        dosageNumberDosageGroupAgesList.addAll(list);
        try {
            ObjectOutputStream out=new ObjectOutputStream(new FileOutputStream("DosageNumberDosageGroupAgesList.bin"));
            try {
                out.writeObject(dosageNumberDosageGroupAgesList);
                return true;
            }finally {
                out.close();
            }
        }catch (IOException e){
            return false;
        }
    }

    /**
     * This method read data from a file.
     * @return an array of vaccine type
     */

    public  ArrayList<DosageNumberDosageGroupAge> readFromFile(){
        ArrayList<DosageNumberDosageGroupAge> dosageNumberDosageGroupAgeList= new ArrayList<>();
        try {
            ObjectInputStream in=new ObjectInputStream(new FileInputStream("DosageNumberDosageGroupAgesList.bin"));
            try {
                dosageNumberDosageGroupAgeList= (ArrayList<DosageNumberDosageGroupAge>) in.readObject();
            }finally {
                in.close();
            }
        }catch (IOException | ClassNotFoundException e){
            e.printStackTrace();
        }
        return dosageNumberDosageGroupAgeList;
    }
}
