package app.domain.model.stores;

import app.domain.model.SNSUser;
import app.domain.model.VaccineType;

import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class SNSUserStore {
    private List<SNSUser> SNSUserList= new ArrayList<>();
    /**
     * @author Rodrigo Moreira <1211026@isep.ipp.pt>
     * @param name Name of the SNS User
     * @param sex Gender of the SNS User
     * @param dateBirth Date of Birth of the SNS User
     * @param address Address of the SNS User
     * @param phoneNumber  Phone Number of the SNS User
     * @param email Email of the SNS User
     * @param id ID of the SNS User
     * @return a created SNS User
     */

    public SNSUser createSNSUser(String name, String sex, LocalDate dateBirth, String address, int phoneNumber, String email, int id, int Citizennumber){
        return new SNSUser(name, sex, dateBirth, address, phoneNumber,email, id, Citizennumber);
    }

    public boolean validateSNSUser (SNSUser SNSuser){
        if (SNSuser != null){
            return ! this.SNSUserList.contains(SNSuser);
        }else {
            return false;
        }
    }

    /**
     * Verifies if the SNS User is valid and then adds it to the list
     * @param SNSUser  SNS User Object
     * @return SNS User object added to the list
     */

    public boolean saveSNSUser(SNSUser SNSUser) {
        if (validateSNSUser(SNSUser)){
            return this.SNSUserList.add(SNSUser) && recordFile();
        }else{
            return false;
        }
    }

    public List<SNSUser> getSNSUserList()
    {
        return SNSUserList;
    }

    public  boolean  recordFile(){
        try {
            ObjectOutputStream out=new ObjectOutputStream(new FileOutputStream("SNSUserLIst.bin"));
            try {
                out.writeObject(SNSUserList);
                return true;
            }finally {
                out.close();
            }
        }catch (IOException e){
            return false;
        }
    }

    /**
     * This method read data from a file.
     * @return an array of vaccine type
     */

    public  ArrayList<SNSUser> readFromFile(){
        ArrayList<SNSUser> userlist= new ArrayList<>();
        try {
            ObjectInputStream in=new ObjectInputStream(new FileInputStream("SNSUserLIst.bin"));
            try {
                userlist= (ArrayList<SNSUser>) in.readObject();
            }finally {
                in.close();
            }
        }catch (IOException | ClassNotFoundException e){
            e.printStackTrace();
        }
        return userlist;
    }
}