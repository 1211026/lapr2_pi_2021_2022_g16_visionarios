package app.domain.model.stores;



import java.util.Collection;
import java.util.Iterator;
import java.util.Queue;

public class WaitingRoomStore {

    /**
     * @author Diogo Teixeira <1210916>
     */

    private Queue<String>waitingRoomListName= new Queue<>() {
        @Override
        public boolean add(String snsUser) {
            return false;
        }

        @Override
        public boolean offer(String snsUser) {
            return false;
        }

        @Override
        public String remove() {
            return null;
        }

        @Override
        public String poll() {
            return null;
        }

        @Override
        public String element() {
            return null;
        }

        @Override
        public String peek() {
            return null;
        }

        @Override
        public int size() {
            return 0;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public boolean contains(Object o) {
            return false;
        }

        @Override
        public Iterator<String> iterator() {
            return null;
        }

        @Override
        public String[] toArray() {
            return new String[0];
        }

        @Override
        public <T> T[] toArray(T[] a) {
            return null;
        }

        @Override
        public boolean remove(Object o) {
            return false;
        }

        @Override
        public boolean containsAll(Collection<?> c) {
            return false;
        }

        @Override
        public boolean addAll(Collection<? extends String> c) {
            return false;
        }

        @Override
        public boolean removeAll(Collection<?> c) {
            return false;
        }

        @Override
        public boolean retainAll(Collection<?> c) {
            return false;
        }

        @Override
        public void clear() {

        }
    };
    public void addSNSUserName(String name){
        waitingRoomListName.add(name);
    }

    public Queue<String> getWaitingRoomListName() {
        return waitingRoomListName;
    }
    private Queue<Integer>waitingRoomListSNSUserNumber= new Queue<>() {

        @Override
        public int size() {
            return 0;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public boolean contains(Object o) {
            return false;
        }

        @Override
        public Iterator<Integer> iterator() {
            return null;
        }

        @Override
        public String[] toArray() {
            return new String[0];
        }

        @Override
        public <T> T[] toArray(T[] a) {
            return null;
        }

        @Override
        public boolean add(Integer integer) {
            return false;
        }

        @Override
        public boolean remove(Object o) {
            return false;
        }

        @Override
        public boolean containsAll(Collection<?> c) {
            return false;
        }

        @Override
        public boolean addAll(Collection<? extends Integer> c) {
            return false;
        }

        @Override
        public boolean removeAll(Collection<?> c) {
            return false;
        }

        @Override
        public boolean retainAll(Collection<?> c) {
            return false;
        }

        @Override
        public void clear() {

        }

        @Override
        public boolean offer(Integer integer) {
            return false;
        }

        @Override
        public Integer remove() {
            return null;
        }

        @Override
        public Integer poll() {
            return null;
        }

        @Override
        public Integer element() {
            return null;
        }

        @Override
        public Integer peek() {
            return null;
        }
    };
    public void addSNSUserNumber(int SNSUserNumber){
        waitingRoomListSNSUserNumber.add(SNSUserNumber);
    }

    public Queue<Integer> getSNSUserSNSUserNumber() {
        return waitingRoomListSNSUserNumber;
    }
}
