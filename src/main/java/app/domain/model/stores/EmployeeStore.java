package app.domain.model.stores;

import app.domain.model.Employee;
import app.domain.model.SNSUser;
import app.domain.model.VaccineType;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class EmployeeStore {
    List<Employee>EmployeeList= new ArrayList<>();
    private VaccinationCenterStore vaccinationCenterStore = new VaccinationCenterStore();

    /**
     * @author Diogo Teixeira <1210916@isep.ipp.pt>
     * @param id ID of the employee
     * @param name Name of the employee
     * @param address Address of the employee
     * @param roleId Role of the employee (Receptionist, Nurse..)
     * @param email Email of the employee
     * @param citizenCardNumber Unique identification of the employee
     * @param phoneNumber Phone Number of the employee
     * @return a created New Employee
     */

    public Employee createNewEmployee(int id, String name, String address, String email, int phoneNumber, int citizenCardNumber, String roleId){
        return new Employee(id,name, address,email,phoneNumber,citizenCardNumber, roleId);
    }

    /**
     * @param ne New Employee Object
     * @return false if the Employee is null
     */

    public boolean validateNewEmployee (Employee ne){
        if (ne != null) return !this.vaccinationCenterStore.getVcList().contains(ne);
        else {
            return false;
        }
    }

    /**
     * Verifies if the Employee is valid and then adds it to the list
     * @param ne New Employee Object
     * @return Returns the addiction of the new employee object to the list
     */

    public boolean saveNewEmployee (Employee ne){
        if (validateNewEmployee(ne)){
            return this.EmployeeList.add(ne) && recordFile();
        }else{
            return false;
        }
    }

    public List<Employee> getEmployeeList() {
        return EmployeeList;
    }

    /**
     * @author Gabriel Silva <1210808@isep.ipp.pt>
     * @param s Contains the role that the Administrator introduced
     * @return returns the List filled with Employees of given role
     */

    public List<Employee> getEmployeeRoleList(String s)
    {
        List<Employee> result = new ArrayList<>();

        for(Employee item: EmployeeList) if(item.getRoleId().equals(s)) result.add(item);

        return result;
    }

    /**
     * Method to record a file
     * @return
     */
    public  boolean  recordFile(){
        try {
            ObjectOutputStream out=new ObjectOutputStream(new FileOutputStream("EmployeeList.bin"));
            try {
                out.writeObject(EmployeeList);
                return true;
            }finally {
                out.close();
            }
        }catch (IOException e){
            return false;
        }
    }

    /**
     * This method read data from a file.
     * @return a array of vaccine type
     */

    public  ArrayList<Employee> readFromFile(){
        ArrayList<Employee> nelist= new ArrayList<>();
        try {
            ObjectInputStream in=new ObjectInputStream(new FileInputStream("EmployeeList.bin"));
            try {
                nelist= (ArrayList<Employee>) in.readObject();
            }finally {
                in.close();
            }
        }catch (IOException | ClassNotFoundException e){
            e.printStackTrace();
        }
        return nelist;
    }
}
