package app.domain.model.stores;

import app.domain.model.NumberFullyVaccinatedPerDay;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;

public class NumberFullyVaccinatedStore implements Serializable {
/**
 * @author Diogo Teixeira <1210916@isep.ipp.pt>
 */

    ArrayList<NumberFullyVaccinatedPerDay> numberFullyVaccinatedList= new ArrayList<>();

    /**
     * @param date
     * @param numberFullyVaccinated
     * @return a created numberFullyVaccinated
     */

    public NumberFullyVaccinatedPerDay addNumberFullyVaccinatedPerDay(LocalDate date, int numberFullyVaccinated){
        return new NumberFullyVaccinatedPerDay(date,numberFullyVaccinated);
    }

    public ArrayList<NumberFullyVaccinatedPerDay> getNumberFullyVaccinatedList() {
        return numberFullyVaccinatedList;
    }
}