package app.domain.model.stores;

import app.domain.model.GroupAgeOfThePerson;
import app.domain.model.VaccineType;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class GroupAgeStore {
    private List<GroupAgeOfThePerson> GroupAgeList= new ArrayList<>();
    /**
     * @author Rodrigo Moreira <1211026@isep.ipp.pt>
     * @param groupAge Group Age to take the vaccine
     * @return the group Age of the person
     */

    public GroupAgeOfThePerson createNewGroupAge(String groupAge){
        return new GroupAgeOfThePerson(groupAge);
    }

    public boolean verifyGroupAgeOfThePerson (GroupAgeOfThePerson groupAge){
        if (groupAge != null){
            return ! this.GroupAgeList.contains(groupAge);
        }else {
            return false;
        }
    }

    public boolean saveGroupAgeOfThePerson(GroupAgeOfThePerson groupAge) {
        if (verifyGroupAgeOfThePerson(groupAge)){
            return this.GroupAgeList.add(groupAge)&& recordFileGroupAge();
        }else{
            return false;
        }
    }
    public List<GroupAgeOfThePerson> getGroupAgeList(){return GroupAgeList;}

    public  boolean  recordFileGroupAge() {
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("GroupAgeList.bin"));
            try {
                out.writeObject(GroupAgeList);
                return true;
            } finally {
                out.close();
            }
        } catch (IOException e) {
            return false;
        }
    }

        public  List<GroupAgeOfThePerson> readFromFileGroupAge(){
            List<GroupAgeOfThePerson> AgeGroupList= new ArrayList<>();
            try {
                ObjectInputStream in=new ObjectInputStream(new FileInputStream("GroupAgeList.bin"));
                try {
                    AgeGroupList= (List<GroupAgeOfThePerson>) in.readObject();
                }finally {
                    in.close();
                }
            }catch (IOException | ClassNotFoundException e){
                e.printStackTrace();
            }
            return AgeGroupList;
        }

    }