package app.domain.model.stores;

import app.domain.model.Dosage;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class DosageNumberStore {
    private List<Dosage> numberOfDosesPerAgeGroupList = new ArrayList<>();
    private List<Dosage> dosageNumberList = new ArrayList<>();

    /**
     * @param vaccineDosage Vaccine Dosage
     * @return a new quantity of dosage for each vaccine
     * @author Rodrigo Moreira <1211026@isep.ipp.pt>
     */

    public Dosage createDosageNumber(int vaccineDosage) {
        return new Dosage(vaccineDosage);
    }

    public boolean verifyDosageNumber(Dosage vaccineDosage) {
        if (vaccineDosage != null) {
            return !this.numberOfDosesPerAgeGroupList.contains(vaccineDosage);
        } else {
            return false;
        }
    }

    public boolean saveDosageNumber(Dosage vaccineDosage) {
        if (verifyDosageNumber(vaccineDosage) == true) {
            return this.dosageNumberList.add(vaccineDosage) && recordFileVacDosage();
        } else {
            return false;
        }
    }

    public boolean recordFileVacDosage() {
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("VaccineDosageList.bin"));
            try {
                out.writeObject(dosageNumberList);
                return true;
            } finally {
                out.close();
            }
        } catch (IOException e) {
            return false;
        }
    }

    public  List<Dosage> readFromFileVacDosage(){
        List<Dosage> VacDosagelist= new ArrayList<>();
        try {
            ObjectInputStream in=new ObjectInputStream(new FileInputStream("VaccineDosageList.bin"));
            try {
                VacDosagelist= (List<Dosage>) in.readObject();
            }finally {
                in.close();
            }
        }catch (IOException | ClassNotFoundException e){
            e.printStackTrace();
        }
        return VacDosagelist;
    }
}
