package app.domain.model.stores;

import app.domain.model.Dosage;
import app.domain.model.VaccineType;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class DosagePerAgeGroupStore {
    private List<Dosage> numberOfDosesPerAgeGroupList = new ArrayList<>();
    /**
     * @author Rodrigo Moreira <1211026@isep.ipp.pt>
     * @param numberOfDosesPerAgeGroup The number of dosages per age for the vaccine
     * @return a new dosage per age group
     */

    public Dosage createDosagePerAgeGroup(int numberOfDosesPerAgeGroup){
        return new Dosage(numberOfDosesPerAgeGroup);
    }

    public boolean verifyDosagePerAgeGroup (Dosage numberOfDosesPerAgeGroup){
        if (numberOfDosesPerAgeGroup != null){
            return ! this.numberOfDosesPerAgeGroupList.contains(numberOfDosesPerAgeGroup);
        }else {
            return false;
        }
    }

    public boolean saveDosagePerAgeGroup(Dosage numberOfDosesPerAgeGroup) {
        if (verifyDosagePerAgeGroup(numberOfDosesPerAgeGroup)){
            return this.numberOfDosesPerAgeGroupList.add(numberOfDosesPerAgeGroup)&& recordFileDosagePerAge();
        }else{
            return false;
        }
    }

    public List<Dosage> getNumberOfDosesPerAgeGroupList() {
        return numberOfDosesPerAgeGroupList;
    }

    public  boolean  recordFileDosagePerAge(){
        try {
            ObjectOutputStream out=new ObjectOutputStream(new FileOutputStream("DosagePerAgeGroupList.bin"));
            try {
                out.writeObject(numberOfDosesPerAgeGroupList);
                return true;
            }finally {
                out.close();
            }
        }catch (IOException e){
            return false;
        }
    }

    public  List<Dosage> readFromFileDosagePerAge(){
        List<Dosage> DosePerAgeGrouplist= new ArrayList<>();
        try {
            ObjectInputStream in=new ObjectInputStream(new FileInputStream("DosagePerAgeGroupList.bin"));
            try {
                DosePerAgeGrouplist= (List<Dosage>) in.readObject();
            }finally {
                in.close();
            }
        }catch (IOException | ClassNotFoundException e){
            e.printStackTrace();
        }
        return DosePerAgeGrouplist;
    }
}
