package app.domain.model.stores;
import app.domain.model.LegacyData;
import java.io.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Rui Milhazes <1211031@isep.ipp.pt>
 */

public class LegacyDataStore  {

    List<LegacyData>LegacyDataList = new ArrayList<>();

    public LegacyData createLegacyData(app.domain.model.SNSUser SNSUser, String vaccine, String dose, String lotNumber, LocalDateTime scheduleDateTime, LocalDateTime arrivalDateTime, LocalDateTime nurseAdministrationDateTime, LocalDateTime leavingDateTime){
        return new LegacyData(SNSUser, vaccine, dose, lotNumber, scheduleDateTime, arrivalDateTime, nurseAdministrationDateTime, leavingDateTime);
    }

    public boolean validateNewLegacyData(LegacyData lds){
        if (lds != null) return !this.LegacyDataList.contains(lds);
        else {
            return false;
        }
    }

    /**
     * Verifies if the Legacy Data is valid and saves
     * @param lds new legacy data object
     * @return the addition of a new Legacy Data
     */
    public boolean saveNewLegacyData(LegacyData lds){
        if (validateNewLegacyData(lds)){
            return this.LegacyDataList.add(lds) && recordFile();
        }else{
            return false;
        }
    }

    public  boolean  recordFile(){
        try {
            ObjectOutputStream out=new ObjectOutputStream(new FileOutputStream("LegacyData.bin"));
            try {
                out.writeObject(LegacyDataList);
                return true;
            }finally {
                out.close();
            }
        }catch (IOException e){
            return false;
        }
    }

    /**
     * This method read data from a file.
     * @return a array of vaccine type
     */

    public  ArrayList<LegacyData> readFromFile(){
        ArrayList<LegacyData> lsdlist= new ArrayList<>();
        try {
            ObjectInputStream in=new ObjectInputStream(new FileInputStream("LegacyData.bin"));
            try {
                lsdlist= (ArrayList<LegacyData>) in.readObject();
            }finally {
                in.close();
            }
        }catch (IOException | ClassNotFoundException e){
            e.printStackTrace();
        }
        return lsdlist;
    }
}
