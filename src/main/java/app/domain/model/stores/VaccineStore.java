package app.domain.model.stores;

import app.domain.model.Vaccine;
import app.domain.model.VaccineType;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class VaccineStore {
    private List<Vaccine>VaccineList= new ArrayList<>();
    /**
     * @author Rodrigo Moreira <1211026@isep.ipp.pt>
     * @param name Name of the vaccine
     * @param brand Brand of the vaccine
     * @param code code of the vaccine
     * @return a new specified vaccine
     */

    public Vaccine createNewVaccine(String name, String brand, String code){
        return new Vaccine(name,brand,code);
    }

    public boolean verifyNewVaccine (Vaccine vaccine){
        if (vaccine != null){
            return ! this.VaccineList.contains(vaccine);
        }else {
            return false;
        }
    }

    public boolean saveNewVaccine(Vaccine vaccine) {
        if (verifyNewVaccine(vaccine)){
            return this.VaccineList.add(vaccine)&& recordFileVaccine();
        }else{
            return false;
        }
    }
    public List<Vaccine> getVaccinelist() {
        return VaccineList;
    }

    public  boolean  recordFileVaccine(){
        List<Vaccine> vclist= new ArrayList<>();
        vclist=readFromFileVaccine();
        VaccineList.addAll(vclist);
        try {
            ObjectOutputStream out=new ObjectOutputStream(new FileOutputStream("VaccineList.bin"));
            try {
                out.writeObject(VaccineList);
                return true;
            }finally {
                out.close();
            }
        }catch (IOException e){
            return false;
        }
    }

    public  List<Vaccine> readFromFileVaccine(){
        List<Vaccine> vlist= new ArrayList<>();
        try {
            ObjectInputStream in=new ObjectInputStream(new FileInputStream("VaccineList.bin"));
            try {
                vlist= (List<Vaccine>) in.readObject();
            }finally {
                in.close();
            }
        }catch (IOException | ClassNotFoundException e){
            e.printStackTrace();
        }
        return vlist;
    }


}
