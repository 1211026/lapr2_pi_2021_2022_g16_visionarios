package app.domain.model.stores;

import app.domain.model.VaccineType;

import java.io.*;
import java.util.ArrayList;

public class VaccineTypeStore {
     ArrayList<VaccineType> VaccineTypeList = new ArrayList<>();
    /**
     * @author Rui Milhazes <1211031@isep.ipp.pt>
     * @param code Code of the vaccine
     * @param designation Designation of the vaccine (Covid 19, Flu, Monkeypox)
     * @param duration Duration of the vaccine
     * @return a created Vaccine Type
     */
    public VaccineType createVaccineType(String code, String designation, int duration){
        return new VaccineType(code,designation, duration);
    }
    /**
     * @param vt Vaccine Type object
     * @return false if the Vaccine Type is null
     */

    public boolean validateVaccineType (VaccineType vt){
        if (vt == null)
            return false;
        return ! this.VaccineTypeList.contains(vt);
    }

    /**
     * Verifies if the Vaccine Type is valid and then adds it to the list
     * @param vt Vaccination Type Object
     * @return Returns the addiction of the vaccination type object to the list
     */

    public boolean saveVaccineType (VaccineType vt){
        if (!validateVaccineType(vt))
            return false;
        return this.VaccineTypeList.add(vt)&& recordFile();
    }

    public ArrayList<VaccineType> getVaccineTypeList() {
        return VaccineTypeList;
    }

    /**
     * This method record the vaccines type on a file.
     * @return true if the vaccine was recorded ,false if the file is not valid.
     */

    public  boolean  recordFile(){
        ArrayList<VaccineType> vtlist= new ArrayList<>();
        vtlist=readFromFile();
        VaccineTypeList.addAll(vtlist);
        try {
            ObjectOutputStream out=new ObjectOutputStream(new FileOutputStream("VaccineTypeList.bin"));
            try {
                out.writeObject(VaccineTypeList);
                return true;
            }finally {
                out.close();
            }
        }catch (IOException e){
            return false;
        }
    }

    /**
     * This method read data from a file.
     * @return an array of vaccine type
     */

    public  ArrayList<VaccineType> readFromFile(){
        ArrayList<VaccineType> vtlist= new ArrayList<>();
        try {
            ObjectInputStream in=new ObjectInputStream(new FileInputStream("VaccineTypeList.bin"));
            try {
                vtlist= (ArrayList<VaccineType>) in.readObject();
            }finally {
                in.close();
            }
        }catch (IOException | ClassNotFoundException e){
            e.printStackTrace();
        }
        return vtlist;
    }
}
