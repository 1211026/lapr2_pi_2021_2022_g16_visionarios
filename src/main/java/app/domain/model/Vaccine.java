package app.domain.model;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

/**
     * @author Rodrigo Moreira <1211026@isep.ipp.pt>
     */

    public class Vaccine implements Serializable {
      /**
       * code of the Vaccine
     */
        private String code;
        /**
         * name of the Vaccine
         */
        private String name;

        /**
         * brand of the Vaccine
         */

        private String brand;

    /**
     * Constructor
     * Creates and instance of Vaccine receiving all the atributes
     */

        public Vaccine(String name, String brand,String code) {
            this.name = name;
            this.brand = brand;
            this.code= code;
        }

    /**
     * verify if the vaccine creater previously have name and brand.
     * @param name,brand name of the vaccine and brand that develop the vaccine.
     * @return if the administrator didn't insert the name or the brand the program alerts him to fill all the parameters.
     */

    public static void verifyNewVaccineName(String name) {
        if (StringUtils.isBlank(name))
            throw new IllegalArgumentException("Name cannot be blank.");
    }

    public static void verifyNewVaccineBrand(String brand) {
        if (StringUtils.isBlank(brand))
            throw new IllegalArgumentException("Brand cannot be blank.");
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCode() {
        return code;
    }
    public String toString(){
        return String.format("%s %s %s",name,brand,code);
    }

}



