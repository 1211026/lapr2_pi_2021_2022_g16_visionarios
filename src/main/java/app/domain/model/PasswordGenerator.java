package app.domain.model;

import java.util.Random;

public class PasswordGenerator {

    /**
     * @author Diogo Teixeira <1210916>
     * Method to generate a random password
     * @return
     */
    public String generatePassword(){
        StringBuilder password= new StringBuilder();
        Random random= new Random();
        char randomizedNumberOrAlphabetic=(char)(random.nextInt(2) +'0');

        int numCharPassword = 7;
        for (int i = 0; i < numCharPassword; i++) {
            char character = 0;
            if (randomizedNumberOrAlphabetic==48) {
                character=(char) (random.nextInt(10) + '0');
            }
            if (randomizedNumberOrAlphabetic==49){
                character= (char) (random.nextInt(26) + 'A');
            }
            password.append(character);
        }

        return password.toString();
    }
}
