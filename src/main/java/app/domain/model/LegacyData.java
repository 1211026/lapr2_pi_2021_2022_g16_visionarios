package app.domain.model;

import app.controller.CreateSNSUserController;
import app.domain.model.stores.LegacyDataStore;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;

public class LegacyData {
    /**
     * Method to read the legacy file
     * @param legacyFile the file the user will load
     * @throws IOException
     */

    private SNSUser SNSUser;
    private String vaccine;
    private String dose;
    private String lotNumber;
    private LocalDateTime scheduleDateTime;
    private LocalDateTime arrivalDateTime;
    private LocalDateTime nurseAdministrationDateTime;
    private LocalDateTime leavingDateTime;

    /**
     * LegacyDataStore Constructor
     * @param SNSUser sns user
     * @param vaccine vaccine
     * @param dose dose
     * @param lotNumber lot number
     * @param scheduleDateTime scheduled time
     * @param arrivalDateTime arrival time
     * @param nurseAdministrationDateTime nurse administrated time
     * @param leavingDateTime leaving time
     */

    public LegacyData(app.domain.model.SNSUser SNSUser, String vaccine, String dose, String lotNumber, LocalDateTime scheduleDateTime, LocalDateTime arrivalDateTime, LocalDateTime nurseAdministrationDateTime, LocalDateTime leavingDateTime) {
        this.SNSUser = SNSUser;
        this.vaccine = vaccine;
        this.dose = dose;
        this.lotNumber = lotNumber;
        this.scheduleDateTime = scheduleDateTime;
        this.arrivalDateTime = arrivalDateTime;
        this.nurseAdministrationDateTime = nurseAdministrationDateTime;
        this.leavingDateTime = leavingDateTime;
    }

    public app.domain.model.SNSUser getSNSUser() {
        return SNSUser;
    }

    public String getVaccine() {
        return vaccine;
    }

    public String getDose() {
        return dose;
    }

    public String getLotNumber() {
        return lotNumber;
    }

    public LocalDateTime getScheduleDateTime() {
        return scheduleDateTime;
    }

    public LocalDateTime getArrivalDateTime() {
        return arrivalDateTime;
    }

    public LocalDateTime getNurseAdministrationDateTime() {
        return nurseAdministrationDateTime;
    }

    public LocalDateTime getLeavingDateTime() {
        return leavingDateTime;
    }

    /**
     * Method to validate the contents in each line of the file
     * @param line is a line of the file
     * @return a new legacy data store
     * @throws IllegalArgumentException
     */

    public static LegacyData validateLine(String line) throws IllegalArgumentException  {
        CreateSNSUserController createSNSUserController=new CreateSNSUserController();
        List<SNSUser> snsUserList=new ArrayList<>();
        snsUserList=createSNSUserController.readFromFile();

        String[] columns = line.split(";");
        if (columns.length != 8) {
            throw new IllegalArgumentException("The inserted file doesn't have 8 columns.");
        }

        int snsUserNumber ;
        snsUserNumber = Integer.parseInt(columns[0]);
        String vaccine = columns[1];
        String dose = columns[2];
        String lotNumber = columns[3];
        LocalDateTime ScheduledTime;
        LocalDateTime ArrivalTime;
        LocalDateTime NurseAdministrationTime;
        LocalDateTime LeavingTime;
        SNSUser user = snsUserList.stream().filter(x -> x.getId() == snsUserNumber).findFirst().orElse(null);
        if (user == null) {
            throw new IllegalArgumentException("This user is not registered in our system.");
        }

        try
        {ScheduledTime = LocalDateTime.parse(columns[4], DateTimeFormatter.ofPattern("M/dd/yyyy H:mm"));
        } catch (DateTimeParseException ex) {
            throw new IllegalArgumentException("This scheduled date time is invalid");
        }

        try
        {ArrivalTime = LocalDateTime.parse(columns[5], DateTimeFormatter.ofPattern("M/dd/yyyy H:mm"));
        } catch (DateTimeParseException ex) {
            throw new IllegalArgumentException("This arrival date time is invalid.");
        }

        try
        {NurseAdministrationTime = LocalDateTime.parse(columns[6], DateTimeFormatter.ofPattern("M/dd/yyyy H:mm"));
        } catch (DateTimeParseException ex) {
            throw new IllegalArgumentException("This nurse administration time is invalid.");
        }

        try
        {LeavingTime = LocalDateTime.parse(columns[7], DateTimeFormatter.ofPattern("M/dd/yyyy H:mm\r"));
        } catch (DateTimeParseException ex) {
            throw new IllegalArgumentException("This leaving time is invalid.");
        }

        return new LegacyData(user, vaccine, dose, lotNumber, ScheduledTime, ArrivalTime, NurseAdministrationTime, LeavingTime);
    }
    /**
     * Method to call the readLine method
     * @param legacyFile
     * @return a file
     * @throws IOException
     */

    public static String fileReader(File legacyFile) throws IOException {
        return Files.readString(legacyFile.toPath());
    }

    /**
     * Method to split the file by lines and put it into an array
     * @param fileContents are what's in the file
     * @return a legacySystems array
     */

    public static List<LegacyData> readLine(String fileContents) {
        String[] split = fileContents.split("\n");
        boolean header = false;
        List<LegacyData> legacySystems = new ArrayList<>();
        for (String line : split) {
            if (!header) {
                header = true;
                continue;
            }

            LegacyData ls = validateLine(line);
            legacySystems.add(ls);
        }
        return legacySystems;
    }

    /**
     * Method to string
     * @return a to string format
     */

    public String toString(){
        return String.format("%s %s %s %s %s %s %s %s",SNSUser.getName(), vaccine, dose, lotNumber, scheduleDateTime, arrivalDateTime, nurseAdministrationDateTime, leavingDateTime);
    }

    /**
     * Method compareTo
     * @return res
     */

    public int compareTo() {
        int res = 0;
        if (this.getArrivalDateTime().isBefore(getArrivalDateTime())) {
            res =- 1;
        }
        if (this.getArrivalDateTime().isAfter(getArrivalDateTime())) {
            res = 1;
        }
        return res;
    }
}
