package app.domain.model;

import java.io.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;
import app.controller.CreateSNSUserController;

/**
 * @author Rui Milhazes <1211031@isep.ipp.pt>
 * file paths:
 * C:\Users\ruimm\OneDrive\Documentos\EXCEL\CSVfileWHEADER.csv
 * C:\Users\ruimm\OneDrive\Documentos\EXCEL\CSVfileNOheader.csv
 * C:\Users\ruimm\OneDrive\Documentos\SNSUserDataFromGaoFuNationalCenterDoPortoVaccinationCenter1.csv
 * C:\Users\rodri\OneDrive\Ambiente de Trabalho\SNSUserDataFromGaoFuNationalCenterDoPortoVaccinationCenter.csv
 *
 */

public class CSVReader extends CreateSNSUserController {

    /**
     * Method to check if the CSV file has a header
     * @param csvFile the file that contains the users
     * @throws IOException i
     */

    public static boolean checkHeader(String csvFile) throws IOException {
        File file = new File(csvFile);
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String line = br.readLine();
        String[] tempArr;
        boolean header;
        tempArr = line.split(";");
        String line1column5 = "PhoneNumber";
        header = line1column5.equals(tempArr[4]);
        br.close();
        return header;
        }


    /**
     * Method to read the file and load the users using the SNSUser, and createSNSUserController classes
     * @param csvFile the file that contains the users
     * @throws IOException i
     */

    public static void read(String csvFile) throws IOException {
        CreateSNSUserController sns = new CreateSNSUserController();
        ArrayList<String> ar = new ArrayList<>();
        FileReader fr=new FileReader(csvFile);
        BufferedReader br = new BufferedReader(fr);
        String line;
        StringTokenizer st;
        if (checkHeader(csvFile)) {
            br.readLine();
        }
        int c = 0;
            while ((line=br.readLine()) != null) {

                st = new StringTokenizer(line, ";");
                while (st.hasMoreTokens()) {
                    String sd = st.nextToken();
                    if (sd != null) {
                        ar.add(sd);
                    }
                }
                String name = ar.get(c++);
                //System.out.println(name);
                String date;
                String sex = ar.get(c++);
                if(!sex.equals("Masculino")  && !sex.equals("Feminino")) {
                    //System.out.println(sex);
                    date = sex;
                    sex = "N/A";
                }else{
                    date = ar.get(c++);
                }
                //System.out.println(sex);
                LocalDate birthDate = LocalDate.parse(date, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
                //System.out.println(birthDate);
                String address = ar.get(c++);
                //System.out.println(address);
                int phoneNumber = Integer.parseInt(ar.get(c++));
                //System.out.println(phoneNumber);
                String email = ar.get(c++);
                //System.out.println(email);
                int SNSUserNumber = Integer.parseInt(ar.get(c++));
                //System.out.println(SNSUserNumber);
                int CitizenNumber = Integer.parseInt(ar.get(c++));
                //System.out.println(CitizenNumber);
                sns.createSNSUser(name, sex, birthDate, address, phoneNumber, email, SNSUserNumber, CitizenNumber);
                sns.saveSNSUser();
                }
        }
    /**
     * Method to read a CSV file without a header and print it
     * @param csvFile the file that contains the users
     */

    public static void readWithoutHeader(String csvFile) {

        try {
            File file = new File(csvFile);
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            String line;
            String[] tempArr;
            while ((line = br.readLine()) != null) {
                tempArr = line.split(";");
                for (String tempStr : tempArr) {

                    System.out.print(tempStr + " , ");
                }
                System.out.println();
            }
            br.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    /**
     * Method to read the CSV file with a header and print it
     * @param csvFile the file the user loaded
     */

    public static void readwithheader (String csvFile)  {
        try {
            File file = new File(csvFile);
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            String line;
            String[] tempArr;
            while ((line = br.readLine()) != null) {
                tempArr = line.split(";");
                for (String tempStr : tempArr) {
                    System.out.print(tempStr + " ; ");
                }
                System.out.println();
            }
            br.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}