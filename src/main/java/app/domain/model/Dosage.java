package app.domain.model;

import app.ui.console.utils.Utils;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

/**
 * @author Rodrigo Moreira <1211026@isep.ipp.pt>
 */

public class Dosage implements Serializable {
    /**
     * Number of doses per age group
     */
    private int numberOfDosesPerAgeGroup;

    /**
     * The dosage to be administrated of a certain vaccine
     */
    private int vaccineDosage;

    /**
     * Time interval between each dose
     */

    private int timeInterval;

    /**
     * Constructor
     * Creates and instance of Dosage receiving all the atributes
     */

    public Dosage(int numberOfDosesPerAgeGroup) {
        this.numberOfDosesPerAgeGroup = numberOfDosesPerAgeGroup;
        this.vaccineDosage = vaccineDosage;
        this.timeInterval = timeInterval;
    }

    public int getNumberOfDosesPerAgeGroup() {
        return numberOfDosesPerAgeGroup;
    }

    public void setNumberOfDosesPerAgeGroup(int numberOfDosesPerAgeGroup) {
        this.numberOfDosesPerAgeGroup = numberOfDosesPerAgeGroup;
    }

    public int getVaccineDosage() {
        return vaccineDosage;
    }

    public void setVaccineDosage(int vaccineDosage) {
        this.vaccineDosage = vaccineDosage;
    }

    public int getTimeInterval() {
        return timeInterval;
    }

    public void setTimeInterval(int timeInterval) {
        this.timeInterval = timeInterval;
    }


}
