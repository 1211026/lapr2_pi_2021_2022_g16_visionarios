package app.domain.model;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

/**
 * @author Rui Milhazes <1211031@isep.ipp.pt>
 */
public class VaccineType implements Serializable {
    /**
     * code of the Vaccine Type
     */
    private String code;
    /**
     * designation of the Vaccine Type
     */
    private String designation;

    /**
     * duration of the Vaccine Type
     */
    private int duration;

    private int minAge = 13;

    public VaccineType(String code, String designation, int duration) {
        checkCodeRules(code);
        checkCodeRules(designation);
        this.code = code;
        this.designation = designation;
        this.duration = duration;

    }
    /**
     * This method contains the rules for the code
     */
    private void checkCodeRules(String code) {
        if (StringUtils.isBlank(code))
            throw new IllegalArgumentException("Code cannot be blank.");
        if ( (code.length() < 4) || (code.length() > 8))
            throw new IllegalArgumentException("Code must have 4 to 8 chars.");
    }

    /**
     * Sets and Gets for the code attribute
     * @return
     */
    private void checkDesignationRules(String designation){}
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Gets and Sets for the designation attribute
     * @return
     */
    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    /**
     * Gets and Sets for the Duration attribute
     * @return
     */
    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
    public String toString(){
        return String.format("%s %s %d",code,designation,duration);
    }
}
