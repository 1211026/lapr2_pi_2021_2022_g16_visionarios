package app.domain.model;

import java.io.Serializable;
import java.sql.Time;

/**
 * @author Rodrigo Ferreira <1211030@isep.ipp.pt>
 */
public class VaccinationCenter implements Serializable {

    private static final long serialVersionUID = 7266260775786184873L;
    /**
     * Name of the Vaccination Center
     */
    private String name;
    /**
     * Address of the Vaccination Center
     */
    private String address;
    /**
     * Phone Number of the Vaccination Center
     */
    private int phoneNumber;
    /**
     * Email of the Vaccination Center
     */
    private String email;
    /**
     * Fax of the Vaccination Center
     */
    private int faxNumber;
    /**
     * Website Address of the Vaccination Center
     */
    private String websiteAddress;
    /**
     * Opening and closing hours of the Vaccination Center
     */
    private Time openingHours;
    private Time closingHours;
    /**
     * Slot Duration of the Vaccination Center
     */
    private int slotDuration;
    /**
     * Max slot vaccines of the Vaccination Center
     */
    private int maxSlotVaccines;

    /**
     * Constructor
     * Creates and instance of VaccinationCenter receiving all the attributes
     */
    public VaccinationCenter(String name, String address, int phoneNumber, String email, int faxNumber, String websiteAddress, Time openingHours, Time closingHours, int slotDuration, int maxSlotVaccines) {
        this.name = name;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.faxNumber = faxNumber;
        this.websiteAddress = websiteAddress;
        this.openingHours = openingHours;
        this.closingHours = closingHours;
        this.slotDuration = slotDuration;
        this.maxSlotVaccines = maxSlotVaccines;
    }

    public String getOpeningHours() {
        return openingHours.toString();
    }

    public String getClosingHours() {
        return closingHours.toString();
    }

    public int getSlotDuration() {
        return slotDuration;
    }

    public int getMaxSlotVaccines() {
        return maxSlotVaccines;
    }
    public String toString(){
        return String.format("%s %s %d %s %d %s %s %s %d %d",name,address,phoneNumber,email,faxNumber,websiteAddress,openingHours,closingHours,slotDuration,maxSlotVaccines);
    }

    public String getName(){return name;}
}
