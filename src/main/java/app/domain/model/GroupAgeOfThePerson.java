package app.domain.model;

import java.io.Serializable;

/**
 * @author Rodrigo Moreira <1211026@isep.ipp.pt>
 */

public class GroupAgeOfThePerson implements Serializable {

    /**
     * Group age of the person that will be vaccinated
     */
    private String groupAge;

    /**
     * Constructor
     * Creates and instance of Group age of the person receiving all the atributes
     */

    public GroupAgeOfThePerson(String groupAge) {
        this.groupAge = groupAge;
    }

    public String getGroupAge() {
        return groupAge;
    }

    public void setGroupAge(String groupAge) {
        this.groupAge = groupAge;
    }
}
