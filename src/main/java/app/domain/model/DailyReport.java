package app.domain.model;

import app.controller.App;
import app.domain.model.stores.NumberFullyVaccinatedStore;
import app.domain.model.stores.VaccinationCenterStore;
import app.domain.model.stores.VaccineAdministrationStore;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TimerTask;

/**
 * @author Gabriel Costa e Silva <1210808@isep.ipp.pt>
 */
public class DailyReport extends TimerTask {
    private NumberFullyVaccinatedStore numberFullyVaccinatedStore= new NumberFullyVaccinatedStore();
    /**
     * Method that runs the daily report
     */
    public void run()
    {
        VaccinationCenterStore vcStore = App.getInstance().getCompany().getVaccinationCenterStore();
        ArrayList<VaccinationCenter> listCenters = vcStore.readFromFile();

        VaccineAdministrationStore vaStore = App.getInstance().getCompany().getVaccineAdministrationStore();
        ArrayList<VaccineAdministration> listAdministrations = new ArrayList<>();
        VaccineAdministration ce = new VaccineAdministration(127846781, "Covid", "Pfizer", 23, "OK", LocalDateTime.of(2022,06,17,8,0), listCenters.get(0));
        listAdministrations.add(ce);

        System.out.println("Daily Report");

        //HashMap that will store the vaccination centers and its respective number of people vaccinated
        HashMap<String, Integer> result;
        result = getResult(listCenters, listAdministrations);
        System.out.println(result);
        try
        {
            saveCSVFile(result);
        }catch (FileNotFoundException e){
            System.out.println("The file was not found");
        }
    }

    /**
     * Method that will save the CSV File of the Daily Report
     *
     * @param result HashMap containing the data to store in the CSVFile
     * @throws FileNotFoundException if the file is not found
     */

    public void saveCSVFile(HashMap<String, Integer> result) throws FileNotFoundException {
        File file = new File("DailyReport_" + LocalDate.now() + ".csv");
        PrintWriter out = new PrintWriter(file);
        out.print(LocalDateTime.now());
        out.print(";");
        for (Map.Entry<String, Integer> entry : result.entrySet()) {
            String key = entry.getKey();
            Integer value = entry.getValue();
            out.print(key + " : " + value);
        }
        out.println();
        out.close();
    }

    /**
     * Method that will compute the result of the daily report
     *
     * @param listCenters List containing the vaccination centers
     * @param listAdministrations List of the vaccine administrations
     * @return returns the result computed to the run() method of the class
     *
     */
    public HashMap<String, Integer> getResult(ArrayList<VaccinationCenter> listCenters, ArrayList<VaccineAdministration> listAdministrations)
    {
        //Count
        int count = 0;
        int totalCount=0;
        LocalDate today= LocalDate.now();
        HashMap<String, Integer> result = new HashMap<>();
        System.out.println("Number of people vaccinated today at: \n");
        for(VaccinationCenter b: listCenters)
        {
            for(VaccineAdministration c: listAdministrations){
                totalCount++;
                if(c.getVaccinationCenter().equals(b) && c.getDateOfAdministration().toLocalDate().equals(LocalDateTime.now().toLocalDate()))
                {
                    count++;
                }
            }
            result.put(b.getName(), count);
            count = 0;
        }
        numberFullyVaccinatedStore.addNumberFullyVaccinatedPerDay(today, totalCount);
        return result;
    }
}
