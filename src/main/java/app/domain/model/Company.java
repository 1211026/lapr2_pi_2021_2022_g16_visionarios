package app.domain.model;

import app.domain.model.stores.LegacyDataStore;
import app.domain.model.stores.VaccinationCenterStore;
import app.domain.model.stores.VaccineAdministrationStore;
import app.ui.gui.LegacyDataFXController;
import pt.isep.lei.esoft.auth.AuthFacade;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */

public class Company {


    private String designation;
    private AuthFacade authFacade;
    private final VaccinationCenterStore vcs;
    private final VaccineAdministrationStore vas;
    private final LegacyDataStore lds;

    public Company(String designation)
    {
        if (StringUtils.isBlank(designation))
            throw new IllegalArgumentException("Designation cannot be empty.");

        this.designation = designation;
        this.authFacade = new AuthFacade();
        vcs = new VaccinationCenterStore();
        vas = new VaccineAdministrationStore();
        lds = new LegacyDataStore();
    }

    public String getDesignation() {
        return designation;
    }

    public AuthFacade getAuthFacade() {
        return authFacade;
    }

    public void scheduleDailyReport(String time)
    {
        DailyReportTimer thread = new DailyReportTimer(time);
        thread.start();
    }


    /**
     * Method that gets the vaccination center store of the company
     *
     * @return Returns the vaccination center store of the company
     */

    public VaccinationCenterStore getVaccinationCenterStore()
    {
        return this.vcs;
    }

    /**
     * Method that gets the vaccine administration store of the company
     *
     * @return Returns the vaccine administration store of the company
     */

    public VaccineAdministrationStore getVaccineAdministrationStore()
    {
        return this.vas;
    }

    public LegacyDataStore getLegacyDataStore(){
        return this.lds;
    }
}

