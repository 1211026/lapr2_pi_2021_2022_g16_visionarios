package app.domain.shared;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Constants {
    public static final String ROLE_ADMIN = "Administrator";
    public static final String ROLE_NURSE = "Nurse";
    public static final String ROLE_RECEP = "Receptionist";
    public static final String ROLE_USER= "SNSUser";
    public static final String ROLE_COORD= "Center Coordinator";


    public static final String PARAMS_FILENAME = "config.properties";
    public static final String PARAMS_COMPANY_DESIGNATION = "Company.Designation";
    public static final String PARAMS_DAILY_REPORT_TIME = "DailyReportTime";
}
