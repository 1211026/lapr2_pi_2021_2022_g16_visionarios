package app.controller;

import app.domain.model.Company;
import app.domain.model.DosageNumberDosageGroupAge;
import app.domain.model.stores.DosaNumberDosageGroupAgeStore;
import java.util.ArrayList;

public class DosageNumberDosageGroupAgeController {
    private Company company;
    private DosaNumberDosageGroupAgeStore dosaNumberDosageGroupAgeStore=new DosaNumberDosageGroupAgeStore();
    private DosageNumberDosageGroupAge dosageNumberDosageGroupAge;
    public DosageNumberDosageGroupAgeController () {
        this(App.getInstance().getCompany());
    }

    public DosageNumberDosageGroupAgeController(Company company) {
        this.company = company;
        this.dosageNumberDosageGroupAge = null;
    }

    /**
     * Creates a New Vaccine and verifies if it is valid.
     * @param name Name of the vaccine
     * @param brand Brand of the vaccine
     * @param code code of the vaccine
     * @param dosageNumber dosage of the vaccine in mL.
     * @param groupAge group age for the vaccine.
     * @param dosagePerGroupAge dosage for each group.
     * @return a created Vaccine.
     */

    public boolean createDosageNumberDosageGroupAge(String name, String brand, String code,int dosageNumber,String groupAge,int dosagePerGroupAge){
        this.dosageNumberDosageGroupAge=this.dosaNumberDosageGroupAgeStore.createDosageNumberDosageGroupAge(name,brand,code,dosageNumber,groupAge,dosagePerGroupAge);
        return this.dosaNumberDosageGroupAgeStore.validateDosageNumberDosageGroupAge(dosageNumberDosageGroupAge);
    }
    /**
     * Saves the Vaccine.
     * @return vaccine list.
     */

    public boolean saveDosageNumberDosageGroupAge(){
        return this.dosaNumberDosageGroupAgeStore.saveDosageNumberDosageGroupAge(dosageNumberDosageGroupAge);
    }
    public ArrayList<DosageNumberDosageGroupAge> getVaccineTypeList() {
        return this.dosaNumberDosageGroupAgeStore.getDosageNumberDosageGroupAge();
    }
    /**
     * This method record the vaccines on a file.
     * @return true if the vaccine was recorded ,false if the file is not valid.
     */
    public boolean recordFile(){
        return this.dosaNumberDosageGroupAgeStore.recordFile();
    }
    /**
     * This method read data from a file.
     * @return an array of vaccine.
     */
    public ArrayList<DosageNumberDosageGroupAge> readFromFile(){
        return this.dosaNumberDosageGroupAgeStore.readFromFile();
    }
}

