package app.controller;
import app.domain.model.LegacyDataSort;
import app.domain.model.stores.LegacyDataStore;
import app.ui.console.utils.Utils;
import java.io.File;
import java.io.IOException;
import java.util.Objects;

/**
 * @author Rui Milhazes <1211031@isep.ipp>
 */

public class LegacyDataController {

    /**
     * Method to check which option the user chooses
     */

    public static String optionCheck() {
        System.out.println("Option A - Bubble sort.");
        System.out.println("Option B - Quick sort.");
        String sort = Utils.readLineFromConsole("What sorting method do you prefer?");
        System.out.println("");
        return sort;
    }

    /**
     * Method to call the methods to the corresponding options
     * @param legacyFile the file loaded by the user
     */

    public static void legacyDataController(File legacyFile) throws IOException {
        String sort = optionCheck();
        while (!Objects.equals(sort, "A") || !sort.equals("B")) {
            System.out.println("Please choose one of the options below");
            if (Objects.equals(sort, "A")) {
                System.out.println("You have chosen Bubble sort:");
                System.out.println(" ");
                LegacyDataSort.bubbleSort(legacyFile);
                System.out.println("");
                break;
            } else {
                if (Objects.equals(sort, "B")) {
                    System.out.println("You have chosen Quick sort:");
                    System.out.println(" ");
                    LegacyDataSort.quickSort(legacyFile);
                    System.out.println("");
                    break;
                } else {
                    if (!Objects.equals(sort, "A") && !Objects.equals(optionCheck(), "B")) ;
                    {
                        System.out.println("Please choose one of the options below");
                        System.out.println(" ");
                    }
                }
            }
        }
    }
}