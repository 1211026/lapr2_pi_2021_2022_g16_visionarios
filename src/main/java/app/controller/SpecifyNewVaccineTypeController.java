package app.controller;
import app.domain.model.Company;
import app.domain.model.VaccineType;
import app.domain.model.stores.VaccineTypeStore;

import java.util.ArrayList;

/**
 * @author Rui Milhazes <1211031@isep.ipp.pt>
 */

public class SpecifyNewVaccineTypeController {
    private Company company;
    private VaccineTypeStore vaccineTypeStore=new VaccineTypeStore();
    private VaccineType vt;
    public SpecifyNewVaccineTypeController () {
        this(App.getInstance().getCompany());
    }

    public SpecifyNewVaccineTypeController(Company company) {
        this.company = company;
        this.vt = null;
    }

    /**
     * Creates a New Vaccine Type and verifies if it is valid
     * @param code
     * @param designation
     * @param duration
     * @return validatevaccine
     */

    public boolean createVaccineType(String code, String designation, int duration){
        this.vt=this.vaccineTypeStore.createVaccineType(code, designation, duration);
        return this.vaccineTypeStore.validateVaccineType(vt);
    }
    /**
     * Saves the Vaccine Type
     * @return vaccine type list
     */

    public boolean saveVaccineType(){
        return this.vaccineTypeStore.saveVaccineType(vt);
    }
    public ArrayList<VaccineType> getVaccineTypeList() {
        return this.vaccineTypeStore.getVaccineTypeList();
    }
    /**
     * This method record the vaccines type on a file.
     * @return true if the vaccine was recorded ,false if the file is not valid.
     */
    public boolean recordFile(){
        return this.vaccineTypeStore.recordFile();
    }
    /**
     * This method read data from a file.
     * @return an array of vaccine type
     */
    public ArrayList<VaccineType> readFromFile(){
        return this.vaccineTypeStore.readFromFile();
    }
}
