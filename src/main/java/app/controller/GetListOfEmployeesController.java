package app.controller;

import app.domain.model.Company;
import app.domain.model.Employee;
import app.domain.model.stores.EmployeeStore;

import java.util.List;

/**
 * @author Gabriel Silva <1210808>
 */
public class GetListOfEmployeesController {
    private Company company;
    private EmployeeStore employeeStore = new EmployeeStore();

    public GetListOfEmployeesController() {this(App.getInstance().getCompany());}

    public GetListOfEmployeesController(Company company) {
        this.company = company;

    }
    public List<Employee> getEmployeeList(String s)
    {
        return this.employeeStore.getEmployeeRoleList(s);
    }
}
