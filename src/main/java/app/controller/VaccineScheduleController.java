package app.controller;
import app.domain.model.*;
import app.domain.model.stores.VaccinationScheduleStore;

import java.io.FileNotFoundException;
import java.util.ArrayList;

/**
 * @author Rodrigo Ferreira <1211030@isep.ipp.pt>
 */
public class VaccineScheduleController{
        private Company company;

        private VaccineSchedule vs;
    private VaccinationScheduleStore vaccinationScheduleStore=new VaccinationScheduleStore();

    public VaccineScheduleController () {
        this(App.getInstance().getCompany());
    }

        public VaccineScheduleController(Company company) {
            this.company = company;
            this.vs=null;
        }

        /**
         *Creates a Vaccine Schedule for the SNS User and verifies if is valid.
         * @param snsUserNumber The SNS user number
         * @param vc Vaccination Center provided
         * @param data The data for the vaccination schedule
         * @param time The hour for the vaccination schedule
         * @param vaccineType The type of vaccine that it is going to be administrated
         * @return a created Vaccine Schedule.
         */

        public boolean createVaccineSchedule(int snsUserNumber, VaccinationCenter vc, String data, String time, VaccineType vaccineType) {
            this.vs = this.vaccinationScheduleStore.createVaccineSchedule(snsUserNumber,vc,data,time,vaccineType);
            return this.vaccinationScheduleStore.validateVaccineSchedule(vs);
        }

        /**
        * Creates a Vaccine Schedule by the Receptionist and verifies if it's valid.
        * @param snsUserNumber The SNS user number
        * @param vc Vaccination Center provided
        * @param data The data for the vaccination schedule
        * @param time The hour for the vaccination schedule
        * @param vaccineType The type of vaccine that it is going to be administrated
        * @return a created Vaccine Schedule
        */

        public boolean createVaccineScheduleReceptionist(int snsUserNumber, VaccinationCenter vc, String data, String time, VaccineType vaccineType) {
            this.vs = this.vaccinationScheduleStore.createVaccineSchedule(snsUserNumber,vc,data,time,vaccineType);
            return this.vaccinationScheduleStore.validateVaccineScheduleReceptionist(vs);
        }

        /**
         * Saves the vaccine schedule.
         * @return saves and adds the schedule to the list if is valid.
         */

        public boolean saveVaccineSchedule() {
            return this.vaccinationScheduleStore.saveVaccineSchedule(vs);
        }

        /**
         * Verifies if the Sns user has more than one equal schedule.
         * @return false if there is more than one equal schedule, true if there is just one schedule.
         */

        public boolean checkSameVaccine(){
            return this.vaccinationScheduleStore.checkSameVaccine(vs);
        }

        /**
         * Verifies for the time chosen if the slot is full, calls the method from the company.
         * @return false if the slot is full, true if the slot is not full.
         */

        public boolean checkMaxSlot(){
            return this.vaccinationScheduleStore.checkMaxSlot(vs);
        }

        /**
         *Verifies for the time chosen the slot duration and if the time is available,calls the method from the company.
         * @return false if the time is not available, true if the time is available.
         */

        public boolean checkSlotDuration(){
            return this.vaccinationScheduleStore.checkSlotDuration(vs);
        }

        public void sendSMS() throws FileNotFoundException {
            this.vaccinationScheduleStore.sendSms(vs);
        }

    public ArrayList<VaccineSchedule> getVslist() {
        return this.vaccinationScheduleStore.getVslist();
    }
    /**
     * This method record the vaccines schedule on a file.
     * @return true if the vaccine was recorded ,false if the file is not valid.
     */
    public boolean recordFile(){
        return this.vaccinationScheduleStore.recordFile();
    }
    /**
     * This method read data from a file.
     * @return an array of vaccine schedule
     */
    public ArrayList<VaccineSchedule> readFromFile(){
        return this.vaccinationScheduleStore.readFromFile();
    }
}

