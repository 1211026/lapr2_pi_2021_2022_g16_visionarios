package app.controller;

//import app.domain.model.NumberFullyVaccinatedConvertedToFile;
import app.domain.model.ConvertToFile;
import app.domain.model.ValidateDates;

import java.io.IOException;

/**
 * @author Diogo Teixeira<1210916@isep.ipp.pt>
 */
public class NumberFullyVaccinatedController {

    private ValidateDates vd= new ValidateDates();

    private ConvertToFile nfvFile= new ConvertToFile();

    /**
     * Validate if the range of days is possible and if the values entered are valid
     * @param firstDay First day of the range
     * @param lastDay Last day of the range
     */
    public int ValidateInterval(String firstDay,String lastDay){
        int res;
        res=vd.ValidateInterval(firstDay,lastDay);
        return res;
    }
    /**
     * Convert the number of fully vaccinated of each day for a CSV file
     * @param firstDay First day of the range
     * @param lastDay Last day of the range
     */
    public void ConvertNumberOfFullyVaccinatedToFile(String firstDay,String lastDay) throws IOException {
        nfvFile.ValidateIntervalOnSystem(firstDay,lastDay);
    }
}
