package app.controller;

import app.domain.model.*;
import app.domain.model.stores.VaccineAdministrationStore;

import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class VaccineAdministrationController {
    private Company company;
    private VaccineAdministration va;
    private VaccineAdministrationStore vaccineAdministrationStore= new VaccineAdministrationStore();

    public VaccineAdministrationController () {
        this(App.getInstance().getCompany());
    }

    public VaccineAdministrationController(Company company) {
        this.company = company;
        this.va=null;
    }

    /**
     *Creates a Vaccine Schedule for the SNS User and verifies if is valid.
     * @param vaccineType vaccine type for the administration.
     * @param vaccineName vaccine name for the administration.
     * @param dose of the vaccine.
     * @param loteNumber of the vaccine.
     * @return a created Vaccine Administration.
     */

    public boolean createVaccineAdministration(int snsUserNumber, String vaccineType, String vaccineName, int dose, String loteNumber, LocalDateTime dateOfAdministration, VaccinationCenter vaccinationCenter) {
        this.va = this.vaccineAdministrationStore.createVaccineAdministration(snsUserNumber,vaccineType,vaccineName,dose,loteNumber,dateOfAdministration,vaccinationCenter);
        return this.vaccineAdministrationStore.validateVaccineAdministration(va);
    }

    /**
     * Saves the vaccine administration.
     * @return saves and adds the administration to the list if is valid.
     */

    public boolean saveVaccineAdministration() {
        return this.vaccineAdministrationStore.saveVaccineAdminstration(va);
    }

    /**
     * Method to send a sms to a SNS user calls the method from store.
     * @throws FileNotFoundException If the file is not found.
     */
    public void sendSms(){
        try {
            this.vaccineAdministrationStore.sendSMS();
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Method to help the nurse check the user´s name, age, adverse reactions and the vaccine schedule calls the method from store.
     * @param snsUserNumber of the person to be vaccinated.
     */
    public String checkUserInfo(int snsUserNumber){
        return this.vaccineAdministrationStore.checkUserInfo(snsUserNumber);
    }

    /**
     * This method helps the nurse choose the best vaccine to administer according to group age and vaccine schedule calls the method from store.
     * @param vaccineType schedule
     * @param age of the user
     */
    public ArrayList<DosageNumberDosageGroupAge> checkVaccineToGive(String vaccineType, int age){
       return this.vaccineAdministrationStore.checkVaccineToGive(vaccineType,age);
    }

    /**
     * This method helps the nurse to choose the dose of vaccine.
     * @param snsUserNumber
     * @return the dose of the vaccine.
     */
    public int checkDose(int snsUserNumber){
        return this.vaccineAdministrationStore.checkDose(snsUserNumber);
    }

    /**
     * This method calculates the SNS user age calls the method from the store.
     * @param snsUserNumber
     * @return the age of the SNS user.
     */
    public double ageOfPerson(int snsUserNumber){
        return this.vaccineAdministrationStore.ageOfPerson(snsUserNumber);
    }
    /**
     * This method record the vaccines administer on a file.
     * @return true if the vaccine was recorded ,false if the file is not valid.
     */

    public VaccinationCenter checkVaccinationCenter(int snsUserNumber){
        return this.vaccineAdministrationStore.checkVaccinationCenter(snsUserNumber);
    }
    public boolean recordFile(){
        return this.vaccineAdministrationStore.recordFile();
    }
    /**
     * This method read data from a file.
     * @return an array of vaccine administration
     */
    public ArrayList<VaccineAdministration> readFromFile(){
        return this.vaccineAdministrationStore.readFromFile();
    }

}
