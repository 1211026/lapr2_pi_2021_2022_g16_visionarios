package app.controller;

import app.domain.model.Company;
import app.domain.model.stores.VaccinationScheduleStore;
import app.domain.shared.Constants;
import pt.isep.lei.esoft.auth.AuthFacade;
import pt.isep.lei.esoft.auth.UserSession;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class App {

    private Company company;
    private AuthFacade authFacade;
    private VaccinationScheduleStore vaccinationScheduleStore;
    private ConsultTheListOfSNSusersController app;
    private Properties props = getProperties();
    private App()
    {
        this.company = new Company(props.getProperty(Constants.PARAMS_COMPANY_DESIGNATION));
        this.authFacade = this.company.getAuthFacade();
        bootstrap();
    }

    public Company getCompany()
    {
        return this.company;
    }

    public ConsultTheListOfSNSusersController getTheListOfSNSUsers() {
        return app;
    }

    public VaccinationScheduleStore getVaccinationScheduleStore(){return  this.vaccinationScheduleStore;}


    public UserSession getCurrentUserSession()
    {
        return this.authFacade.getCurrentUserSession();
    }

    public boolean doLogin(String email, String pwd)
    {
        return this.authFacade.doLogin(email,pwd).isLoggedIn();
    }

    public void doLogout()
    {
        this.authFacade.doLogout();
    }

    private Properties getProperties()
    {
        Properties props = new Properties();

        // Add default properties and values
        props.setProperty(Constants.PARAMS_COMPANY_DESIGNATION, "DGS/SNS");


        // Read configured values
        try
        {
            InputStream in = new FileInputStream(Constants.PARAMS_FILENAME);
            props.load(in);
            in.close();
        }
        catch(IOException ex)
        {

        }
        return props;
    }

    private void bootstrap()
    {
        this.authFacade.addUserRole(Constants.ROLE_ADMIN,Constants.ROLE_ADMIN);
        this.authFacade.addUserRole(Constants.ROLE_RECEP,Constants.ROLE_RECEP);
        this.authFacade.addUserRole(Constants.ROLE_NURSE,Constants.ROLE_NURSE);
        this.authFacade.addUserRole(Constants.ROLE_USER,Constants.ROLE_USER);
        this.authFacade.addUserRole(Constants.ROLE_COORD,Constants.ROLE_COORD);
        this.authFacade.addUserWithRole("Diogo Teixeira", "1210916@isep.ipp.pt", "1234",Constants.ROLE_ADMIN);
        this.authFacade.addUserWithRole("Rui Milhazes", "1211031@isep.ipp.pt", "1234",Constants.ROLE_ADMIN);
        this.authFacade.addUserWithRole("Rodrigo Ferreira", "1211030@isep.ipp.pt", "1234",Constants.ROLE_ADMIN);
        this.authFacade.addUserWithRole("Rodrigo Moreira", "1211026@isep.ipp.pt", "1234",Constants.ROLE_ADMIN);
        this.authFacade.addUserWithRole("Gabriel Silva", "1210808@isep.ipp.pt", "1234",Constants.ROLE_ADMIN);
        this.authFacade.addUserWithRole("Nurse","nurse@nurse.pt","1234",Constants.ROLE_NURSE);
        this.authFacade.addUserWithRole("Receptionist","recepTeste1@recep.pt","1234",Constants.ROLE_RECEP);
        this.authFacade.addUserWithRole("SNSUser","user1@user.pt","1234",Constants.ROLE_USER);
        this.authFacade.addUserWithRole("CenterCoordinator", "coord@center.pt", "1234", Constants.ROLE_COORD);
    }

    // Extracted from https://www.javaworld.com/article/2073352/core-java/core-java-simply-singleton.html?page=2

    private static App singleton = null;

    public static App getInstance()
    {
        if(singleton == null)
        {
            synchronized(App.class)
            {
                singleton = new App();
            }
        }
        return singleton;
    }
    public void scheduleDailyReport(){
        company.scheduleDailyReport(props.getProperty(Constants.PARAMS_DAILY_REPORT_TIME));
    }
}
