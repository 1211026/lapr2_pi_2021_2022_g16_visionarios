package app.controller;

import app.domain.model.VaccineSchedule;
import app.domain.model.VaccineScheduleForToday;

/**
 * @author Diogo Teixeira<1210916@isep.ipp.pt>
 */
public class RegisterTheArrivalOfSNSUserController {
    private VaccineScheduleForToday vaccineScheduleForToday = new VaccineScheduleForToday();
    /**
     * Creates a Vaccination schedule for the day and verifies if none has been created.
     */
    public void createVaccinationScheduledForToday() {
        this.vaccineScheduleForToday.createVaccinationScheduledForToday();
    }

    /**
     * Verify if the SNS user data is valid
     * @param name
     * @param SNSUserNumber
     */

    public int verifySNSUser(String name, int SNSUserNumber) {
        int errorType=0;
        errorType=vaccineScheduleForToday.validateSNSUserNumber(SNSUserNumber);
        if (errorType==0)
        errorType=vaccineScheduleForToday.validateSNSUser(name,SNSUserNumber);
        return errorType;
    }
    /**
     * Save the name of SNS user who arrived and register him on waiting room
     */
    public void saveSNSUser() {
        vaccineScheduleForToday.saveArrivalOfSNSUser();
    }

}
