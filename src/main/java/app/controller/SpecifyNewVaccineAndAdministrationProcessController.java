package app.controller;

import app.domain.model.*;
import app.domain.model.stores.DosageNumberStore;
import app.domain.model.stores.DosagePerAgeGroupStore;
import app.domain.model.stores.GroupAgeStore;
import app.domain.model.stores.VaccineStore;

import java.util.ArrayList;
import java.util.List;


/**
 * @author Rodrigo Moreira <1211026@isep.ipp.pt>
 */

public class SpecifyNewVaccineAndAdministrationProcessController {
    private Company company;
    private VaccineStore vaccineStore1=new VaccineStore();
    private GroupAgeStore groupAgeStore=new GroupAgeStore();
    private DosagePerAgeGroupStore dosagePerAgeGroupStore= new DosagePerAgeGroupStore();
    private DosageNumberStore dosageNumberStore= new DosageNumberStore();
    private GroupAgeOfThePerson ageGroup;
    private Vaccine vaccine;
    private Dosage DosesPerAgeGroup;
    private Dosage vacDosage;

    public SpecifyNewVaccineAndAdministrationProcessController () {
        this(App.getInstance().getCompany());
    }

    /**
     * Constructor
     * @param company Company that will be received
     */

    public SpecifyNewVaccineAndAdministrationProcessController(Company company) {
        this.company = company;
        this.ageGroup=null;
        this.DosesPerAgeGroup=null;
        this.vacDosage=null;
        this.vaccine=null;
    }

    /**
     * Specify a new vaccine and verifies if is valid.
     * @param name the name of the vaccine.
     * @param brand the brand that developed the vaccine.
     * @param code the code of that specific vaccine.
     * @return the created vaccine and the boolean value that confirms if the vaccine is valid.
     */

    public boolean createNewVaccine(String name, String brand, String code){
        this.vaccine= this.vaccineStore1.createNewVaccine(name,brand,code);
        return this.vaccineStore1.verifyNewVaccine(vaccine);
    }

    /**
     * Save the new vaccine inserted in the previous method and verifies if is valid
     * There's no need parameter for this method
     * @return a boolean value to confirm if the program saved the vaccine inserted
     */

    public boolean saveNewVaccine(){
        return this.vaccineStore1.saveNewVaccine(vaccine);
    }

    public boolean recordFileVac(){
        return this.vaccineStore1.recordFileVaccine();
    }

    public List<Vaccine> readFromFileVac(){
        return this.vaccineStore1.readFromFileVaccine();
    }

    /**
     * Create a new Age group and verifies if is valid.
     * @param groupAge the age group that includes differente people from different ages.
     * @return the boolean value that confirms if the program created and verified the new age group.
     */

    public boolean createNewGroupAge(String groupAge){
        this.ageGroup= this.groupAgeStore.createNewGroupAge(groupAge);
        return this.groupAgeStore.verifyGroupAgeOfThePerson(ageGroup);
    }

    /**
     * Save the group age that was previous inserted.
     * There is no parameter for this method.
     * @return return the boolean value that confirms if the program saved the age group inserted.
     */

    public boolean saveGroupAgeOfThePerson(){
        return this.groupAgeStore.saveGroupAgeOfThePerson(ageGroup);
    }


    public boolean recordFile(){
        return this.groupAgeStore.recordFileGroupAge();
    }

    public List<GroupAgeOfThePerson> readFromFile(){
        return this.groupAgeStore.readFromFileGroupAge();
    }

    /**
     * Create a new dosage number of a especified vaccine for a certain age group and verifies if is valid.
     * @param numberOfDosesPerAgeGroup  represents the number of doses that each person that belongs to a specific age group needs to take of a certain vaccine.
     * @return if the value is true the program created the dosage per age group, if is false the program didnt created the DosagePerAgeGroup.
     */

    public boolean createDosagePerAgeGroup(int numberOfDosesPerAgeGroup){
        this.DosesPerAgeGroup= this.dosagePerAgeGroupStore.createDosagePerAgeGroup(numberOfDosesPerAgeGroup);
        return this.dosagePerAgeGroupStore.verifyDosagePerAgeGroup(DosesPerAgeGroup);
    }

    /**
     * Save the DosagePerAgeGroup inserted
     * There is no parameter needed
     * @return if its true the return the program saved the value inserted, if it return false the program didnt save the value.
     */

    public boolean saveDosagePerAgeGroup(){
        return this.dosagePerAgeGroupStore.saveDosagePerAgeGroup(DosesPerAgeGroup);
    }

    public boolean recordFileDosePerAgeGroup(){
        return this.dosagePerAgeGroupStore.recordFileDosagePerAge();
    }

    public List<Dosage> readFromFileDosePerAgeGroup(){
        return this.dosagePerAgeGroupStore.readFromFileDosagePerAge();
    }

    /**
     * Create a dosage number and verifies if it's valid
     * @param vaccineDosage the quantity of a certain dosage for each age group in mL.
     * @return dosage number
     */

    public boolean createDosageNumber(int vaccineDosage){
        this.vacDosage=this.dosageNumberStore.createDosageNumber(vaccineDosage);
        return this.dosageNumberStore.verifyDosageNumber(vacDosage);
    }

    /**
     * Saves the dosage number inserted and verifies if it's valid.
     * There is no parameter needed.
     * @return a boolean value, if it's true the program saved the value inserted in createDosageNumber, if it returns false the program didn't saved the value.
     */

    public boolean saveDosageNumber(){
        return this.dosageNumberStore.saveDosageNumber(vacDosage);
    }

    public boolean recordFileVacDosage(){
        return this.dosageNumberStore.recordFileVacDosage();
    }

    public List<Dosage> readFromFileVacDosage(){
        return this.dosageNumberStore.readFromFileVacDosage();
    }






}
