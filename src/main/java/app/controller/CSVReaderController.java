package app.controller;

import app.domain.model.CSVReader;
import java.io.IOException;

/**
 * @author Rui Milhazes <1211031@isep.ipp.pt>
 */

public class CSVReaderController {

    /**
     * Method to call the reading method with header
     * @param csvFile the file that contains the users
     * @throws IOException Exception generated in the method
     */

    public static void csvControllerWithHeader( String csvFile) throws IOException {
        CSVReader.checkHeader(csvFile);
            CSVReader.readwithheader(csvFile);
    }

    /**
     * Method to call the reading method without a header
     * @param csvFile the file that contains the users
     * @throws IOException Exception generated in the method
     */

    public static void csvControllerWithoutHeader( String csvFile) throws IOException {
        CSVReader.checkHeader(csvFile);
        CSVReader.readWithoutHeader(csvFile);
    }
}

