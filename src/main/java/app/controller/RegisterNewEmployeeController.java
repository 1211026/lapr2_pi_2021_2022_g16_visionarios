package app.controller;

import app.domain.model.Company;
import app.domain.model.Employee;
import app.domain.model.VaccineType;
import app.domain.model.stores.EmployeeStore;

import java.util.ArrayList;

/**
 * @author Diogo Teixeira <1210916@isep.ipp.pt>
 */

public class RegisterNewEmployeeController {
    private EmployeeStore employeeStore = new EmployeeStore();
    private Company company;
    private Employee ne;
    public RegisterNewEmployeeController  () {this(App.getInstance().getCompany());
    }
    public RegisterNewEmployeeController (Company company) {
        this.company = company;
        this.ne = null;
    }
    /**
     * Creates a New Employee and verifies if is valid
     * @param id ID of the employee
     * @param name Name of the employee
     * @param address Address of the employee
     * @param roleId Role of the employee (Receptionist, Nurse..)
     * @param email Email of the employee
     * @param citizenCardNumber Unique identification of the employee
     * @param phoneNumber Phone Number of the employee
     * @return
     */
    public boolean createNewEmployee(int id, String name, String address,String email, int phoneNumber,int citizenCardNumber, String roleId){
        this.ne=this.employeeStore.createNewEmployee( id, name, address, email, phoneNumber, citizenCardNumber, roleId);
        return this.employeeStore.validateNewEmployee(ne);
    }

    /**
     * Saves the New Employee
     * @return
     */

    public boolean saveNewEmployee(){
        return this.employeeStore.saveNewEmployee(ne);
    }

    /**
     * Method to call the recordFile method
     * @return
     */
    public boolean recordFile(){
        return this.employeeStore.recordFile();
    }

    /**
     * Method to call the readFromFile method
     * @return a array of vaccine type
     */

    public ArrayList<Employee> readFromFile(){
        return this.employeeStore.readFromFile();
    }

}
