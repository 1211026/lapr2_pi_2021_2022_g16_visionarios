package app.controller;
import app.domain.model.Company;
import app.domain.model.VaccinationCenter;
import app.domain.model.VaccineType;
import app.domain.model.stores.VaccinationCenterStore;
import java.sql.Time;
import java.util.ArrayList;

/**
 * @author Rodrigo Ferreira <1211030@isep.ipp.pt>
 */

public class RegisterVaccinationCenterController {
    private VaccinationCenterStore vaccinationCenterStore = new VaccinationCenterStore();
    private final Company company;
    private VaccinationCenter vc;
    public RegisterVaccinationCenterController () {
        this(App.getInstance().getCompany());
    }

    /**
     * Constructor
     * @param company Company that will be received
     */
    public RegisterVaccinationCenterController(Company company) {
        this.company = company;
        this.vc = null;
    }

    /**
     * Creates a Vaccination Center and verifies if is valid
     * @param name Name of the vaccination center
     * @param address Address of the vaccination center
     * @param phoneNumber Phone Number of the vaccination center
     * @param email Email of the vaccination center
     * @param faxNumber Fax Number of the vaccination center
     * @param websiteAddress Website address of the vaccination center
     * @param openingHours Opening hours of the vaccination center
     * @param closingHours Closing hours of the vaccination center
     * @param slotDuration Slot Duration of the vaccination center
     * @param maxSlotVaccines Mmax slots vaccines that the vaccination center can take
     * @return The validation of the vaccination center
     */
    public boolean createVaccinationCenter(String name, String address, int phoneNumber, String email, int faxNumber, String websiteAddress, Time openingHours, Time closingHours, int slotDuration, int maxSlotVaccines) {
        this.vc = vaccinationCenterStore.createVaccinationCenter(name, address, phoneNumber, email, faxNumber, websiteAddress, openingHours, closingHours, slotDuration, maxSlotVaccines);
        return this.vaccinationCenterStore.validateVaccinationCenter(vc);
    }

    /**
     * Saves the vaccination center
     * @return saves and adds the center to the list
     */
    public boolean saveVaccinationCenter() {
        return this.vaccinationCenterStore.saveVaccinationCenter(vc);
    }
    public ArrayList<VaccinationCenter> getVcList() {
        return this.vaccinationCenterStore.getVcList();
    }

    public boolean recordFile(){
        return this.vaccinationCenterStore.recordFile();
    }
    /**
     * This method read data from a file.
     * @return an array of vaccine type
     */
    public ArrayList<VaccinationCenter> readFromFile(){
        return this.vaccinationCenterStore.readFromFile();
    }

}