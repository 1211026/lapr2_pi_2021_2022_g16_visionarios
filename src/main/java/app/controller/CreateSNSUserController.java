package app.controller;

import app.domain.model.Company;
import app.domain.model.SNSUser;
import app.domain.model.VaccineType;
import app.domain.model.stores.SNSUserStore;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * @author Rodrigo Moreira <1211026@isep.ipp.pt>
 */

public class CreateSNSUserController {
    private final Company company;
    private SNSUser SNSuser;
    private SNSUserStore snsUserStore=new SNSUserStore();


    public CreateSNSUserController () {
        this(App.getInstance().getCompany());
    }

    public CreateSNSUserController(Company company) {
        this.company = company;
        this.SNSuser = null;
    }

    /**
     * Creates a new SNS user and verifies if is valid
     * @param name Name  of the SNS User
     * @param sex Gender of the SNS User
     * @param dateBirth Date of Birth of the SNS User
     * @param address Address of the SNS User
     * @param phoneNumber Phone Number of the SNS User
     * @param email Email of the SNS User
     * @param id ID of the SNS User
     * @param CitizenNumber Unique identification of the SNS User
     * @return Returns the object created or false (if there was an error in validation)
     */

    public boolean createSNSUser(String name, String sex, LocalDate dateBirth, String address, int phoneNumber, String email, int id, int CitizenNumber){
        this.SNSuser = this.snsUserStore.createSNSUser(name, sex, dateBirth, address, phoneNumber, email, id, CitizenNumber);
        return this.snsUserStore.validateSNSUser(SNSuser);
    }

    /**
     * Saves the New SNS user
     * @return the SNS User or false
     */

    public boolean saveSNSUser(){
        return this.snsUserStore.saveSNSUser(SNSuser);
    }

    public boolean recordFile(){
        return this.snsUserStore.recordFile();
    }
    /**
     * This method read data from a file.
     * @return an array of SNS users.
     */
    public ArrayList<SNSUser> readFromFile(){
        return this.snsUserStore.readFromFile();
    }
}

