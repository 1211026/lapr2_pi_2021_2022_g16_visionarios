package app.controller;

import app.domain.model.Company;
import app.domain.model.SNSUser;
import app.domain.model.stores.SNSUserStore;
import app.domain.model.stores.WaitingRoomStore;
import app.ui.console.utils.Utils;

import java.util.List;
import java.util.Queue;

import static app.controller.App.getInstance;

public class ConsultTheListOfSNSusersController extends Utils {

    /**
     * @author Rodrigo Moreira <1211026>
     */
    private Company company;
    private SNSUserStore snsUserStore = new SNSUserStore();

    public ConsultTheListOfSNSusersController(){
        this(getInstance().getCompany());
   }

    public ConsultTheListOfSNSusersController(Company company) {
        this.company = company;
    }

    public List<SNSUser> getSNSUserList()
    {
        return snsUserStore.getSNSUserList();
    }


}

