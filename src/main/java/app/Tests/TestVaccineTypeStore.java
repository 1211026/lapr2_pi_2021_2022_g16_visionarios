package app.Tests;
import app.controller.App;
import app.domain.model.VaccineType;
import app.domain.model.stores.VaccineTypeStore;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.ArrayList;

public class TestVaccineTypeStore {

    /**
     * @author Rui Milhazes <1211031@isep.ipp.pt>
     * Test for the testValidateVaccineType method with a null instance
     */

    @Test
    public void testValidateVaccineTypenull(){
        VaccineTypeStore vaccineTypeStore=new VaccineTypeStore();
        VaccineType vt1 = null;
        boolean expected = false;
        boolean result =vaccineTypeStore.validateVaccineType(vt1);
        Assertions.assertEquals(expected,result);
    }

    /**
     * Test dot the testValidateVaccineType with a valid vaccine type
     */

    @Test
    public void testvalidateVaccineType(){
        VaccineTypeStore vaccineTypeStore=new VaccineTypeStore();
        VaccineType vt1 = vaccineTypeStore.createVaccineType("1234", "Vaccine", 10);
        boolean expected = true;
        boolean result= vaccineTypeStore.validateVaccineType(vt1);
        Assertions.assertEquals(expected,result);

    }

    /**
     * Test for testSaveVaccineType method with a null instance
     */

    @Test
    public void testSaveVaccineTypeNull(){
        VaccineTypeStore vaccineTypeStore=new VaccineTypeStore();
        VaccineType vt1 = null;
        boolean expected = false;
        boolean result = vaccineTypeStore.saveVaccineType(vt1);
        Assertions.assertEquals(expected,result);

    }

    /**
     * Test for testSaveVaccineType method with a valid Vaccine type
     */

    @Test
    public void testSaveVaccineType(){
        VaccineTypeStore vaccineTypeStore=new VaccineTypeStore();
        VaccineType vt1 = vaccineTypeStore.createVaccineType("1234", "Vaccine", 10);
        ArrayList<VaccineType> vtlist= new ArrayList<>();
        vtlist.add(vt1);
        boolean expected = true;
        boolean result = vaccineTypeStore.saveVaccineType(vt1);
        Assertions.assertEquals(expected,result);
    }
}
