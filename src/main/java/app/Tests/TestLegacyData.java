package app.Tests;

import app.controller.LegacyDataController;
import app.domain.model.SNSUser;
import app.domain.model.stores.LegacyDataStore;
import app.domain.model.stores.SNSUserStore;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class TestLegacyData {
    @Test
    public void testOptionCheckA(){
    LegacyDataController ldc = new LegacyDataController();
    String expected = "A";
    String result = LegacyDataController.optionCheck();
    Assertions.assertEquals(expected,result);
    }
    @Test
    public void testOptionCheckB(){
        LegacyDataController ldc = new LegacyDataController();
        String expected = "B";
        String result = LegacyDataController.optionCheck();
        Assertions.assertEquals(expected,result);
    }


} //@Test
//public void testValidateLine(){
//    LegacyDataStore lds = new LegacyDataStore("Vitor Hugo Cavalcanti", "Spikevax", "Primeira", "21C16-05",  "5/30/2022 08:00 ", "5/30/2022 8:24", "5/30/2022 9:11" ,"5/30/2022");
//    LegacyDataStore data = lds.validateLine("User");
//    boolean exp = true;
//    boolean res= lds.validateLine(data);
//    Assertions.assertEquals(exp,res);
//}
