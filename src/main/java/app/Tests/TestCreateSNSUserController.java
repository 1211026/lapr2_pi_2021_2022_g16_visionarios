package app.Tests;

import app.controller.App;
import app.controller.CreateSNSUserController;
import app.domain.model.Company;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import java.time.LocalDate;

/**
 * @author Rodrigo Moreira <1211026@isep.ipp.pt>
 * Tests for the CreateSNSUserController class
 */

public class TestCreateSNSUserController {
    Company c1=new Company("Companhia 1");
    CreateSNSUserController user1 =new CreateSNSUserController(c1);

    /**
     * Test for the createSNSUser method
     */

    @Test
    public void testCreateSNSUser(){
        boolean expected= true;
        boolean result= user1.createSNSUser("Name1", "Male", LocalDate.ofEpochDay(12/2/2000), "Address",919333222 ,"email@gmail.com" , 9122131, 938123123);
        Assertions.assertEquals(expected,result);
    }

    /**
     * Test for the saveSNSUser method with a valid SNS user
     */
    @Test
    public void testSaveSNSUserValid(){
        user1.createSNSUser("Name1", "Male", LocalDate.ofEpochDay(12/2/2000), "Address",919333222 ,"email@gmail.com" , 9122131, 919112931);
        boolean expected=true;
        boolean result= user1.saveSNSUser();
        Assertions.assertEquals(expected,result);
    }
    /**
     * Test for the saveSNSUser method with an invalid SNS user
     */
    @Test
    public void testSaveSNSUserInvalid(){
        boolean expected=false;
        boolean result= user1.saveSNSUser();
        Assertions.assertEquals(expected,result);
    }
}
