package app.Tests;
import app.domain.model.Dosage;
import app.domain.model.stores.DosagePerAgeGroupStore;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.ArrayList;

public class TestDosagePerGroupAgeStore {
    /**
     * Test for testVerifyDosagePerAgeGroup method with a null instance
     */



    @Test
    public void testVerifyDosagePerAgeGroupnull(){
        DosagePerAgeGroupStore dosagePerAgeGroupStore=new DosagePerAgeGroupStore();
        Dosage dpag1 = null;
        boolean exp = false;
        boolean res =dosagePerAgeGroupStore.verifyDosagePerAgeGroup(dpag1);
        Assertions.assertEquals(exp,res);
    }

    /**
     * Test for testVerifyDosagePerAgeGroup method with a valid Dosage per age group
     */

    @Test
    public void testVerifyDosagePerAgeGroup(){
        ArrayList<Dosage> dosagelist= new ArrayList<>();
        DosagePerAgeGroupStore dosagePerAgeGroupStore=new DosagePerAgeGroupStore();
        Dosage dpag1=dosagePerAgeGroupStore.createDosagePerAgeGroup(2);
        dosagelist.add(dpag1);
        boolean exp = true;
        boolean res = dosagePerAgeGroupStore.verifyDosagePerAgeGroup(dpag1);
        Assertions.assertEquals(exp,res);
    }

    /**
     * Test for testSaveDosagePerAgeGroup method with a valid dose
     */

    @Test
    public void testSaveDosagePerAgeGroup(){
        ArrayList<Dosage> dosagelist= new ArrayList<>();
        DosagePerAgeGroupStore dosagePerAgeGroupStore=new DosagePerAgeGroupStore();
        Dosage dpag1=dosagePerAgeGroupStore.createDosagePerAgeGroup(2);
        dosagelist.add(dpag1);
        boolean exp = true;
        boolean res = dosagePerAgeGroupStore.saveDosagePerAgeGroup(dpag1);
        Assertions.assertEquals(exp,res);
    }

    /**
     * Test for testSaveDosagePerAgeGroup method with a null instance
     */


    @Test
    public void testSaveDosagePerAgeGroupnull(){
        DosagePerAgeGroupStore dosagePerAgeGroupStore=new DosagePerAgeGroupStore();
        Dosage dpag1 = null;
        boolean exp = false;
        boolean res =dosagePerAgeGroupStore.saveDosagePerAgeGroup(dpag1);
        Assertions.assertEquals(exp,res);
    }
}
