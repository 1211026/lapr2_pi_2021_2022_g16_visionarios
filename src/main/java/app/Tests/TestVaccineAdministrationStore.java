package app.Tests;
import app.domain.model.DosageNumberDosageGroupAge;
import app.domain.model.SNSUser;
import app.domain.model.VaccinationCenter;
import app.domain.model.VaccineAdministration;
import app.domain.model.stores.DosaNumberDosageGroupAgeStore;
import app.domain.model.stores.SNSUserStore;
import app.domain.model.stores.VaccinationCenterStore;
import app.domain.model.stores.VaccineAdministrationStore;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class TestVaccineAdministrationStore {

    /**
     * Test for the ValidateVaccineAdministration method with a null instance.
     */

    @Test
    public void testValidateVaccineAdministrationNull(){
        VaccineAdministrationStore vaccineAdministrationStore=new VaccineAdministrationStore();
        VaccineAdministration va=null;
        boolean expected=false;
        boolean result= vaccineAdministrationStore.validateVaccineAdministration(va);
        Assertions.assertEquals(expected,result);
    }

    /**
     * Test for the ValidateVaccineAdministration method with a instance of Vaccine Administration.
     */
    @Test
    public void testValidateVaccineAdministration(){
        VaccineAdministrationStore vaccineAdministrationStore=new VaccineAdministrationStore();
        VaccinationCenter vaccinationCenter= new VaccinationCenter("Center1","Street1",1111,"email",11,"websiteadress", Time.valueOf("08:00:00"),Time.valueOf("17:00:00"),5,10);
        VaccineAdministration va=vaccineAdministrationStore.createVaccineAdministration(1234,"Covid","Pfizer",1,"22-AA", LocalDateTime.of(2022,06,19,8,10),vaccinationCenter);
        boolean expected=true;
        boolean result= vaccineAdministrationStore.validateVaccineAdministration(va);
        Assertions.assertEquals(expected,result);
    }

    /**
     * Test for the saveVaccineAdministration method with a null instance.
     */

    @Test
    public void testSaveVaccineAdministrationNull(){
        VaccineAdministrationStore vaccineAdministrationStore=new VaccineAdministrationStore();
        VaccineAdministration va=null;
        boolean expected=false;
        boolean result= vaccineAdministrationStore.saveVaccineAdminstration(va);
        Assertions.assertEquals(expected,result);
    }

    /**
     * Test for the saveVaccineAdministration method with a valid instance.
     */
    @Test
    public void testSaveVaccineAdministration(){
        VaccineAdministrationStore vaccineAdministrationStore=new VaccineAdministrationStore();
        VaccinationCenter vaccinationCenter= new VaccinationCenter("Center1","Street1",1111,"email",11,"websiteadress", Time.valueOf("08:00:00"),Time.valueOf("17:00:00"),5,10);
        VaccineAdministration va=vaccineAdministrationStore.createVaccineAdministration(1234,"Covid","Pfizer",1,"22-AA", LocalDateTime.of(2022,06,19,8,10),vaccinationCenter);
        boolean expected=true;
        boolean result= vaccineAdministrationStore.saveVaccineAdminstration(va);
        Assertions.assertEquals(expected,result);
    }

    /**
     * Test for the ageOfPerson to check if the operation is correct.
     */
    @Test
    public void testAgeOfPerson(){
        List<SNSUser> snsUsersList=new ArrayList<>();
        SNSUserStore snsUserStore=new SNSUserStore();
        SNSUser user1=snsUserStore.createSNSUser("Name1", "male", LocalDate.ofEpochDay(12/2/2000), "Adress",911222333 ,"Test@gmail.com" ,1234, 91231231);
        snsUsersList.add(user1);
        VaccineAdministrationStore vaccineAdministrationStore=new VaccineAdministrationStore();
        double expected=22;
        double result= vaccineAdministrationStore.ageOfPerson(1234);
        Assertions.assertEquals(expected,result);
    }

    @Test
    public void testcheckVaccineToGive(){
        VaccineAdministrationStore vaccineAdministrationStore=new VaccineAdministrationStore();
        ArrayList<DosageNumberDosageGroupAge> vaccineList=new ArrayList<>();
        ArrayList<DosageNumberDosageGroupAge> bestVaccine=new ArrayList<>();
        DosageNumberDosageGroupAge vaccine1=new DosageNumberDosageGroupAge("Covid","Moderna","afav",30,"19-40",2);
        DosageNumberDosageGroupAge vaccine2=new DosageNumberDosageGroupAge("Covid","Moderna","afav",30,"12-18",2);
        vaccineList.add(vaccine1);
        vaccineList.add(vaccine2);
        bestVaccine.add(vaccine1);
        ArrayList<DosageNumberDosageGroupAge> expected=bestVaccine;
        ArrayList<DosageNumberDosageGroupAge> result=vaccineAdministrationStore.checkVaccineToGive("Covid",23);
        Assertions.assertEquals(expected,result);

    }



}
