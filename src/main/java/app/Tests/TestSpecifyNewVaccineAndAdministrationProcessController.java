package app.Tests;


import app.controller.SpecifyNewVaccineAndAdministrationProcessController;
import app.domain.model.Company;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

public class TestSpecifyNewVaccineAndAdministrationProcessController {
    Company c1=new Company("Companhia 1");
    SpecifyNewVaccineAndAdministrationProcessController s1 =new SpecifyNewVaccineAndAdministrationProcessController(c1);

    /**
     * Test for the createNewGroupAge method
     */

    @Test
    public void testCreateNewGroupAge(){
        boolean expected= true;
        boolean result= s1.createNewGroupAge("Adult");
        Assertions.assertEquals(expected,result);
    }

    /**
     * Test for the saveGroupAgeOfThePerson method with a valid age group
     */
    @Test
    public void testSaveGroupAgeOfThePersonValid(){
        s1.createNewGroupAge("Adult");
        boolean expected=true;
        boolean result= s1.saveGroupAgeOfThePerson();
        Assertions.assertEquals(expected,result);
    }

    /**
     * Test for the saveGroupAgeOfThePerson method with an invalid age group
     */
    @Test
    public void testSaveGroupAgeOfThePersonInvalid(){
        boolean expected=false;
        boolean result= s1.saveGroupAgeOfThePerson();
        Assertions.assertEquals(expected,result);
    }

    /**
     * Test for the createNewVaccine method
     */

    @Test
    public void testCreateNewVaccine(){
        boolean expected= true;
        boolean result= s1.createNewVaccine("Vaccine1","XXXX","MHND");
        Assertions.assertEquals(expected,result);
    }

    /**
     * Test for the saveNewVaccine method with a valid age group
     */
    @Test
    public void testSaveNewVaccine(){
        s1.createNewVaccine("Vaccine1","XXXX","MHND");
        boolean expected=true;
        boolean result= s1.saveNewVaccine();
        Assertions.assertEquals(expected,result);
    }

    /**
     * Test for the saveNewVaccine method with an invalid age group
     */
    @Test
    public void testSaveNewVaccineInvalid(){
        boolean expected=false;
        boolean result= s1.saveNewVaccine();
        Assertions.assertEquals(expected,result);
    }

    /**
     * Test for the createDosagePerAgeGroup method
     */

    @Test
    public void testCreateDosagePerAgeGroup(){
        boolean expected= true;
        boolean result= s1.createDosagePerAgeGroup(1);
        Assertions.assertEquals(expected,result);
    }

    /**
     * Test for the saveDosagePerAgeGroup method with a valid age group
     */
    @Test
    public void testSaveDosagePerAgeGroup(){
        s1.createDosagePerAgeGroup(1);
        boolean expected=true;
        boolean result= s1.saveDosagePerAgeGroup();
        Assertions.assertEquals(expected,result);
    }

    /**
     * Test for the saveDosagePerAgeGroup method with an invalid age group
     */
    @Test
    public void testSaveDosagePerAgeGroupInvalid(){
        boolean expected=false;
        boolean result= s1.saveDosagePerAgeGroup();
        Assertions.assertEquals(expected,result);
    }

    /**
     * Test for the createDosageNumber method
     */

    @Test
    public void testCreateDosageNumber(){
        boolean expected= true;
        boolean result= s1.createDosageNumber(15);
        Assertions.assertEquals(expected,result);
    }

    /**
     * Test for the saveDosageNumber method with a valid age group
     */
    @Test
    public void testSaveDosageNumber(){
        s1.createDosageNumber(15);
        boolean expected=true;
        boolean result= s1.saveDosageNumber();
        Assertions.assertEquals(expected,result);
    }

    /**
     * Test for the saveDosageNumber method with an invalid age group
     */
    @Test
    public void testSavDosageNumber(){
        boolean expected=false;
        boolean result= s1.saveDosageNumber();
        Assertions.assertEquals(expected,result);
    }





}
