package app.Tests;

import app.domain.model.ValidateDates;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

public class TestValidateDates
/**
 * @author Diogo Teixeira <1210916@isep.ipp.pt>
 * Test for the testValidateDates method with a null instance
 */{
     @Test
    public void testValidateDatesTypeNull(){
         ValidateDates validateDates=new ValidateDates();
         String firstDay=null,lastDay=null;
         int exp=2;
         int res=validateDates.ValidateInterval(firstDay, lastDay);
         Assertions.assertEquals(exp,res);
     }
    /**
     * Test do the testValidateDates with a valid interval
     */
    @Test
    public void testValidateDatesWithValidInterval(){

        ValidateDates validateDates=new ValidateDates();
        String firstDay="13/10/2022",lastDay="11/12/2022";
        int exp=0;
        int res=validateDates.ValidateInterval(firstDay,lastDay);
        Assertions.assertEquals(exp,res);
    }
    /**
     * Test do the testValidateDates with a first day after the last day
     */
    @Test
    public void testValidateDatesWithInvalidInterval(){
        ValidateDates validateDates=new ValidateDates();
        String firstDay="13/12/2022",lastDay="11/10/2022";
        int exp=1;
        int res=validateDates.ValidateInterval(firstDay,lastDay);
        Assertions.assertEquals(exp,res);
    }
    /**
     * Test do the testValidateDates with an invalid date
     */
    @Test
    public void testValidateDatesWithInvalidDate(){
        ValidateDates validateDates=new ValidateDates();
        String firstDay="abc",lastDay="abc";
        int exp=2;
        int res=validateDates.ValidateInterval(firstDay,lastDay);
        Assertions.assertEquals(exp,res);
    }
}
