package app.Tests;
import app.controller.SpecifyNewVaccineTypeController;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import app.domain.model.Company;

/**
 * @author Rui Milhazes </1211031@isep.ipp.pt>
 * Tests for the TestSpecifyNewVaccineTypeController class
 */
public class TestSpecifyNewVaccineTypeController {
    Company c1 = new Company("Company 1");
    SpecifyNewVaccineTypeController vt1 =  new SpecifyNewVaccineTypeController(c1);

    /**
     * Test for the createVaccineType method
     */
    @Test
    public void testCreateVaccineType(){
        boolean expected=true;
        boolean result= vt1.createVaccineType("1234", "Vaccine", 10);
        Assertions.assertEquals(expected,result);
    }

    /**
     * Test for the saveVaccineType method with a valid vaccine type
     */

    @Test
    public void testSaveVaccineTypeValid(){
        vt1.createVaccineType("1234", "Vaccine",  10);
        boolean expected = true;
        boolean result = vt1.saveVaccineType();
        Assertions.assertEquals(expected, result);
    }

    /**
     * Test for the saveVaccineType method with an invalid vaccine type
     */

    @Test
    public void testSaveVaccineTypeInvalid(){
        boolean expected = false;
        boolean result = vt1.saveVaccineType();
        Assertions.assertEquals(expected, result);

    }
}
