package app.Tests;
import app.domain.model.SNSUser;
import app.domain.model.stores.SNSUserStore;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.time.LocalDate;
import java.util.ArrayList;

public class TestSNSUserStore {
    /**
     * @author Rodrigo Moreira <1211026@isep.ipp.pt>
     * Test for the testValidateSNSUser method with a null instance
     */

    @Test
    public void testValidateSNSUsernull(){
        SNSUserStore snsUserStore=new SNSUserStore();
        SNSUser user1 = null;
        boolean exp = false;
        boolean res =snsUserStore.validateSNSUser(user1);
        Assertions.assertEquals(exp,res);
    }

    /**
     * Test for the testValidateSNSUser with a valid SNS user
     */

    @Test
    public void testValidateSNSUser(){
        SNSUserStore snsUserStore=new SNSUserStore();
        SNSUser user1= snsUserStore.createSNSUser("Name1", "male", LocalDate.ofEpochDay(12/2/2000), "Adress",911222333 ,"Test@gmail.com" ,1234, 91231231);
        boolean exp = true;
        boolean res= snsUserStore.validateSNSUser(user1);
        Assertions.assertEquals(exp,res);

    }

    /**
     * Test for testSNSUser method with a null instance
     */

    @Test
    public void testSaveSNSUserNull(){
        SNSUserStore snsUserStore=new SNSUserStore();
        SNSUser user1 = null;
        boolean exp = false;
        boolean res = snsUserStore.saveSNSUser(user1);
        Assertions.assertEquals(exp,res);

    }

    /**
     * Test for testSaveSNSUser method with a valid SNS user
     */

    @Test
    public void testSaveSNSUser(){
        ArrayList<SNSUser> userlist= new ArrayList<>();
        SNSUserStore snsUserStore=new SNSUserStore();
        SNSUser user1= snsUserStore.createSNSUser("Name1", "male", LocalDate.ofEpochDay(12/2/2000), "Adress",911222333 ,"Test@gmail.com" ,1234, 91231231);
        userlist.add(user1);
        boolean exp = true;
        boolean res = snsUserStore.saveSNSUser(user1);
        Assertions.assertEquals(exp,res);
    }

}
