package app.Tests;

import app.controller.VaccineAdministrationController;
import app.controller.VaccineScheduleController;
import app.domain.model.Company;
import app.domain.model.VaccinationCenter;
import app.domain.model.VaccineAdministration;
import app.domain.model.VaccineType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class TestVaccineAdministrationController {

    /**
     * Test for the createVaccineAdministration.
     */
    @Test
    public void testcreateVaccineAdministration(){
        Company c1=new Company("Companhia 1");
        VaccinationCenter vaccinationCenter= new VaccinationCenter("Center1","Street1",1111,"email",11,"websiteadress", Time.valueOf("08:00:00"),Time.valueOf("17:00:00"),5,10);
        VaccineAdministrationController vaccineAdministrationController=new VaccineAdministrationController(c1);
        boolean expected=true;
        boolean result= vaccineAdministrationController.createVaccineAdministration(1234,"Covid","Pfizer",1,"22-AA", LocalDateTime.of(2022,06,19,8,10),vaccinationCenter);
        Assertions.assertEquals(expected,result);
    }

    /**
     * Test for the saveVaccineAdministration method with a valid vaccine administration.
     */
    @Test
    public void testSaveVaccineAdministrationValid(){
        Company c1=new Company("Companhia 1");
        VaccineAdministrationController vaccineAdministrationController=new VaccineAdministrationController(c1);
        VaccinationCenter vaccinationCenter= new VaccinationCenter("Center1","Street1",1111,"email",11,"websiteadress", Time.valueOf("08:00:00"),Time.valueOf("17:00:00"),5,10);
        vaccineAdministrationController.createVaccineAdministration(1234,"Covid","Pfizer",1,"22-AA", LocalDateTime.of(2022,06,19,8,10),vaccinationCenter);
        boolean expected=true;
        boolean result= vaccineAdministrationController.saveVaccineAdministration();
        Assertions.assertEquals(expected,result);
    }

    /**
     * Test for the saveVaccineAdministration method with a valid vaccine administration.
     */
    @Test
    public void testSaveVaccineAdministrationInvalid(){
        Company c1=new Company("Companhia 1");
        VaccineAdministrationController vaccineAdministrationController=new VaccineAdministrationController(c1);
        boolean expected=false;
        boolean result= vaccineAdministrationController.saveVaccineAdministration();
        Assertions.assertEquals(expected,result);
    }
}
