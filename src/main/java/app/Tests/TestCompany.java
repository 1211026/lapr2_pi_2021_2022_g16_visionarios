
package app.Tests;
import app.domain.model.*;
import app.domain.model.stores.*;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.sql.Time;
import java.time.LocalDate;
import java.util.ArrayList;

public class TestCompany {
    /**
     * @author Rodrigo Ferreira <1211030@isep.ipp.pt>
     * Tests for the Company class
     */
    DosagePerAgeGroupStore dosagePerAgeGroupStore;
    EmployeeStore employeeStore;
    VaccinationCenterStore vaccinationCenterStore;
    VaccineTypeStore vaccineTypeStore;
    SNSUserStore snsUserStore;
    VaccineStore vaccineStore;
    GroupAgeStore groupAgeStore;
    DosageNumberStore dosageNumberStore;
    VaccinationCenter vc1= vaccinationCenterStore.createVaccinationCenter("Center1","Street1",1111,"email",11,"websiteadress", Time.valueOf("08:00:00"),Time.valueOf("17:00:00"),5,10);
    VaccineType vt1 = vaccineTypeStore.createVaccineType("1234", "Vaccine",  10);
    Employee ne1= employeeStore.createNewEmployee(12345, "Name1", "Street1", "email@gmail.com",911111111 ,1234567 , "Nurse");
    SNSUser user1= snsUserStore.createSNSUser("Name1", "male", LocalDate.ofEpochDay(12/2/2000), "Address",911222333 ,"Test@gmail.com" ,1234, 91231231);
    Vaccine vac1=vaccineStore.createNewVaccine("Vaccine1","XXXX","MHDP");
    GroupAgeOfThePerson g1=groupAgeStore.createNewGroupAge("Adult");
    Dosage dn1=dosageNumberStore.createDosageNumber(15);
    Dosage dpag1=dosagePerAgeGroupStore.createDosagePerAgeGroup(2);
    /**
     * Test for the validateVaccinationCenter method with a null instance
     */
    @Test
    public void testValidateVaccinationCenterNull(){
        VaccinationCenter vc1=null;
        boolean expected=false;
        boolean result= vaccinationCenterStore.validateVaccinationCenter(vc1);
        Assertions.assertEquals(expected,result);
    }
    /**
     * Test for the validateVaccinationCenter method with a instance of VaccinationCenter
     */
    @Test
    public void testValidateVaccinationCenter(){
        boolean expected=true;
        boolean result= vaccinationCenterStore.validateVaccinationCenter(vc1);
        Assertions.assertEquals(expected,result);
    }
    /**
     * Test for saveVaccinationCenter method with a valid center
     */
    @Test
    public void testSaveVaccinationCenter(){
        ArrayList<VaccinationCenter> vclist= new ArrayList<>();
        vclist.add(vc1);
        boolean expected=true;
        boolean result= vaccinationCenterStore.saveVaccinationCenter(vc1);
        Assertions.assertEquals(expected,result);
    }
    /**
     * Test for the saveVaccinationCenter method with a null instance
     */
    @Test
    public void testSaveVaccinationCenterNull(){
        VaccinationCenter vc1= null;
        boolean expected=false;
        boolean result= vaccinationCenterStore.saveVaccinationCenter(vc1);
        Assertions.assertEquals(expected,result);
    }

    /**
     * @author Rui Milhazes <1211031@isep.ipp.pt>
     * Test for the testValidateVaccineType method with a null instance
     */

    @Test
    public void testValidateVaccineTypenull(){
        VaccineType vt1 = null;
        boolean expected = false;
        boolean result =vaccineTypeStore.validateVaccineType(vt1);
        Assertions.assertEquals(expected,result);
    }

    /**
     * Test dot the testValidateVaccineType with a valid vaccine type
     */

    @Test
    public void testvalidateVaccineType(){
    boolean expected = true;
    boolean result= vaccineTypeStore.validateVaccineType(vt1);
    Assertions.assertEquals(expected,result);

    }

    /**
     * Test for testSaveVaccineType method with a null instance
     */

    @Test
    public void testSaveVaccineTypeNull(){
        VaccineType vt1 = null;
        boolean expected = false;
        boolean result = vaccineTypeStore.saveVaccineType(vt1);
        Assertions.assertEquals(expected,result);

    }

    /**
     * Test for testSaveVaccineType method with a valid Vaccine type
     */

    @Test
    public void testSaveVaccineType(){
        ArrayList<VaccineType> vtlist= new ArrayList<>();
        vtlist.add(vt1);
        boolean expected = true;
        boolean result = vaccineTypeStore.saveVaccineType(vt1);
        Assertions.assertEquals(expected,result);
    }

    /**
     * @author Diogo Teixeira <1210916@isep.ipp.pt>
     * Test for the testValidateNewEmployee method with a null instance
     */

    @Test
    public void testValidateNewEmployeenull(){
        Employee ne1 = null;
        boolean exp = false;
        boolean res =employeeStore.validateNewEmployee(ne1);
        Assertions.assertEquals(exp,res);
    }

    /**
     * Test dot the testValidateEmployee with a valid new employee
     */

    @Test
    public void testvalidateNewEmployee(){
        boolean exp = true;
        boolean res= employeeStore.validateNewEmployee(ne1);
        Assertions.assertEquals(exp,res);

    }

    /**
     * Test for testSaveEmployee method with a null instance
     */

    @Test
    public void testSaveEmloyeeNull(){
        Employee ne1 = null;
        boolean exp = false;
        boolean res = employeeStore.saveNewEmployee(ne1);
        Assertions.assertEquals(exp,res);

    }

    /**
     * Test for testSaveEmployee method with a valid New Employee
     */

    @Test
    public void testSaveNewEmployee(){
        ArrayList<Employee> nelist= new ArrayList<>();
        nelist.add(ne1);
        boolean exp = true;
        boolean res = employeeStore.saveNewEmployee(ne1);
        Assertions.assertEquals(exp,res);
    }

    /**
     * @author Rodrigo Moreira <1211026@isep.ipp.pt>
     * Test for the testValidateSNSUser method with a null instance
     */

    @Test
    public void testValidateSNSUsernull(){
        SNSUser user1 = null;
        boolean exp = false;
        boolean res =snsUserStore.validateSNSUser(user1);
        Assertions.assertEquals(exp,res);
    }

    /**
     * Test for the testValidateSNSUser with a valid SNS user
     */

    @Test
    public void testValidateSNSUser(){
        boolean exp = true;
        boolean res= snsUserStore.validateSNSUser(user1);
        Assertions.assertEquals(exp,res);

    }

    /**
     * Test for testSNSUser method with a null instance
     */

    @Test
    public void testSaveSNSUserNull(){
        SNSUser user1 = null;
        boolean exp = false;
        boolean res = snsUserStore.saveSNSUser(user1);
        Assertions.assertEquals(exp,res);

    }

    /**
     * Test for testSaveSNSUser method with a valid SNS user
     */

    @Test
    public void testSaveSNSUser(){
        ArrayList<SNSUser> userlist= new ArrayList<>();
        userlist.add(user1);
        boolean exp = true;
        boolean res = snsUserStore.saveSNSUser(user1);
        Assertions.assertEquals(exp,res);
    }

    /**
     * Test for testVerifyNewVaccine method with a null instance
     */

    @Test
    public void testVerifyNewVaccinenull(){
        Vaccine vac1 = null;
        boolean exp = false;
        boolean res =vaccineStore.verifyNewVaccine(vac1);
        Assertions.assertEquals(exp,res);
    }

    /**
     * Test for testVerifyNewVaccine method with a valid vaccine
     */

    @Test
    public void testVerifyNewVaccine(){
        ArrayList<Vaccine> vaclist= new ArrayList<>();
        vaclist.add(vac1);
        boolean exp = true;
        boolean res = vaccineStore.verifyNewVaccine(vac1);
        Assertions.assertEquals(exp,res);
    }

    /**
     * Test for testSaveNewVaccine method with a valid vaccine
     */

    @Test
    public void testSaveNewVaccine(){
        ArrayList<Vaccine> vaclist= new ArrayList<>();
        vaclist.add(vac1);
        boolean exp = true;
        boolean res = vaccineStore.saveNewVaccine(vac1);
        Assertions.assertEquals(exp,res);
    }

    /**
     * Test for testSaveNewVaccine method with a null instance
     */


    @Test
    public void testSaveNewVaccinenull(){
        Vaccine vac1 = null;
        boolean exp = false;
        boolean res =vaccineStore.saveNewVaccine(vac1);
        Assertions.assertEquals(exp,res);
    }


    /**
     * Test for testVerifyGroupAgeOfThePerson method with a null instance
     */



    @Test
    public void testVerifyGroupAgeOfThePersonnull(){
        GroupAgeOfThePerson g1 = null;
        boolean exp = false;
        boolean res =groupAgeStore.verifyGroupAgeOfThePerson(g1);
        Assertions.assertEquals(exp,res);
    }

    /**
     * Test for testVerifyGroupAgeOfThePerson method with a valid age group
     */

    @Test
    public void testVerifyGroupAgeOfThePerson(){
        ArrayList<GroupAgeOfThePerson> groupAgelist= new ArrayList<>();
        groupAgelist.add(g1);
        boolean exp = true;
        boolean res = groupAgeStore.verifyGroupAgeOfThePerson(g1);
        Assertions.assertEquals(exp,res);
    }

    /**
     * Test for testSaveGroupAgeOfThePerson method with a valid age group
     */

    @Test
    public void testSaveGroupAgeOfThePerson(){
        ArrayList<GroupAgeOfThePerson> groupAgelist= new ArrayList<>();
        groupAgelist.add(g1);
        boolean exp = true;
        boolean res = groupAgeStore.saveGroupAgeOfThePerson(g1);
        Assertions.assertEquals(exp,res);
    }

    /**
     * Test for testSaveGroupAgeOfThePerson method with a null instance
     */


    @Test
    public void testSaveGroupAgeOfThePersonnull(){
        GroupAgeOfThePerson g1 = null;
        boolean exp = false;
        boolean res =groupAgeStore.saveGroupAgeOfThePerson(g1);
        Assertions.assertEquals(exp,res);
    }


    /**
     * Test for testVerifyDosagePerAgeGroup method with a null instance
     */



    @Test
    public void testVerifyDosagePerAgeGroupnull(){
        Dosage dpag1 = null;
        boolean exp = false;
        boolean res =dosagePerAgeGroupStore.verifyDosagePerAgeGroup(dpag1);
        Assertions.assertEquals(exp,res);
    }

    /**
     * Test for testVerifyDosagePerAgeGroup method with a valid Dosage per age group
     */

    @Test
    public void testVerifyDosagePerAgeGroup(){
        ArrayList<Dosage> dosagelist= new ArrayList<>();
        dosagelist.add(dpag1);
        boolean exp = true;
        boolean res = dosagePerAgeGroupStore.verifyDosagePerAgeGroup(dpag1);
        Assertions.assertEquals(exp,res);
    }

    /**
     * Test for testSaveDosagePerAgeGroup method with a valid dose
     */

    @Test
    public void testSaveDosagePerAgeGroup(){
        ArrayList<Dosage> dosagelist= new ArrayList<>();
        dosagelist.add(dpag1);
        boolean exp = true;
        boolean res = dosagePerAgeGroupStore.saveDosagePerAgeGroup(dpag1);
        Assertions.assertEquals(exp,res);
    }

    /**
     * Test for testSaveDosagePerAgeGroup method with a null instance
     */


    @Test
    public void testSaveDosagePerAgeGroupnull(){
        Dosage dpag1 = null;
        boolean exp = false;
        boolean res =dosagePerAgeGroupStore.saveDosagePerAgeGroup(dpag1);
        Assertions.assertEquals(exp,res);
    }


    /**
     * Test for testVerifyDosageNumber method with a null instance
     */



    @Test
    public void testVerifyDosageNumbernull(){
        Dosage dn1 = null;
        boolean exp = false;
        boolean res =dosageNumberStore.verifyDosageNumber(dn1);
        Assertions.assertEquals(exp,res);
    }

    /**
     * Test for testVerifyDosageNumber method with a valid dosage number
     */

    @Test
    public void testVerifyDosageNumber(){
        ArrayList<Dosage> dosageNumberlist= new ArrayList<>();
        dosageNumberlist.add(dn1);
        boolean exp = true;
        boolean res = dosageNumberStore.verifyDosageNumber(dn1);
        Assertions.assertEquals(exp,res);
    }

    /**
     * Test for testSaveDosageNumber method with a valid dosage number
     */

    @Test
    public void testSaveDosageNumber(){
        ArrayList<Dosage> dosageNumberlist= new ArrayList<>();
        dosageNumberlist.add(dn1);
        boolean exp = true;
        boolean res = dosageNumberStore.saveDosageNumber(dn1);
        Assertions.assertEquals(exp,res);
    }

    /**
     * Test for testSaveDosageNumber method with a null instance
     */


    @Test
    public void testSaveDosageNumbernull(){
        Dosage dn1 = null;
        boolean exp = false;
        boolean res =dosageNumberStore.saveDosageNumber(dn1);
        Assertions.assertEquals(exp,res);
    }

    /**
     * @author Rodrigo Ferreira <1211030@isep.ipp.pt>
     * Tests for the Company class
     */
   // @Test
   // public void testValidateVaccineSchedule(){
   //     VaccinationCenterStore vaccinationCenterStore1=new VaccinationCenterStore();
   //     VaccinationScheduleStore vss1=new VaccinationScheduleStore();
   //     VaccinationCenter vc2= vaccinationCenterStore1.createVaccinationCenter("Center1","Adress1",3,"Email1",1,"WebsiteAdress",Time.valueOf("08:00:00"),Time.valueOf("18:00:00"),5,2);
   //     Vaccine vaccine=vaccineStore.createNewVaccine("Name1","Brand1","Code1");
   //     VaccineSchedule vs1= vss1.createVaccineSchedule(11,vc2,"18-05-2022","09:00",vaccine);
   //     boolean expected=true;
   //     boolean result= vss1.validateVaccineSchedule(vs1);
   //     Assertions.assertEquals(expected,result);
   // }
   // /**
   //  * Test for saveVaccinechedule method with a valid schedule.
   //  */
   // @Test
   // public void testSaveVaccineSchedule(){
   //     VaccinationCenterStore vaccinationCenterStore1=new VaccinationCenterStore();
   //     VaccinationScheduleStore vss1=new VaccinationScheduleStore();
   //     VaccinationCenter vc2= vaccinationCenterStore1.createVaccinationCenter("Center1","Adress1",3,"Email1",1,"WebsiteAdress",Time.valueOf("08:00:00"),Time.valueOf("18:00:00"),5,2);
   //     Vaccine vaccine=vaccineStore.createNewVaccine("Name1","Brand1","Code1");
   //     VaccineSchedule vs1= vss1.createVaccineSchedule(11,vc2,"18-05-2022","09:00",vaccine);
   //     boolean expected=true;
   //     boolean result= vss1.saveVaccineSchedule(vs1);
   //     Assertions.assertEquals(expected,result);
   // }
   // /**
   //  * Test for the saveVaccineSchedule method with a null instance.
   //  */
   // @Test
   // public void testSaveVaccineScheduleNull(){
   //     VaccinationCenterStore vaccinationCenterStore1=new VaccinationCenterStore();
   //     VaccinationScheduleStore vss1=new VaccinationScheduleStore();
   //     VaccinationCenter vc2= vaccinationCenterStore1.createVaccinationCenter("Center1","Adress1",3,"Email1",1,"WebsiteAdress",Time.valueOf("08:00:00"),Time.valueOf("18:00:00"),5,2);
   //     VaccineSchedule vs1= null;
   //     boolean expected=false;
   //     boolean result= vss1.saveVaccineSchedule(vs1);
   //     Assertions.assertEquals(expected,result);
   // }
   // /**
   //  * Test for the checkSameVaccine method with one schedule.
   //  */
   // @Test
   // public void testCheckNotSameVaccine(){
   //     VaccinationCenterStore vaccinationCenterStore1=new VaccinationCenterStore();
   //     VaccinationScheduleStore vss1=new VaccinationScheduleStore();
   //     VaccinationCenter vc2= vaccinationCenterStore1.createVaccinationCenter("Center1","Adress1",3,"Email1",1,"WebsiteAdress",Time.valueOf("08:00:00"),Time.valueOf("18:00:00"),5,2);
   //     Vaccine vaccine=vaccineStore.createNewVaccine("Name1","Brand1","Code1");
   //     VaccineSchedule vs1= vss1.createVaccineSchedule(11,vc2,"18-05-2022","09:00",vaccine);
   //     vss1.saveVaccineSchedule(vs1);
   //     VaccineSchedule vs2= vss1.createVaccineSchedule(12,vc2,"18-05-2022","09:00",vaccine);
   //     boolean expected=true;
   //     boolean result= vss1.checkSameVaccine(vs2);
   //     Assertions.assertEquals(expected,result);
   // }
   // /**
   //  * Test for the checkSameVaccine method with 2 equals schedule.
   //  */
   // @Test
   // public void testCheckSameVaccine(){
   //     VaccinationCenterStore vaccinationCenterStore1=new VaccinationCenterStore();
   //     VaccinationScheduleStore vss1=new VaccinationScheduleStore();
   //     VaccinationCenter vc2= vaccinationCenterStore1.createVaccinationCenter("Center1","Adress1",3,"Email1",1,"WebsiteAdress",Time.valueOf("08:00:00"),Time.valueOf("18:00:00"),5,2);
   //     Vaccine vaccine=vaccineStore.createNewVaccine("Name1","Brand1","Code1");
   //     VaccineSchedule vs1= vss1.createVaccineSchedule(11,vc2,"18-05-2022","09:00",vaccine);
   //     vss1.saveVaccineSchedule(vs1);
   //     VaccineSchedule vs2= vss1.createVaccineSchedule(11,vc2,"18-05-2022","09:00",vaccine);
   //     boolean expected=false;
   //     boolean result= vss1.checkSameVaccine(vs2);
   //     Assertions.assertEquals(expected,result);
   // }
   // /**
   //  * Test for the checkMaxSlot method with a not full max slot.
   //  */
   // @Test
   // public void testCheckMaxSlot(){
   //     ArrayList<VaccineSchedule> vslist= new ArrayList<>();
   //     VaccinationCenterStore vaccinationCenterStore1=new VaccinationCenterStore();
   //     VaccinationScheduleStore vss1=new VaccinationScheduleStore();
   //     VaccinationCenter vc2= vaccinationCenterStore1.createVaccinationCenter("Center1","Adress1",3,"Email1",1,"WebsiteAdress",Time.valueOf("08:00:00"),Time.valueOf("19:00:00"),5,2);
   //     Vaccine vaccine=vaccineStore.createNewVaccine("Name1","Brand1","Code1");
   //     VaccineSchedule vs1= vss1.createVaccineSchedule(11,vc2,"18-05-2022","09:00",vaccine);
   //     VaccineSchedule vs2= vss1.createVaccineSchedule(11,vc2,"18-05-2022","09:00",vaccine);
   //     vslist.add(vs1);
   //     boolean expected=true;
   //     boolean result= vss1.checkMaxSlot(vs2);
   //     Assertions.assertEquals(expected,result);
   // }
   // /**
   //  * Test for the checkSameVaccine method with full max slot.
   //  */
   // @Test
   // public void testCheckMaxSlotFull(){
   //     VaccinationCenterStore vaccinationCenterStore1=new VaccinationCenterStore();
   //     VaccinationCenter vc2= vaccinationCenterStore1.createVaccinationCenter("Center1","Adress1",3,"Email1",1,"WebsiteAdress",Time.valueOf("08:00:00"),Time.valueOf("17:00:00"),5,2);
   //     VaccinationScheduleStore vss1=new VaccinationScheduleStore();
   //     Vaccine vaccine=vaccineStore.createNewVaccine("Name1","Brand1","Code1");
   //     VaccineSchedule vs1= vss1.createVaccineSchedule(11,vc2,"18-05-2022","09:00:",vaccine);
   //     vss1.saveVaccineSchedule(vs1);
   //     VaccineSchedule vs2= vss1.createVaccineSchedule(11,vc2,"18-05-2022","09:00",vaccine);
   //     vss1.saveVaccineSchedule(vs1);
   //     VaccineSchedule vs3= vss1.createVaccineSchedule(11,vc2,"18-05-2022","09:00",vaccine);
   //     boolean expected=false;
   //     boolean result= vss1.checkMaxSlot(vs3);
   //     Assertions.assertEquals(expected,result);
   // }
   // /**
   //  * Test for the checkSlotDuration method with a valid time .
   //  */
   // @Test
   // public void testSlotDurationValidTime(){
   //     VaccinationCenterStore vaccinationCenterStore1=new VaccinationCenterStore();
   //     VaccinationScheduleStore vss1=new VaccinationScheduleStore();
   //     VaccinationCenter vc2= vaccinationCenterStore1.createVaccinationCenter("Center1","Adress1",3,"Email1",1,"WebsiteAdress",Time.valueOf("08:00:00"),Time.valueOf("18:00:00"),5,2);
   //     Vaccine vaccine=vaccineStore.createNewVaccine("Name1","Brand1","Code1");
   //     VaccineSchedule vs1= vss1.createVaccineSchedule(11,vc2,"18-05-2022","09:00",vaccine);
   //     boolean expected=true;
   //     boolean result= vss1.checkSlotDuration(vs1);
   //     Assertions.assertEquals(expected,result);
   // }
   // /**
   //  * Test for the checkSlotDuration method with a invalid time .
   //  */
   // @Test
   // public void testSlotDurationInvalidTime(){
   //     VaccinationCenterStore vaccinationCenterStore1=new VaccinationCenterStore();
   //     VaccinationScheduleStore vss1=new VaccinationScheduleStore();
   //     VaccinationCenter vc2= vaccinationCenterStore1.createVaccinationCenter("Center1","Adress1",3,"Email1",1,"WebsiteAdress",Time.valueOf("08:00:00"),Time.valueOf("18:00:00"),5,2);
   //     Vaccine vaccine=vaccineStore.createNewVaccine("Name1","Brand1","Code1");
   //     VaccineSchedule vs1= vss1.createVaccineSchedule(11,vc2,"18-05-2022","09:03",vaccine);
   //     boolean expected=false;
   //     boolean result= vss1.checkSlotDuration(vs1);
   //     Assertions.assertEquals(expected,result);
   // }












}
