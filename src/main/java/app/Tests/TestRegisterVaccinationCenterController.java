package app.Tests;
import app.controller.RegisterVaccinationCenterController;
import app.domain.model.Company;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.sql.Time;

/**
 * @author Rodrigo Ferreira <1211030@isep.ipp.pt>
 * Tests for the RegisterVaccinationCenterController class
 */
public class TestRegisterVaccinationCenterController {
    Company c1=new Company("Companhia 1");
    RegisterVaccinationCenterController cr1=new RegisterVaccinationCenterController(c1);

    /**
     * Test for the createVaccinationCenter method
     */
    @Test
    public void testCreateVaccinationCenter(){
        boolean expected = true;
        boolean result= cr1.createVaccinationCenter("Center1","Street1",1111,"email",11,"websiteadress", Time.valueOf("08:00:00"),Time.valueOf("17:00:00"),5,10);
        Assertions.assertEquals(expected, result);
    }


    /**
     * Test for the saveVaccinationCenter method with a valid center
     */
    @Test
    public void testSaveVaccinationCenterValid(){
        cr1.createVaccinationCenter("Center1","Street1",1111,"email",11,"websiteadress", Time.valueOf("08:00:00"),Time.valueOf("17:00:00"),5,10);
        boolean expected=true;
        boolean result= cr1.saveVaccinationCenter();
        Assertions.assertEquals(expected,result);
    }
    /**
     * Test for the saveVaccinationCenter method with an invalid valid center
     */
    @Test
    public void testSaveVaccinationCenterInvalid(){
        boolean expected=false;
        boolean result= cr1.saveVaccinationCenter();
        Assertions.assertEquals(expected,result);
    }
}
