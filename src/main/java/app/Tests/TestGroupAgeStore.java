package app.Tests;
import app.domain.model.GroupAgeOfThePerson;
import app.domain.model.stores.GroupAgeStore;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.ArrayList;

public class TestGroupAgeStore {
    /**
     * Test for testVerifyGroupAgeOfThePerson method with a null instance
     */



    @Test
    public void testVerifyGroupAgeOfThePersonnull(){
        GroupAgeStore groupAgeStore=new GroupAgeStore();
        GroupAgeOfThePerson g1 = null;
        boolean exp = false;
        boolean res =groupAgeStore.verifyGroupAgeOfThePerson(g1);
        Assertions.assertEquals(exp,res);
    }

    /**
     * Test for testVerifyGroupAgeOfThePerson method with a valid age group
     */

    @Test
    public void testVerifyGroupAgeOfThePerson(){
        ArrayList<GroupAgeOfThePerson> groupAgelist= new ArrayList<>();
        GroupAgeStore groupAgeStore=new GroupAgeStore();
        GroupAgeOfThePerson g1=groupAgeStore.createNewGroupAge("Adult");
        groupAgelist.add(g1);
        boolean exp = true;
        boolean res = groupAgeStore.verifyGroupAgeOfThePerson(g1);
        Assertions.assertEquals(exp,res);
    }

    /**
     * Test for testSaveGroupAgeOfThePerson method with a valid age group
     */

    @Test
    public void testSaveGroupAgeOfThePerson(){
        ArrayList<GroupAgeOfThePerson> groupAgelist= new ArrayList<>();
        GroupAgeStore groupAgeStore=new GroupAgeStore();
        GroupAgeOfThePerson g1=groupAgeStore.createNewGroupAge("Adult");
        groupAgelist.add(g1);
        boolean exp = true;
        boolean res = groupAgeStore.saveGroupAgeOfThePerson(g1);
        Assertions.assertEquals(exp,res);
    }

    /**
     * Test for testSaveGroupAgeOfThePerson method with a null instance
     */


    @Test
    public void testSaveGroupAgeOfThePersonnull(){
        GroupAgeStore groupAgeStore=new GroupAgeStore();
        GroupAgeOfThePerson g1 = null;
        boolean exp = false;
        boolean res =groupAgeStore.saveGroupAgeOfThePerson(g1);
        Assertions.assertEquals(exp,res);
    }

}
