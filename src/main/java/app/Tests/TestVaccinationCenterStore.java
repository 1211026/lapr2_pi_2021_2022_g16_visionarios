package app.Tests;
import app.domain.model.VaccinationCenter;
import app.domain.model.stores.VaccinationCenterStore;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.sql.Time;
import java.util.ArrayList;

public class TestVaccinationCenterStore {
    @Test
    public void testValidateVaccinationCenterNull(){
        VaccinationCenterStore vaccinationCenterStore=new VaccinationCenterStore();
        VaccinationCenter vc1=null;
        boolean expected=false;
        boolean result= vaccinationCenterStore.validateVaccinationCenter(vc1);
        Assertions.assertEquals(expected,result);
    }
    /**
     * Test for the validateVaccinationCenter method with a instance of VaccinationCenter
     */
    @Test
    public void testValidateVaccinationCenter(){
        VaccinationCenterStore vaccinationCenterStore=new VaccinationCenterStore();
        VaccinationCenter vc1= vaccinationCenterStore.createVaccinationCenter("Center1","Street1",1111,"email",11,"websiteadress", Time.valueOf("08:00:00"),Time.valueOf("17:00:00"),5,10);
        boolean expected=true;
        boolean result= vaccinationCenterStore.validateVaccinationCenter(vc1);
        Assertions.assertEquals(expected,result);
    }
    /**
     * Test for saveVaccinationCenter method with a valid center
     */
    @Test
    public void testSaveVaccinationCenter(){
        ArrayList<VaccinationCenter> vclist= new ArrayList<>();
        VaccinationCenterStore vaccinationCenterStore=new VaccinationCenterStore();
        VaccinationCenter vc1= vaccinationCenterStore.createVaccinationCenter("Center1","Street1",1111,"email",11,"websiteadress",Time.valueOf("08:00:00"), Time.valueOf("17:00:00"),5,10);
        vclist.add(vc1);
        boolean expected=true;
        boolean result= vaccinationCenterStore.saveVaccinationCenter(vc1);
        Assertions.assertEquals(expected,result);
    }
    /**
     * Test for the saveVaccinationCenter method with a null instance
     */
    @Test
    public void testSaveVaccinationCenterNull(){
        VaccinationCenterStore vaccinationCenterStore=new VaccinationCenterStore();
        VaccinationCenter vc1= null;
        boolean expected=false;
        boolean result= vaccinationCenterStore.saveVaccinationCenter(vc1);
        Assertions.assertEquals(expected,result);
    }

}
