package app.Tests;
import app.domain.model.VaccinationCenter;
import app.domain.model.Vaccine;
import app.domain.model.VaccineSchedule;
import app.domain.model.VaccineType;
import app.domain.model.stores.VaccinationCenterStore;
import app.domain.model.stores.VaccinationScheduleStore;
import app.domain.model.stores.VaccineStore;
import app.domain.model.stores.VaccineTypeStore;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.sql.Time;
import java.util.ArrayList;

public class TestVaccineScheduleStore {
    /**
     * @author Rodrigo Ferreira <1211030@isep.ipp.pt>
     * Tests for the Company class
     */
    @Test
    public void testValidateVaccineSchedule(){
        VaccinationCenterStore vaccinationCenterStore1=new VaccinationCenterStore();
        VaccinationScheduleStore vss1=new VaccinationScheduleStore();
        VaccineTypeStore vaccineTypeStore=new VaccineTypeStore();
        VaccinationCenter vc2= vaccinationCenterStore1.createVaccinationCenter("Center1","Address1",3,"Email1",1,"WebsiteAdress", Time.valueOf("08:00:00"),Time.valueOf("18:00:00"),5,2);
        VaccineType vaccineType=vaccineTypeStore.createVaccineType("Name1","Code1",5);
        VaccineSchedule vs1= vss1.createVaccineSchedule(11,vc2,"18-05-2022","09:00",vaccineType);
        boolean expected=true;
        boolean result= vss1.validateVaccineSchedule(vs1);
        Assertions.assertEquals(expected,result);
    }
    /**
     * Test for saveVaccinechedule method with a valid schedule.
     */
    @Test
    public void testSaveVaccineSchedule(){
        VaccinationCenterStore vaccinationCenterStore1=new VaccinationCenterStore();
        VaccinationScheduleStore vss1=new VaccinationScheduleStore();
        VaccineTypeStore vaccineTypeStore=new VaccineTypeStore();
        VaccinationCenter vc2= vaccinationCenterStore1.createVaccinationCenter("Center1","Address1",3,"Email1",1,"WebsiteAdress",Time.valueOf("08:00:00"),Time.valueOf("18:00:00"),5,2);
        VaccineType vaccineType=vaccineTypeStore.createVaccineType("Name1","Code1",5);
        VaccineSchedule vs1= vss1.createVaccineSchedule(11,vc2,"18-05-2022","09:00",vaccineType);
        boolean expected=true;
        boolean result= vss1.saveVaccineSchedule(vs1);
        Assertions.assertEquals(expected,result);
    }
    /**
     * Test for the saveVaccineSchedule method with a null instance.
     */
    @Test
    public void testSaveVaccineScheduleNull(){
        VaccinationCenterStore vaccinationCenterStore1=new VaccinationCenterStore();
        VaccinationScheduleStore vss1=new VaccinationScheduleStore();
        VaccinationCenter vc2= vaccinationCenterStore1.createVaccinationCenter("Center1","Address1",3,"Email1",1,"WebsiteAdress",Time.valueOf("08:00:00"),Time.valueOf("18:00:00"),5,2);
        VaccineSchedule vs1= null;
        boolean expected=false;
        boolean result= vss1.saveVaccineSchedule(vs1);
        Assertions.assertEquals(expected,result);
    }
    /**
     * Test for the checkSameVaccine method with one schedule.
     */
    @Test
    public void testCheckNotSameVaccine(){
        VaccinationCenterStore vaccinationCenterStore1=new VaccinationCenterStore();
        VaccinationScheduleStore vss1=new VaccinationScheduleStore();
        VaccineTypeStore vaccineTypeStore=new VaccineTypeStore();
        VaccinationCenter vc2= vaccinationCenterStore1.createVaccinationCenter("Center1","Address1",3,"Email1",1,"WebsiteAdress",Time.valueOf("08:00:00"),Time.valueOf("18:00:00"),5,2);
        VaccineType vaccineType=vaccineTypeStore.createVaccineType("Name1","Code1",5);
        VaccineSchedule vs1= vss1.createVaccineSchedule(11,vc2,"18-05-2022","09:00",vaccineType);
        vss1.saveVaccineSchedule(vs1);
        VaccineSchedule vs2= vss1.createVaccineSchedule(12,vc2,"18-05-2022","09:00",vaccineType);
        boolean expected=true;
        boolean result= vss1.checkSameVaccine(vs2);
        Assertions.assertEquals(expected,result);
    }
    /**
     * Test for the checkSameVaccine method with 2 equals schedule.
     */
    @Test
    public void testCheckSameVaccine(){
        VaccinationCenterStore vaccinationCenterStore1=new VaccinationCenterStore();
        VaccinationScheduleStore vss1=new VaccinationScheduleStore();
        VaccineTypeStore vaccineTypeStore=new VaccineTypeStore();
        VaccinationCenter vc2= vaccinationCenterStore1.createVaccinationCenter("Center1","Address1",3,"Email1",1,"WebsiteAdress",Time.valueOf("08:00:00"),Time.valueOf("18:00:00"),5,2);
        VaccineType vaccineType=vaccineTypeStore.createVaccineType("Name1","Code1",5);
        VaccineSchedule vs1= vss1.createVaccineSchedule(11,vc2,"18-05-2022","09:00",vaccineType);
        vss1.saveVaccineSchedule(vs1);
        VaccineSchedule vs2= vss1.createVaccineSchedule(11,vc2,"18-05-2022","09:00",vaccineType);
        boolean expected=false;
        boolean result= vss1.checkSameVaccine(vs2);
        Assertions.assertEquals(expected,result);
    }
    /**
     * Test for the checkMaxSlot method with a not full max slot.
     */
    @Test
    public void testCheckMaxSlot(){
        ArrayList<VaccineSchedule> vslist= new ArrayList<>();
        VaccinationCenterStore vaccinationCenterStore1=new VaccinationCenterStore();
        VaccinationScheduleStore vss1=new VaccinationScheduleStore();
        VaccineTypeStore vaccineTypeStore=new VaccineTypeStore();
        VaccinationCenter vc2= vaccinationCenterStore1.createVaccinationCenter("Center1","Address1",3,"Email1",1,"WebsiteAdress",Time.valueOf("08:00:00"),Time.valueOf("18:00:00"),5,2);
        VaccineType vaccineType=vaccineTypeStore.createVaccineType("Name1","Code1",5);
        VaccineSchedule vs1= vss1.createVaccineSchedule(11,vc2,"18-05-2022","09:00",vaccineType);
        VaccineSchedule vs2= vss1.createVaccineSchedule(11,vc2,"18-05-2022","09:00",vaccineType);
        vslist.add(vs1);
        boolean expected=true;
        boolean result= vss1.checkMaxSlot(vs2);
        Assertions.assertEquals(expected,result);
    }
    /**
     * Test for the checkSameVaccine method with full max slot.
     */
    @Test
    public void testCheckMaxSlotFull(){
        VaccinationCenterStore vaccinationCenterStore1=new VaccinationCenterStore();
        VaccinationCenter vc2= vaccinationCenterStore1.createVaccinationCenter("Center1","Address1",3,"Email1",1,"WebsiteAdress",Time.valueOf("08:00:00"),Time.valueOf("18:00:00"),5,2);
        VaccinationScheduleStore vss1=new VaccinationScheduleStore();
        VaccineTypeStore vaccineTypeStore=new VaccineTypeStore();
        VaccineType vaccineType=vaccineTypeStore.createVaccineType("Name1","Code1",5);
        VaccineSchedule vs1= vss1.createVaccineSchedule(11,vc2,"18-05-2022","09:00",vaccineType);
        vss1.saveVaccineSchedule(vs1);
        VaccineSchedule vs2= vss1.createVaccineSchedule(11,vc2,"18-05-2022","09:00",vaccineType);
        vss1.saveVaccineSchedule(vs1);
        VaccineSchedule vs3= vss1.createVaccineSchedule(11,vc2,"18-05-2022","09:00",vaccineType);
        boolean expected=false;
        boolean result= vss1.checkMaxSlot(vs3);
        Assertions.assertEquals(expected,result);
    }
    /**
     * Test for the checkSlotDuration method with a valid time .
     */
    @Test
    public void testSlotDurationValidTime(){
        VaccinationCenterStore vaccinationCenterStore1=new VaccinationCenterStore();
        VaccinationScheduleStore vss1=new VaccinationScheduleStore();
        VaccineTypeStore vaccineTypeStore=new VaccineTypeStore();
        VaccinationCenter vc2= vaccinationCenterStore1.createVaccinationCenter("Center1","Address1",3,"Email1",1,"WebsiteAdress",Time.valueOf("08:00:00"),Time.valueOf("18:00:00"),5,2);
        VaccineType vaccineType=vaccineTypeStore.createVaccineType("Name1","Code1",5);
        VaccineSchedule vs1= vss1.createVaccineSchedule(11,vc2,"18-05-2022","09:00",vaccineType);
        boolean expected=true;
        boolean result= vss1.checkSlotDuration(vs1);
        Assertions.assertEquals(expected,result);
    }
    /**
     * Test for the checkSlotDuration method with a invalid time .
     */
    @Test
    public void testSlotDurationInvalidTime(){
        VaccinationCenterStore vaccinationCenterStore1=new VaccinationCenterStore();
        VaccinationScheduleStore vss1=new VaccinationScheduleStore();
        VaccineTypeStore vaccineTypeStore=new VaccineTypeStore();
        VaccinationCenter vc2= vaccinationCenterStore1.createVaccinationCenter("Center1","Address1",3,"Email1",1,"WebsiteAdress",Time.valueOf("08:00:00"),Time.valueOf("18:00:00"),5,2);
        VaccineType vaccineType=vaccineTypeStore.createVaccineType("Name1","Code1",5);
        VaccineSchedule vs1= vss1.createVaccineSchedule(11,vc2,"18-05-2022","09:03",vaccineType);
        boolean expected=false;
        boolean result= vss1.checkSlotDuration(vs1);
        Assertions.assertEquals(expected,result);
    }
}
