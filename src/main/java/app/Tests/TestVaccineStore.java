package app.Tests;
import app.domain.model.Vaccine;
import app.domain.model.stores.VaccineStore;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.ArrayList;

public class TestVaccineStore {
    @Test
    public void testVerifyNewVaccinenull(){
        VaccineStore vaccineStore=new VaccineStore();
        Vaccine vac1 = null;
        boolean exp = false;
        boolean res =vaccineStore.verifyNewVaccine(vac1);
        Assertions.assertEquals(exp,res);
    }

    /**
     * Test for testVerifyNewVaccine method with a valid vaccine
     */

    @Test
    public void testVerifyNewVaccine(){
        ArrayList<Vaccine> vaclist= new ArrayList<>();
        VaccineStore vaccineStore=new VaccineStore();
        Vaccine vac1=vaccineStore.createNewVaccine("Vaccine1","XXXX","MHDP");
        vaclist.add(vac1);
        boolean exp = true;
        boolean res = vaccineStore.verifyNewVaccine(vac1);
        Assertions.assertEquals(exp,res);
    }

    /**
     * Test for testSaveNewVaccine method with a valid vaccine
     */

    @Test
    public void testSaveNewVaccine(){
        ArrayList<Vaccine> vaclist= new ArrayList<>();
        VaccineStore vaccineStore=new VaccineStore();
        Vaccine vac1=vaccineStore.createNewVaccine("Vaccine1","XXXX","MHDP");
        vaclist.add(vac1);
        boolean exp = true;
        boolean res = vaccineStore.saveNewVaccine(vac1);
        Assertions.assertEquals(exp,res);
    }

    /**
     * Test for testSaveNewVaccine method with a null instance
     */


    @Test
    public void testSaveNewVaccinenull(){
        VaccineStore vaccineStore=new VaccineStore();
        Vaccine vac1 = null;
        boolean exp = false;
        boolean res =vaccineStore.saveNewVaccine(vac1);
        Assertions.assertEquals(exp,res);
    }
}
