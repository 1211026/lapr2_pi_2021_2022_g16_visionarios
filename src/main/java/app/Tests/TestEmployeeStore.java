package app.Tests;
import app.domain.model.Employee;
import app.domain.model.stores.EmployeeStore;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.ArrayList;

public class TestEmployeeStore {
    /**
     * @author Diogo Teixeira <1210916@isep.ipp.pt>
     * Test for the testValidateNewEmployee method with a null instance
     */

    @Test
    public void testValidateNewEmployeenull(){
        EmployeeStore employeeStore=new EmployeeStore();
        Employee ne1 = null;
        boolean exp = false;
        boolean res =employeeStore.validateNewEmployee(ne1);
        Assertions.assertEquals(exp,res);
    }

    /**
     * Test dot the testValidateEmployee with a valid new employee
     */

    @Test
    public void testvalidateNewEmployee(){
        EmployeeStore employeeStore=new EmployeeStore();
        Employee ne1= employeeStore.createNewEmployee(12345, "Name1", "Street1", "email@gmail.com",911111111 ,1234567 , "Nurse");
        boolean exp = true;
        boolean res= employeeStore.validateNewEmployee(ne1);
        Assertions.assertEquals(exp,res);

    }

    /**
     * Test for testSaveEmployee method with a null instance
     */

    @Test
    public void testSaveEmloyeeNull(){
        EmployeeStore employeeStore=new EmployeeStore();
        Employee ne1 = null;
        boolean exp = false;
        boolean res = employeeStore.saveNewEmployee(ne1);
        Assertions.assertEquals(exp,res);

    }

    /**
     * Test for testSaveEmployee method with a valid New Employee
     */

    @Test
    public void testSaveNewEmployee(){
        ArrayList<Employee> nelist= new ArrayList<>();
        EmployeeStore employeeStore=new EmployeeStore();
        Employee ne1= employeeStore.createNewEmployee(12345, "Name1", "Street1", "email@gmail.com",911111111 ,1234567 , "Nurse");
        nelist.add(ne1);
        boolean exp = true;
        boolean res = employeeStore.saveNewEmployee(ne1);
        Assertions.assertEquals(exp,res);
    }
}
