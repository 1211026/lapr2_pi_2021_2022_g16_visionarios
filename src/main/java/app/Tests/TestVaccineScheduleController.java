package app.Tests;
import app.domain.model.Vaccine;
import app.domain.model.VaccineType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import app.controller.VaccineScheduleController;
import app.domain.model.VaccinationCenter;
import app.domain.model.Company;

import java.sql.Time;

/**
 * Tests for the VaccineScheduleController class
 */
public class TestVaccineScheduleController {

    /**
     * Test for the createVaccineSchedule method for US02
     */
    @Test
    public void testCreateVaccineSchedule(){
        Company c1=new Company("Companhia 1");
        VaccineScheduleController cr1=new VaccineScheduleController(c1);
        VaccinationCenter vc1=new VaccinationCenter("Center1","Adress1",3,"Email1",1,"WebsiteAddress", Time.valueOf("08:00:00"),Time.valueOf("18:00:00"),5,3);
        VaccineType vaccineType=new VaccineType("Name1","Code1",5);
        boolean expected=true;
        boolean result= cr1.createVaccineSchedule(11,vc1,"20/05/2022","15:00",vaccineType);
        Assertions.assertEquals(expected,result);
    }

    /**
     * Test for the createVaccineSchedule method for US02
     */
    @Test
    public void testCreateVaccineScheduleReceptionist(){
        Company c1=new Company("Companhia 1");
        VaccineScheduleController cr1=new VaccineScheduleController(c1);
        VaccinationCenter vc1 = new VaccinationCenter("Center1","Adress1",3,"Email1",1,"WebsiteAddress",Time.valueOf("08:00:00"),Time.valueOf("18:00:00"),5,3);
        VaccineType vaccineType=new VaccineType("Name1","Code1",5);
        boolean expected = true;
        boolean result= cr1.createVaccineSchedule(11,vc1,"20/05/2022","15:00",vaccineType);
        Assertions.assertEquals(expected,result);
    }


    /**
     * Test for the saveVaccineSchedule method with a valid schedule.
     */
    @Test
    public void testSaveVaccineSchedule(){
        Company c1=new Company("Companhia 1");
        VaccineScheduleController cr1=new VaccineScheduleController(c1);
        VaccinationCenter vc1=new VaccinationCenter("Center1","Adress1",3,"Email1",1,"WebsiteAddress",Time.valueOf("08:00:00"),Time.valueOf("18:00:00"),5,3);
        VaccineType vaccineType=new VaccineType("Name1","Code1",5);
        cr1.createVaccineSchedule(11,vc1,"20/05/2022","15:00",vaccineType);
        boolean expected=true;
        boolean result= cr1.saveVaccineSchedule();
        Assertions.assertEquals(expected,result);
    }
    /**
     * Test for the saveVaccineSchedule method with a invalid schedule.
     */
    @Test
    public void testSaveVaccinationCenterInvalid(){
        Company c1=new Company("Companhia 1");
        VaccineScheduleController cr1=new VaccineScheduleController(c1);
        boolean expected = false;
        boolean result= cr1.saveVaccineSchedule();
        Assertions.assertEquals(expected,result);
    }
    /**
     * Test for the sameVaccine method with 2 different vaccines.
     */
    @Test
    public void testCheckSameVaccine(){
        Company c1=new Company("Companhia 1");
        VaccineScheduleController cr1=new VaccineScheduleController(c1);
        VaccinationCenter vc1=new VaccinationCenter("Center1","Adress1",3,"Email1",1,"WebsiteAddress",Time.valueOf("08:00:00"),Time.valueOf("18:00:00"),5,3);
        VaccineType vaccineType=new VaccineType("Name1","Code1",5);
        cr1.createVaccineSchedule(11,vc1,"20/05/2022","15:00",vaccineType);
        boolean expected=true;
        boolean result= cr1.checkSameVaccine();
        Assertions.assertEquals(expected,result);
    }
    /**
     * Test for the sameVaccine method with 2 equal vaccines.
     */
    @Test
    public void testCheckSameVaccineEqual(){
        Company c1=new Company("Companhia 1");
        VaccineScheduleController cr1=new VaccineScheduleController(c1);
        VaccinationCenter vc1=new VaccinationCenter("Center1","Adress1",3,"Email1",1,"WebsiteAddress",Time.valueOf("08:00:00"),Time.valueOf("18:00:00"),5,3);
        VaccineType vaccineType=new VaccineType("Name1","Code1",5);
        cr1.createVaccineSchedule(11,vc1,"20/05/2022","15:00",vaccineType);
        cr1.saveVaccineSchedule();
        cr1.createVaccineSchedule(11,vc1,"20/05/2022","15:00",vaccineType);
        cr1.saveVaccineSchedule();
        boolean expected=false;
        boolean result= cr1.checkSameVaccine();
        Assertions.assertEquals(expected,result);
    }
    /**
     * Test for the checkMaxSlot method with not full slot.
     */
    @Test
    public void testCheckMaxSlot(){
        Company c1=new Company("Companhia 1");
        VaccineScheduleController cr1=new VaccineScheduleController(c1);
        VaccinationCenter vc1=new VaccinationCenter("Center1","Adress1",3,"Email1",1,"WebsiteAddress",Time.valueOf("08:00:00"),Time.valueOf("18:00:00"),5,3);
        VaccineType vaccineType=new VaccineType("Name1","Code1",5);
        cr1.createVaccineSchedule(11,vc1,"20/05/2022","15:00",vaccineType);
        cr1.saveVaccineSchedule();
        cr1.createVaccineSchedule(11,vc1,"20/05/2022","15:00",vaccineType);
        cr1.saveVaccineSchedule();
        boolean expected=true;
        boolean result= cr1.checkMaxSlot();
        Assertions.assertEquals(expected,result);
    }
    /**
     * Test for the checkMaxSlot method with full slot.
     */
    @Test
    public void testCheckMaxSlotFull(){
        Company c1=new Company("Companhia 1");
        VaccineScheduleController cr1=new VaccineScheduleController(c1);
        VaccinationCenter vc1=new VaccinationCenter("Center1","Adress1",3,"Email1",1,"WebsiteAddress",Time.valueOf("08:00:00"),Time.valueOf("18:00:00"),5,2);
        VaccineType vaccineType=new VaccineType("Name1","Code1",5);
        cr1.createVaccineSchedule(11,vc1,"20/05/2022","15:00",vaccineType);
        cr1.saveVaccineSchedule();
        cr1.createVaccineSchedule(11,vc1,"20/05/2022","15:00",vaccineType);
        cr1.createVaccineSchedule(11,vc1,"20/05/2022","15:00",vaccineType);
        cr1.saveVaccineSchedule();
        boolean expected=false;
        boolean result= cr1.checkMaxSlot();
        Assertions.assertEquals(expected,result);
    }
    /**
     * Test for the checkSlotDuration method with valid time.
     */
    @Test
    public void testCheckSlotDuration(){
        Company c1=new Company("Companhia 1");
        VaccineScheduleController cr1=new VaccineScheduleController(c1);
        VaccinationCenter vc1=new VaccinationCenter("Center1","Adress1",3,"Email1",1,"WebsiteAddress",Time.valueOf("08:00:00"),Time.valueOf("18:00:00"),5,3);
        VaccineType vaccineType=new VaccineType("Name1","Code1",5);
        cr1.createVaccineSchedule(11,vc1,"20/05/2022","15:00",vaccineType);
        cr1.saveVaccineSchedule();
        boolean expected=true;
        boolean result= cr1.checkSlotDuration();
        Assertions.assertEquals(expected,result);
    }
    /**
     * Test for the checkSlotDuration method with  invalid time.
     */
    @Test
    public void testCheckSlotDurationInvalid(){
        Company c1=new Company("Companhia 1");
        VaccineScheduleController cr1=new VaccineScheduleController(c1);
        VaccinationCenter vc1=new VaccinationCenter("Center1","Adress1",3,"Email1",1,"WebsiteAddress",Time.valueOf("18:00:00"),Time.valueOf("18:00:00"),5,3);
        VaccineType vaccineType=new VaccineType("Name1","Code1",5);
        cr1.createVaccineSchedule(11,vc1,"20/05/2022","15:03",vaccineType);
        cr1.saveVaccineSchedule();
        boolean expected=false;
        boolean result= cr1.checkSlotDuration();
        Assertions.assertEquals(expected,result);
    }
}


