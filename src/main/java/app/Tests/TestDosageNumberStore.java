package app.Tests;
import app.domain.model.Dosage;
import app.domain.model.stores.DosageNumberStore;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.ArrayList;

public class TestDosageNumberStore {
    /**
     * Test for testVerifyDosageNumber method with a null instance
     */



    @Test
    public void testVerifyDosageNumbernull(){
        DosageNumberStore dosageNumberStore=new DosageNumberStore();
        Dosage dn1 = null;
        boolean exp = false;
        boolean res =dosageNumberStore.verifyDosageNumber(dn1);
        Assertions.assertEquals(exp,res);
    }

    /**
     * Test for testVerifyDosageNumber method with a valid dosage number
     */

    @Test
    public void testVerifyDosageNumber(){
        ArrayList<Dosage> dosageNumberlist= new ArrayList<>();
        DosageNumberStore dosageNumberStore=new DosageNumberStore();
        Dosage dn1=dosageNumberStore.createDosageNumber(15);
        dosageNumberlist.add(dn1);
        boolean exp = true;
        boolean res = dosageNumberStore.verifyDosageNumber(dn1);
        Assertions.assertEquals(exp,res);
    }

    /**
     * Test for testSaveDosageNumber method with a valid dosage number
     */

    @Test
    public void testSaveDosageNumber(){
        ArrayList<Dosage> dosageNumberlist= new ArrayList<>();
        DosageNumberStore dosageNumberStore=new DosageNumberStore();
        Dosage dn1=dosageNumberStore.createDosageNumber(15);
        dosageNumberlist.add(dn1);
        boolean exp = true;
        boolean res = dosageNumberStore.saveDosageNumber(dn1);
        Assertions.assertEquals(exp,res);
    }

    /**
     * Test for testSaveDosageNumber method with a null instance
     */


    @Test
    public void testSaveDosageNumbernull(){
        DosageNumberStore dosageNumberStore=new DosageNumberStore();
        Dosage dn1 = null;
        boolean exp = false;
        boolean res =dosageNumberStore.saveDosageNumber(dn1);
        Assertions.assertEquals(exp,res);
    }
}
