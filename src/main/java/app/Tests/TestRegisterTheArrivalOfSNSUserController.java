package app.Tests;

import app.controller.CreateSNSUserController;
import app.controller.RegisterTheArrivalOfSNSUserController;
import app.domain.model.Company;
import app.domain.model.stores.SNSUserStore;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * @author Diogo Teixeira<1210916@isep.ipp.pt>
 * Test for RegisyerTheArrivaçOfSMSUserController
 */
public class TestRegisterTheArrivalOfSNSUserController {
    RegisterTheArrivalOfSNSUserController userController= new RegisterTheArrivalOfSNSUserController();
    CreateSNSUserController createSNSUserController=new CreateSNSUserController();
    SNSUserStore snsUserStore=new SNSUserStore();
    @Test
    public void testVerifySNSUserValid(){
        int expected=0;
        createSNSUserController.createSNSUser("Diogo","male", LocalDate.parse("25/08/2003", DateTimeFormatter.ofPattern("dd/MM/yyyy")),"Rua 25 abril" ,939450858 , "drpt@gmail.com",123456789 ,123456789 );
        int result=userController.verifySNSUser("Diogo", 123456789);
        Assertions.assertEquals(expected, result);
    }
    @Test
    public void testVerifySNSUserWrongName(){
        int expected=2;
        createSNSUserController.createSNSUser("Diogo","male", LocalDate.parse("25/08/2003", DateTimeFormatter.ofPattern("dd/MM/yyyy")),"Rua 25 abril" ,939450858 , "drpt@gmail.com",123456789 ,123456789 );
        int result=userController.verifySNSUser("Diogo", 123456789);
        Assertions.assertEquals(expected, result);
    }

}
