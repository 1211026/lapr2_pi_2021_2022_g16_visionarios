package app.ui;
import app.controller.App;
import app.domain.model.DailyReportTimer;
import app.ui.console.*;
import pt.isep.lei.esoft.auth.mappers.dto.UserRoleDTO;
import javafx.application.Application;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */

public class Main  {

    public static void main(String[] args)
    {
        try
        {

            //execute the daily task
            App.getInstance().scheduleDailyReport();

            Main Menu;
            MainMenuUI menu = new MainMenuUI();
            menu.run();


        }
        catch( Exception e )
        {
            e.printStackTrace();
        }
    }
    }
