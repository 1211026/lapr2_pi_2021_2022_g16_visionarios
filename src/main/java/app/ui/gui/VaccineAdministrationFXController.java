package app.ui.gui;

import app.controller.VaccineAdministrationController;
import app.domain.model.DosageNumberDosageGroupAge;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.net.URL;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.ResourceBundle;


public class VaccineAdministrationFXController implements Initializable {
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }
    private VaccineAdministrationController vaccineAdministrationController=new VaccineAdministrationController();

    @FXML
    private TextField txtSnsUserNumber;

    @FXML
    private TextArea txtShowinformation;

    @FXML
    private Button btnConfirmInfo;

    @FXML
    private TextField txtVaccineType;

    @FXML
    private Button btnSelectVaccine;

    @FXML
    private TextField txtAgeUser;

    @FXML
    private Button btnConfirmSnsNumber;

    @FXML
    private TextArea txtShowVaccines;

    @FXML
    private TextField txtSelectVaccine;

    @FXML
    void selectUser(ActionEvent event) {
        int snsUserNumber=Integer.parseInt(txtSnsUserNumber.getText());
        txtShowinformation.setText(vaccineAdministrationController.checkUserInfo(snsUserNumber));

    }

    @FXML
    void selectInfo(ActionEvent event) {
        String vaccineType=txtVaccineType.getText();
        int age=Integer.parseInt(txtAgeUser.getText());
        txtShowVaccines.setText(String.valueOf(vaccineAdministrationController.checkVaccineToGive(vaccineType,age)));
    }

    @FXML
    void selectVaccine(ActionEvent event) {
        DosageNumberDosageGroupAge dosageNumberDosageGroupAge;
        ArrayList<DosageNumberDosageGroupAge> bestVaccineList= new ArrayList<>();
        String vaccineType=txtVaccineType.getText();
        int snsUserNumber=Integer.parseInt(txtSnsUserNumber.getText());
        int age=Integer.parseInt(txtAgeUser.getText());
        int index=Integer.parseInt(txtSelectVaccine.getText());
        bestVaccineList=vaccineAdministrationController.checkVaccineToGive(vaccineType,age);
        dosageNumberDosageGroupAge=bestVaccineList.get(index);
        vaccineAdministrationController.createVaccineAdministration(snsUserNumber,dosageNumberDosageGroupAge.getName(), dosageNumberDosageGroupAge.getBrand(), vaccineAdministrationController.checkDose(snsUserNumber), dosageNumberDosageGroupAge.getCode(), LocalDateTime.now(),vaccineAdministrationController.checkVaccinationCenter(snsUserNumber));
        vaccineAdministrationController.saveVaccineAdministration();
        if (vaccineAdministrationController.saveVaccineAdministration()==true){
            Alert a =new Alert(Alert.AlertType.INFORMATION,"Vaccine Administer with sucess");
        }else {
            Alert a =new Alert(Alert.AlertType.INFORMATION,"The administration was not register ");
        }

    }


}