package app.ui.gui;

import app.controller.LegacyDataController;
import app.domain.model.LegacyDataSort;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class LegacyDataFXController implements Initializable {

    @FXML
    private Button button2;

    @FXML
    private Button button3;

    @FXML
    private TextField txt1;

    @FXML
    private TextArea area1;

    @FXML
    private Pane Pane1;

    @FXML
    private Button button1;

    @FXML
    private ChoiceBox<String> choice1;
    private String[] sort = {"Bubble sort", "Quick Sort"};
    @FXML
    private ChoiceBox<String> choice2;
    private String[] sortpreference = {"Arrival Time", "Center Leaving Time"};


    @FXML
    void readFilePath(ActionEvent event) throws IOException {
    File filePath= new File(txt1.getText());
        LegacyDataController.legacyDataController(filePath);
    }

    @FXML
    void SelectsSortingMethod(ActionEvent event) {
    boolean option = choice1.getItems().addAll(sort);
    LegacyDataController.optionCheck();
    }

    @FXML
    void SelectsSortingPreference(ActionEvent event) throws IOException {
        boolean option = choice2.getItems().addAll(sortpreference);
        LegacyDataSort.bubbleSort(new File(txt1.getText()));
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }
}
