package app.ui.gui;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.io.IOException;

public class NumberFullyVaccinatedController {
    private app.controller.NumberFullyVaccinatedController numberFullyVaccinatedController= new app.controller.NumberFullyVaccinatedController();
    @FXML
    private TextField LastDateTFD;

    @FXML
    private Button OkBTN;

    @FXML
    private Label Error1LBL;

    @FXML
    private TextField FirstDateTFD;

    @FXML
    void ConvertFileFx(ActionEvent event) throws IOException {
        String firstDate=FirstDateTFD.getText();
        String lastDate=LastDateTFD.getText();
        int errorType;
        errorType=numberFullyVaccinatedController.ValidateInterval(firstDate, lastDate);
        if (errorType==4)
            Error1LBL.setText("The dates can not be empty.");
        if (errorType==2)
            Error1LBL.setText("The first date is invalid. Please enter a valid date!");
        if (errorType==3)
            Error1LBL.setText("The last date is invalid. Please enter a valid date!");
        if (errorType==1)
            Error1LBL.setText("The interval of days is invalid!!");
        if (errorType==0) {
            numberFullyVaccinatedController.ConvertNumberOfFullyVaccinatedToFile(firstDate,lastDate);
            Error1LBL.setText("The file has been created.");
        }
    }

}

