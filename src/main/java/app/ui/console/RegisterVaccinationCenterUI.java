package app.ui.console;
import app.controller.App;
import app.domain.model.Company;
import app.ui.console.utils.Utils;
import app.controller.RegisterVaccinationCenterController;
import app.domain.model.VaccinationCenter;

import java.sql.Time;
import java.util.Objects;

public class RegisterVaccinationCenterUI implements Runnable {
    private final Company c1 = App.getInstance().getCompany();
    private final RegisterVaccinationCenterController vcController = new RegisterVaccinationCenterController(c1);


    /**
     * @author Rodrigo Ferreira
     * @author Gabriel Silva <1210808@isep.ipp.pt>
     *
     */

    public void run() {
        System.out.println("Register new Vaccination Center");
        if (registerVaccinationCenter()){

            System.out.println("Center created with success");
        }else{
            System.out.println("Center not created try again");
        }
    }
    public boolean registerVaccinationCenter(){
        boolean flag = false;

        do {
            String name = Utils.readLineFromConsole("Enter name: ");
            while(Objects.equals(name, "")) {
                name = Utils.readLineFromConsole("Please enter a name for the vaccination center: ");
            }
            String address = Utils.readLineFromConsole("Enter address:");
            while(Objects.equals(address, "")){
                address = Utils.readLineFromConsole("Please enter a valid address for the vaccination center");
            }
            int phoneNumber = Utils.readIntegerFromConsole("Enter the phone number: ");
            while(phoneNumber == -1 || (phoneNumber <= 99999999 && phoneNumber >= 0)){
                phoneNumber = Utils.readIntegerFromConsole("Please enter a valid phone number with 9 digits: ");
            }
            String email=Utils.readLineFromConsole("Enter email:");
            while(Objects.equals(email, "") && !email.contains("@"))
            {
                email = Utils.readLineFromConsole("Please enter a valid email: ");
            }
            int fax = Utils.readIntegerFromConsole("Enter fax:");
            while(fax == -1 || (fax <= 99999999 && fax >= 0)){
                fax = Utils.readIntegerFromConsole("Please enter a valid fax with 9 digits: ");
            }
            String websiteAddress = Utils.readLineFromConsole("Enter website address:");
            while(Objects.equals(websiteAddress, "")){
                websiteAddress = Utils.readLineFromConsole("Please enter the website address:");
            }
            String openHours;
            Time openingHours = null;
            boolean check = false;
            while(!check){
                openHours = Utils.readLineFromConsole("Please enter the opening hours (HH:mm:ss): ");
                try{
                    if (openHours != null) {
                        openingHours = Time.valueOf(openHours);
                    }
                    check = true;
                }
                catch (Exception e){

                    System.out.println("Time format is not well written! Please use the format (HH:mm:ss) and try again \n");
                }
            }

            String closeHours;
            Time closingHours = null;
            check = false;
            while(!check){
                closeHours = Utils.readLineFromConsole("Please enter the opening hours (HH:mm:ss): ");
                try{
                    if (closeHours != null) {
                        closingHours = Time.valueOf(closeHours);
                    }
                    check = true;
                }
                catch (Exception e){

                    System.out.println("Time format is not well written! Please use the format (HH:mm:ss) and try again \n");
                }
            }

            int slotDuration=Utils.readIntegerFromConsole("Enter slot duration:");

            while(slotDuration == -1){
                slotDuration = Utils.readIntegerFromConsole("Please enter a valid slot number: ");
            }
            int maxSlotVaccines=Utils.readIntegerFromConsole("Enter max slot vaccines:");
            while(maxSlotVaccines == -1){
                maxSlotVaccines = Utils.readIntegerFromConsole("Please enter a valid max slot Vaccines number: ");
            }

            System.out.printf("\n\n|Name:%s\nAddress:%s\nPhone number:%s\nEmail:%s\nFax:%s\nWebsite address:%s\nOpening Hours:%s\nClosing Hours:%s\nSlot duration:%s\nMax slot vaccines:%s",name,address,phoneNumber,email,fax,websiteAddress,openingHours,closingHours,slotDuration,maxSlotVaccines);
            String confirm = Utils.readLineFromConsole("Confirm all the data:(Y/N)");

            while(!confirm.equals("Y")  && !confirm.equals("y") && !confirm.equals("n") && !confirm.equals("N")) {
                confirm = Utils.readLineFromConsole("Wrong option! \n Please insert one of the following characters to confirm the operation: (Y/N)");
            };
            if (confirm.equals("Y") || confirm.equals("y")){
                vcController.createVaccinationCenter(name,address,phoneNumber,email,fax,websiteAddress,openingHours,closingHours,slotDuration,maxSlotVaccines);
                flag = true;
                return vcController.saveVaccinationCenter();
            }
            else
            {
                System.out.println("You have chosen to not confirm all the data, so anything was saved");
            }

        }while (!flag);
        return flag;
    }
}
