package app.ui.console;

import app.controller.VaccineAdministrationController;
import app.domain.model.DosageNumberDosageGroupAge;
import app.domain.model.VaccineAdministration;
import app.ui.console.utils.Utils;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class VaccineAdministrationUI extends Application implements Runnable{
    VaccineAdministrationController vaccineAdministrationController=new VaccineAdministrationController();
    @Override
    public void run() {
        System.out.println("Register the vaccine administration");
        int option = Utils.readIntegerFromConsole("If you want to run on the console type 1, if you want JAvaFX type 2");
        if (option==1){
            if (registerVaccineAdministration()){
                System.out.println("Register the administration with sucess");
                vaccineAdministrationController.sendSms();
            }else {
                System.out.println("The administration was not register");
            }
        }else {
            launch();
        }
    }


    public boolean registerVaccineAdministration(){
        DosageNumberDosageGroupAge dosageNumberDosageGroupAge;
        int snsUserNumber = Utils.readIntegerFromConsole("Introduce the sns user number of the person to be vaccinated:");
        System.out.println(vaccineAdministrationController.checkUserInfo(snsUserNumber));
        String nameOfTheVaccine=Utils.readLineFromConsole("Introduce the name of the vaccine that was displayed:");
        int ageOfTheUser = Utils.readIntegerFromConsole("Introduce the age of the user that was displayed:");
        ArrayList<DosageNumberDosageGroupAge> bestVaccineList= new ArrayList<>();
        bestVaccineList=vaccineAdministrationController.checkVaccineToGive(nameOfTheVaccine,ageOfTheUser);
        Utils.showList(bestVaccineList,"List of possible vaccines to administer");
        int option = Utils.readIntegerFromConsole("Select the vaccine to be administer");
        dosageNumberDosageGroupAge = bestVaccineList.get(option-1);
        System.out.printf("\n\n|Sns User Number:%d\nVaccine Name:%s\nVaccine Brand:%s\nDose:%d\nLote:%s\nDate:%s\nVaccination Center:%s",snsUserNumber,dosageNumberDosageGroupAge.getName(),dosageNumberDosageGroupAge.getBrand(),vaccineAdministrationController.checkDose(snsUserNumber),dosageNumberDosageGroupAge.getCode(),LocalDate.now(),vaccineAdministrationController.checkVaccinationCenter(snsUserNumber));
        String confirm = Utils.readLineFromConsole("Confirm all the data:(Y/N");
        assert confirm != null;
        if (confirm.equals("Y") || confirm.equals("y")){
            vaccineAdministrationController.createVaccineAdministration(snsUserNumber,dosageNumberDosageGroupAge.getName(), dosageNumberDosageGroupAge.getBrand(), vaccineAdministrationController.checkDose(snsUserNumber), dosageNumberDosageGroupAge.getCode(), LocalDateTime.now(),vaccineAdministrationController.checkVaccinationCenter(snsUserNumber));
            return vaccineAdministrationController.saveVaccineAdministration();
        }else {
            return false;
        }
    }

    @Override
    public void start(Stage stage) throws Exception {
        try {
            Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("VaccineAdministration.fxml"));
            Scene scene=new Scene(root);
            stage.setScene(scene);
            stage.show();
        }catch (Exception a){
            a.printStackTrace();
        }
    }
}
