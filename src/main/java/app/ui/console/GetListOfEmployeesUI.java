package app.ui.console;
import app.controller.App;
import app.controller.GetListOfEmployeesController;
import app.domain.model.Company;
import app.domain.model.Employee;
import app.domain.model.stores.EmployeeStore;
import app.ui.console.utils.Utils;

import java.util.List;

/**
 * @author Gabriel Silva <1210808@isep.ipp.pt>
 */

public class GetListOfEmployeesUI implements Runnable{
    private EmployeeStore employeeStore = new EmployeeStore();
    private Company c1 = App.getInstance().getCompany();
    private GetListOfEmployeesController leController = new GetListOfEmployeesController();
    private List<Employee> listOfEmployees = employeeStore.getEmployeeList();

    public void run() {
        System.out.println("Get List of Employees by a given function");
        if (listOfEmployeesByRole()) {
            System.out.println("List obtained with success");
        }else{
            System.out.println("Failed to obtain list of the employees by the given role");
        }
    }

    /**
     * Method to print the list of the employees of a certain role/function
     * @return
     */

    public boolean listOfEmployeesByRole()
    {
        System.out.println("Choose the function desired to obtain the list of employees: ");
        String s = (String)Utils.showAndSelectOne(Employee.getListEmployeeRoles(), "Header");
        System.out.println(s);
        List<Employee> l = leController.getEmployeeList(s);
        if(!l.isEmpty())
        {
            System.out.println("All the employees with the role Receptionist: ");
            System.out.println();
            System.out.println("------------------------------------------------------------------");
            for(Employee item: leController.getEmployeeList(s)){
                System.out.println("Id: " + item.getId());
                System.out.println("Name: " + item.getName());
                System.out.println("Address: " + item.getAddress());
                System.out.println("Email: " + item.getEmail());
                System.out.println("Phone Number: " + item.getPhoneNumber());
                System.out.println("Citizen Card Number: " + item.getCitizenCardNumber());
            }
            System.out.println("------------------------------------------------------------------");

            return true;
        }

        return false;
    }
}


