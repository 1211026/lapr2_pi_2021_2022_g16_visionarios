package app.ui.console;

import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class NurseUI implements Runnable {

    public NurseUI()
    {
    }

    public void run() {
        List<MenuItem> options = new ArrayList<MenuItem>();
        options.add(new MenuItem("Option A- Consult the users in the waiting room of a vaccination center. ", new ConsultTheListOfSNSusersUI()));//new ShowTextUI("You have chosen Option A.")
        options.add(new MenuItem("Option B- Register the administration of a vaccine.", new VaccineAdministrationUI()));//new ShowTextUI("You have chosen Option E.")
        int option = 0;

        do
        {
            option = Utils.showAndSelectIndex(options, "\n\n Nurse Menu:");

            if ( (option >= 0) && (option < options.size()))
            {
                options.get(option).run();
            }
        }
        while (option != -1 );
    }
}

