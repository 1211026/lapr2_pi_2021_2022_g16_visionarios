package app.ui.console;

import app.controller.CreateSNSUserController;
import app.ui.console.utils.Utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class CreateSNSUserUI implements Runnable {

    /**
     * @author Diogo Teixeira <1210916>
     * @author Rui Milhazes <1211031>
     */
    CreateSNSUserController sns = new CreateSNSUserController();
    public void run() {

            System.out.println("Register new SNSUSer");
            if (registerSNSUser() == true) {
                System.out.println("SNSUser registered with success");
            } else {
                System.out.println("Employee not registered. Please try again");
            }
        }

    /**
     * Method to register the attributes for the SNS user
     * @return
     */
    private boolean registerSNSUser() {

            String name = Utils.readLineFromConsole("Enter name:");
            String sex = Utils.readLineFromConsole("Enter sex:");
            String dateBirth = Utils.readLineFromConsole("Enter date of birth ( dd/MM/yyyy):");
            String address = Utils.readLineFromConsole("Enter address:");
            String phoneNumber = Utils.readLineFromConsole("Enter phone number:");
            String email = Utils.readLineFromConsole("Enter email:");
            String id = Utils.readLineFromConsole("Enter id:");
            String CitizenNumber = Utils.readLineFromConsole("Enter citizen card number:");
            System.out.printf("\n\n|Id:%s\nName:%s\nAddress:%s\nEmail:%s\nPhone number:%s\nCitizen card number:%s", id, name, address, email, phoneNumber, CitizenNumber);
            String confirm = Utils.readLineFromConsole("Confirm all the data:(Y / N)");
            if (confirm.equals("Y") || confirm.equals("y")) {
                assert id != null;
                int idRes = Integer.parseInt(id);
                assert dateBirth !=null;
                LocalDate dateBirthRes = LocalDate.parse(dateBirth, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
                assert phoneNumber != null;
                int phoneNumberRes = Integer.parseInt(phoneNumber);
                assert CitizenNumber != null;
                int CitizenNumberRes = Integer.parseInt(CitizenNumber);
                sns.createSNSUser(name, sex, dateBirthRes, address, phoneNumberRes, email, idRes, CitizenNumberRes);
                return sns.saveSNSUser();
            } else {
                return false;
            }
        }
    }

