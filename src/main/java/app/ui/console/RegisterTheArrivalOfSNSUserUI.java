package app.ui.console;

import app.controller.RegisterTheArrivalOfSNSUserController;
import app.ui.console.utils.Utils;


public class RegisterTheArrivalOfSNSUserUI implements Runnable {
    /**
     * @author Diogo Teixeira <1210916>
     */
    private RegisterTheArrivalOfSNSUserController raController= new RegisterTheArrivalOfSNSUserController();

    public void run(){
        System.out.println("Register the arrival of SNS user");
        registerArrivalOfSNSUser();

    }

    /**
     * Method to register the SNS user upon arrival
     */

    public void registerArrivalOfSNSUser(){
        raController.createVaccinationScheduledForToday();
        String name= Utils.readLineFromConsole("Enter SNS user name: " );
        int SNSUserNumber=Utils.readIntegerFromConsole("Enter SNS user number: ");
        int error =raController.verifySNSUser(name, SNSUserNumber);
        if (error!=0){
            System.out.println(error(error));
        }
        else {
            String confirm = Utils.readLineFromConsole("Confirm all the data:(Yes/No)");
            assert confirm != null;
            if (confirm.equals("Yes") || confirm.equals("yes")) {
                raController.saveSNSUser();
            } else {
                registerArrivalOfSNSUser();
            }
        }
    }

    /**
     * Method to detect password or SNSNumber errors
     * @param errorType ErrorType
     * @return
     */
    public String error(int errorType){
        String error="";
        String errorNamePasswordNotValid="The name or password is wrong.";
        String errorSNSUserNumber="The SNS user number is not in the system. Verify your SNS user number or the vaccine schedule ";

        if (errorType==1){
            error=error+errorNamePasswordNotValid;
        }
        else
            if (errorType==2){
            error=error+errorSNSUserNumber;
        }
        return error;
    }
}
