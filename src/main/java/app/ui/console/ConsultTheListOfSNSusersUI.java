package app.ui.console;


import app.domain.model.stores.WaitingRoomStore;
import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Queue;


public class ConsultTheListOfSNSusersUI implements Runnable {
    private WaitingRoomStore waitingRoomStore = new WaitingRoomStore();
    private Queue<String>WRoomList= waitingRoomStore.getWaitingRoomListName();



    public void run(){
        printList();
    }


    public void printList() {
        System.out.println("List of SNS users in the waiting room by order of arrival:");
        System.out.println(WRoomList);
    }

}
