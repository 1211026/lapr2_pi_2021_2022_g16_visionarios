package app.ui.console;

import app.controller.App;
import app.controller.RegisterVaccinationCenterController;
import app.controller.SpecifyNewVaccineTypeController;
import app.controller.VaccineScheduleController;
import app.domain.model.*;
import app.domain.model.stores.VaccinationCenterStore;
import app.domain.model.stores.VaccinationScheduleStore;
import app.domain.model.stores.VaccineStore;
import app.ui.console.utils.Utils;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class VaccineScheduleUI implements Runnable{
    //private VaccinationCenterStore vaccinationCenterStore=new VaccinationCenterStore();
    private VaccineStore vaccineStore;
    private Company c1 = App.getInstance().getCompany();
    private VaccineSchedule vs;
    private VaccineScheduleController vsController = new VaccineScheduleController(c1);
    private RegisterVaccinationCenterController vcController = new RegisterVaccinationCenterController(c1);
    private SpecifyNewVaccineTypeController specifyNewVaccineTypeController=new SpecifyNewVaccineTypeController(c1);
    private VaccinationCenterStore vcStore = new VaccinationCenterStore();
    public void run(){
        System.out.println("Register new Vaccine Schedule");
        boolean flag=false;
        do {
            if (registerVaccineSchedule()){
                System.out.println("Vaccine schedule with sucess");
                String option= Utils.readLineFromConsole("Do you want to receive a message(Y/N)");
                if (option.equals("Yes")){
                    try {
                        vsController.sendSMS();
                    } catch (FileNotFoundException e) {
                        throw new RuntimeException(e);
                    }
                }
                flag=true;
            }else{
                System.out.println("Vaccine was not Schedule");
                System.out.println(error());
            }
        }while (flag==false);

    }
    public boolean registerVaccineSchedule(){
        VaccinationCenter vc1;
        VaccineType vaccineType;
        ArrayList<VaccinationCenter>vaccinationCenterslist=new ArrayList<>();
        ArrayList<VaccineType>vaccineTypesList=new ArrayList<>();
        vaccinationCenterslist=vcController.readFromFile();
        vaccineTypesList=specifyNewVaccineTypeController.readFromFile();
        int snsUserNumber= Utils.readIntegerFromConsole("Enter SnsUserNumber:");
        Utils.showList(vaccinationCenterslist,"List of Vaccination Center");
        int option = Utils.readIntegerFromConsole("Select the Vaccination Center");
        vc1 = vaccinationCenterslist.get(option-1);
        String date=Utils.readLineFromConsole("Enter date(dd/mm/yyyy:");
        String time=Utils.readLineFromConsole("Enter the time(hh:mm):");
        Utils.showList(vaccineTypesList,"List of Vaccines");
        int option2=Utils.readIntegerFromConsole("Select the Vaccine");
        vaccineType=vaccineTypesList.get(option2-1);
        System.out.printf("\n\n|Sns User Number:%s\nVaccination Center:%s\nDate:%s\nTime:%s\nVaccine:%s",snsUserNumber,vc1,date,time,vaccineType);
        String confirm = Utils.readLineFromConsole("Confirm all the data:(Y/N");
        assert confirm != null;
        if (confirm.equals("Y") || confirm.equals("y")){
            vsController.createVaccineSchedule(snsUserNumber,vc1,date,time,vaccineType);
            return vsController.saveVaccineSchedule();
        }else {
            return false;
        }
    }
    public String error(){
        String error="";
        String errorSameVaccine="You have two equal vaccines schedule";
        String errorSlotFull="The slot is full chose another hour";
        String errorDateFull="The date is not valid chose another hour";

        if (!vsController.checkSameVaccine()){
            error=error+errorSameVaccine;
        }
        if (!vsController.checkMaxSlot()){
            error=error+errorSlotFull;
        }
        if (!vsController.checkSlotDuration()){
            error=error+errorDateFull;
        }
        return error;
    }




}
