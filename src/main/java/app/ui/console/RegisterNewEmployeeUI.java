package app.ui.console;

import app.controller.App;
import app.controller.RegisterNewEmployeeController;
import app.domain.model.Company;
import app.domain.model.Employee;
import app.ui.console.utils.Utils;

/**
 * @author Diogo Teixeira <1210916@isep.ipp.pt>
 */
public class RegisterNewEmployeeUI implements Runnable {
    private Company c1= App.getInstance().getCompany();
    private Employee ne;
    private RegisterNewEmployeeController neController=new RegisterNewEmployeeController(c1);


    @Override
    public void run() {
        System.out.println("Register new Employee");
        if (registerNewEmployee()==true){
            System.out.println("Employee registered with success");
        }else{
            System.out.println("Employee not registered. Please try again");
        }
    }

    /**
     * Method to register the attributes for a new employee
     * @return
     */

    public boolean registerNewEmployee(){
        String id= Utils.readLineFromConsole("Enter id:");
        String name=Utils.readLineFromConsole("Enter name:");
        String address =Utils.readLineFromConsole("Enter address:");
        String email=Utils.readLineFromConsole("Enter email:");
        String phoneNumber=Utils.readLineFromConsole("Enter phone number:");
        String citizenCardNumber=Utils.readLineFromConsole("Enter citizen card number:");
        String roleId=Utils.readLineFromConsole("Enter role id:");
        System.out.printf("\n\n|Id:%s\nName:%s\nAddress:%s\nEmail:%s\nPhone number:%s\nCitizen card number:%s\nRole id:%s",id,name,address,email, phoneNumber,citizenCardNumber,roleId);
        String confirm=Utils.readLineFromConsole("Confirm all the data:(Y / N)");
        if (confirm.equals("Y") || confirm.equals("y")){
            assert id != null;
            int idRes=Integer.parseInt(id);
            assert phoneNumber != null;
            int phoneNumberRes=Integer.parseInt(phoneNumber);
            assert citizenCardNumber != null;
            int citizenCardNumberRes=Integer.parseInt(citizenCardNumber);
            neController.createNewEmployee(idRes,name,address,email,phoneNumberRes,citizenCardNumberRes,roleId);
            return neController.saveNewEmployee();
        }else {
            return false;
        }
    }
}
