package app.ui.console;

import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class CenterCoordinatorUI implements Runnable {

    public CenterCoordinatorUI()
    {
    }

    public void run() {
        List<MenuItem> options = new ArrayList<MenuItem>();
        options.add(new MenuItem("Option A- Load an Legacy data file ",  new LegacyDataUI()));
        options.add(new MenuItem("Option B - Analyze the performance of a center", new AnalyzePerformanceCenterUI()));
        int option = 0;

        do
        {
            option = Utils.showAndSelectIndex(options, "\n\n Center Coordinator Menu:");

            if ( (option >= 0) && (option < options.size()))
            {
                options.get(option).run();
            }
        }
        while (option != -1 );
    }
}

