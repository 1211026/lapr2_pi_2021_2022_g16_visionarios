package app.ui.console;
import app.controller.SpecifyNewVaccineTypeController;
import app.domain.model.Company;
import app.domain.model.VaccineType;
import app.ui.console.utils.Utils;

import java.util.Objects;

public class VaccineTypeUI implements Runnable{

    /**
     * @author Rui Milhazes <1211031>
     */
    private Company c1 = new Company ("Company 1");
    private SpecifyNewVaccineTypeController vtController = new SpecifyNewVaccineTypeController(c1);
    private VaccineType vt;

    @Override

    public void run() {
        System.out.println("Specify new Vaccine type.");
        if (SpecifyNewVaccineType()) {
            System.out.println("New vaccine type specified");
        }else{
            System.out.println("Failed to specify new vaccine type. Try again.");
        }
    }

    /**
     * Method to register the attributes of a new vaccine type
     * @return vtcontroller
     */

    public boolean SpecifyNewVaccineType(){
        System.out.print("(Every attribute must have between 4 and 8 characters)");
        String code = Utils.readLineFromConsole("Enter code.");
        String designation = Utils.readLineFromConsole("Enter designation.");
        int duration = Utils.readIntegerFromConsole("Enter the duration.");
        System.out.printf("\n\n|Code:%s\n|Designation:%s\n|Duration:%s", code, designation, duration);
        String confirm = Utils.readLineFromConsole("Confirm all the data:(Y/N)");
        if (Objects.equals(confirm, "Y")) {
            vtController.createVaccineType(code, designation, duration);
            return vtController.saveVaccineType();
        }else{
            return false;
        }
    }

}
