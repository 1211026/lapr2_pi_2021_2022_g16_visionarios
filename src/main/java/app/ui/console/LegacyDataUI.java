package app.ui.console;
import app.controller.LegacyDataController;
import app.ui.console.utils.Utils;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @author Rui Milhazes <1211031@isep.ipp.pt>
 */

public class LegacyDataUI extends Application implements Runnable  {

    public void run() {
        try {
            launch();
            LegacyFilePath();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to locate the file from the console
     * @throws FileNotFoundException generated in the method
     */

    private void LegacyFilePath() throws IOException {

        String legacyFile = Utils.readLineFromConsole("Enter the file path:");
                LegacyDataController.legacyDataController(new File(legacyFile));
                    System.out.println(" ");
                    System.out.println("The Data has been successfully loaded.");

        }

    @Override
    public void start(Stage stage) throws Exception {
        try {
            //FXMLLoader fxmlLoader=new FXMLLoader(MainJavaFX.class.getResource("VaccineAdministration.fxml"));
            Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("LegacyData.fxml"));
            //Scene scene= new Scene(fxmlLoader.load(),320,240);
            Scene scene=new Scene(root);
            stage.setScene(scene);
            stage.show();
        }catch (Exception a){
            a.printStackTrace();
        }
    }
}
