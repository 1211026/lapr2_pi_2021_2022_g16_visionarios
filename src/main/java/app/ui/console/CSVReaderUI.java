package app.ui.console;
import app.domain.model.CSVReader;
import app.ui.console.utils.Utils;
import app.controller.CSVReaderController;
import java.io.IOException;
import java.util.Objects;

/**
 * @author Rui Milhazes <1211031@isep.ipp.pt>
 */

public class CSVReaderUI implements Runnable {

    public void run() {
        FilePath();
    }

    /**
     * Method to locate the file from the console
     * @throws IOException Exception generated in the method
     */

    private void FilePath() {
        boolean a=false;
        try {
            do {
                String csvFile = Utils.readLineFromConsole("Enter the file path:");
                String confirm = Utils.readLineFromConsole("Do you want to load this file? (Yes/No)");
                if (Objects.equals(confirm, "Yes") || Objects.equals(confirm, "yes")){
                CSVReader.read(csvFile);}else{break;}
                System.out.println(" ");
                System.out.println("The users have been successfully loaded ");
                String print = Utils.readLineFromConsole("Do you want to print the information?(Yes/No)");
                if (Objects.equals(confirm, "Yes") && (Objects.equals(print, "Yes"))) {
                    if (CSVReader.checkHeader(csvFile)) {
                        CSVReaderController.csvControllerWithHeader(csvFile);
                        break;
                    } else {
                        CSVReaderController.csvControllerWithoutHeader(csvFile);

                    }
                    a = true;
                } else {
                    a=false;
                }
                break;
            } while (a);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}