package app.ui.console;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class DevTeamUI implements Runnable{

    public DevTeamUI()
    {

    }
    public void run()
    {
        System.out.println("\n");
        System.out.printf("Development Team:\n");
        System.out.printf("\t Student Diogo Teixeira - 1210916@isep.ipp.pt \n");
        System.out.printf("\t Student Gabriel Silva - 1210808@isep.ipp.pt \n");
        System.out.printf("\t Student Rodrigo Moreira - 1211026@isep.ipp.pt \n");
        System.out.printf("\t Student Rodrigo Ferreira - 1211030@isep.ipp.pt \n");
        System.out.printf("\t Student Rui Milhazes - 1211031@isep.ipp.pt \n");
        System.out.println("\n");
    }
}
