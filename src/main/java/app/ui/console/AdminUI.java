package app.ui.console;

import app.controller.CreateSNSUserController;
import app.ui.Main;
import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */

public class AdminUI implements Runnable{
    public AdminUI()
    {
    }

    public void run()
    {
        List<MenuItem> options = new ArrayList<MenuItem>();
        options.add(new MenuItem("Option I- Register a vaccination center to respond to a certain pandemic.", new RegisterVaccinationCenterUI() ));//new ShowTextUI("You have chosen Option I.")
        options.add(new MenuItem("Option J- Register a new employee ", new RegisterNewEmployeeUI()));
        options.add(new MenuItem("Option K- Get a list of Employees with a given function/role. ", new GetListOfEmployeesUI()));
        options.add(new MenuItem("Option L- Specify a new Vaccine Type ",new  VaccineTypeUI()));//new ShowTextUI("You have chosen Option L.")
        options.add(new MenuItem("Option M- Specify a new vaccine and its administration process.", new SpecifyNewVaccineAndAdministrationProcessUI()));//ShowTextUI("You have chosen Option M.")
        options.add(new MenuItem("Option N- Load an CSV file ",  new CSVReaderUI()));//new ShowTextUI("You have chosen Option N."),

        int option = 0;
        do
        {
            option = Utils.showAndSelectIndex(options, "\n\nAdmin Menu:");

            if ( (option >= 0) && (option < options.size()))
            {
                options.get(option).run();
            }
        }
        while (option != -1 );
    }
}
