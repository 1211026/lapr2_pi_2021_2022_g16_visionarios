package app.ui.console;

import app.controller.NumberFullyVaccinatedController;
import app.ui.console.utils.Utils;

import java.io.IOException;


public class NumberFullyVaccinatedUI implements Runnable {
    private NumberFullyVaccinatedController nfvController= new NumberFullyVaccinatedController();


    public void run(){
        System.out.println("Request the number of users fully vaccinated per day.");
        try {
            NumberFullyVaccinated();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public void NumberFullyVaccinated() throws IOException {
        int errorType;
        String firstDay= Utils.readLineFromConsole("Enter the first date(dd/mm/yyyy) of the range: ");
        String lastDay= Utils.readLineFromConsole("Enter the last date(dd/mm/yyyy) of the range: ");
        errorType=nfvController.ValidateInterval(firstDay,lastDay);
        if (errorType==4)
            System.out.println("The dates can not be empty.");
        if (errorType==2)
            System.out.println("The first date is invalid. Please enter a valid date!");
        if (errorType==3)
            System.out.println("The last date is invalid. Please enter a valid date!");
        if (errorType==1)
            System.out.println("The interval of days is invalid!!");
        else if (errorType==0) {
            nfvController.ConvertNumberOfFullyVaccinatedToFile(firstDay,lastDay);
        }
    }
}
