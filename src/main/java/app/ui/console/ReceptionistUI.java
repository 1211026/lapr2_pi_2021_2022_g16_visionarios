package app.ui.console;

import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class ReceptionistUI implements Runnable {

    public ReceptionistUI()
    {
    }

    public void run() {
        List<MenuItem> options = new ArrayList<MenuItem>();
        options.add(new MenuItem("Option A- Register the arrival of a SNS user to take the vaccine.   ", new RegisterTheArrivalOfSNSUserUI()));//new ShowTextUI("You have chosen Option A.")
        options.add(new MenuItem("Option B- Register a SNS user. ",  new CreateSNSUserUI()));//new ShowTextUI("You have chosen Option B.")
        options.add(new MenuItem("Option C- Schedule a vaccination. ", new VaccineScheduleUI() ));//new ShowTextUI("You have chosen option C")

        int option = 0;
        do
        {
            option = Utils.showAndSelectIndex(options, "\n\n Receptionist Menu:");

            if ( (option >= 0) && (option < options.size()))
            {
                options.get(option).run();
            }
        }
        while (option != -1 );
    }
}

