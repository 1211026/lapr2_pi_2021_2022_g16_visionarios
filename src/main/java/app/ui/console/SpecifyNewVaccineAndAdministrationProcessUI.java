package app.ui.console;
import app.controller.DosageNumberDosageGroupAgeController;
import app.controller.SpecifyNewVaccineAndAdministrationProcessController;
import app.domain.model.*;
import app.domain.model.stores.VaccineTypeStore;
import app.ui.console.utils.Utils;
import app.controller.RegisterVaccinationCenterController;

import java.util.ArrayList;
import java.util.List;

public class SpecifyNewVaccineAndAdministrationProcessUI implements Runnable {
    private Company c1=new Company("Company 1");
    private VaccineTypeStore vaccineTypeStore=new VaccineTypeStore();
    private SpecifyNewVaccineAndAdministrationProcessController specifyNewVaccineController=new SpecifyNewVaccineAndAdministrationProcessController(c1);
    private DosageNumberDosageGroupAgeController dosageNumberDosageGroupAgeController=new DosageNumberDosageGroupAgeController(c1);
    @Override
    public void run() {
        boolean run=true;
        boolean flag;
        try {
            vaccineTypeStore.getVaccineTypeList();
        }catch (NullPointerException b){
            System.out.println("To register a new vaccine you need to have a vaccine type registered (option L on Menu).  ");
            run=false;
        }
        if(run) {
            System.out.println("Register new Vaccine");
            String confirm1=Utils.readLineFromConsole("Do you want to create a process of administration of a vaccine already created(Y/N)");
            if (confirm1.equals("Y")){
                do {
                    System.out.println("Specify the administration process");
                    if (registerProcessOfAdministration() == true) {
                        System.out.println("Process of administration created with success");
                    } else {
                        System.out.println("Process of administration created try again");
                    }
                    String confirm=Utils.readLineFromConsole("Do you want to create another process of administration(Y/N)");
                    if (confirm.equals("Y")){
                        flag=false;
                    }else {
                        flag=true;
                    }
                }while (flag==false);
            }else {
                if (registerVaccine() == true) {
                    System.out.println("Vaccine created with success");
                } else {
                    System.out.println("Vaccine not created try again");
                }
                do {
                    System.out.println("Specify the administration process");
                    if (registerProcessOfAdministration() == true) {
                        System.out.println("Process of administration created with success");
                    } else {
                        System.out.println("Process of administration created try again");
                    }
                    String confirm=Utils.readLineFromConsole("Do you want to create another process of administration(Y/N)");
                    if (confirm.equals("Y")){
                        flag=false;
                    }else {
                        flag=true;
                    }
                }while (flag==false);
            }


        }
    }
    public boolean registerVaccine(){
        String name=Utils.readLineFromConsole("Enter name:");
        Vaccine.verifyNewVaccineName(name);
        String brand=Utils.readLineFromConsole("Enter brand:");
        Vaccine.verifyNewVaccineBrand(brand);
        String code=Utils.readLineFromConsole("Enter code:");
        System.out.printf("\n\n|Code:%s\nName:%s\nBrand:%s",name,brand,code);
        String confirm=Utils.readLineFromConsole("Confirm all the data:(Y/N)");
        if (confirm.equals("Y")){
            specifyNewVaccineController.createNewVaccine(name,brand,code);
            return specifyNewVaccineController.saveNewVaccine() ;
        }else {
            return false;
        }
    }
    public boolean registerProcessOfAdministration(){
            List<Vaccine> vacinneList=new ArrayList<>();
            Vaccine vaccine;
            vacinneList=specifyNewVaccineController.readFromFileVac();
            Utils.showList(vacinneList,"List of Vaccines");
            int option = Utils.readIntegerFromConsole("Select the vaccine to specify the administration");
            vaccine = vacinneList.get(option-1);
            String name=vaccine.getName();
            String brand =vaccine.getBrand();
            String code=vaccine.getCode();
            int dosage=Utils.readIntegerFromConsole("Enter the dose");
            String groupAge=Utils.readLineFromConsole("Enter the group age (age-age) ");
            int dosePerGroupAge=Utils.readIntegerFromConsole("Enter the dose for the respectably group age");
            System.out.printf("\n\n|Code:%s\nName:%s\nBrand:%s\n|Dosage:%d\nGroup Age:%s\nDosage per group age:%s",name,brand,code,dosage,groupAge,dosePerGroupAge);
            String confirm=Utils.readLineFromConsole("Confirm all the data:(Y/N)");
            if (confirm.equals("Y")){
                dosageNumberDosageGroupAgeController.createDosageNumberDosageGroupAge(name,brand,code,dosage,groupAge,dosePerGroupAge);
                return dosageNumberDosageGroupAgeController.saveDosageNumberDosageGroupAge() ;
            }else {
                return false;
            }
    }


}
